package com.siampiwat.MenuSiamGiftCard;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.result.Result;
import com.google.gson.Gson;
import com.siampiwat.API.SiamGiftCardApi;
import com.siampiwat.BaseObject.BaseOnActivity;
import com.siampiwat.BaseObject.BottomView;
import com.siampiwat.Extention.ContextKt;
import com.siampiwat.Model.SiamGiftCardModel;
import com.siampiwat.Model.SiamGiftCardResponseModel;
import com.siampiwat.R;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class SiamGiftCardFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String data;

    private BottomView bottomView;
    private Activity activity;
    private SiamGiftCardModel siamGiftCardModel;

    public SiamGiftCardFragment() {

    }

    public static SiamGiftCardFragment newInstance(String param1, String param2) {
        SiamGiftCardFragment fragment = new SiamGiftCardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            activity = getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_siam_gift_card, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bindView(view);
        bindEvent();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);



    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void bindView(View view)
    {
        bottomView = view.findViewById(R.id.bottomView);
    }

    private void bindEvent()
    {
        bottomView.setListener(new BottomView.onClickNavigator() {
            @Override
            public void onAccept() {
//                CustomerModel model = new CustomerModel();
//                model.id = "0001-000";
//                model.name = "ราชา ขายดี";
//                model.type = "ประชาชน";
//                model.expireDate = "20/20";
//                model.point = "20,000";
//                model.channelSubscribe = "internet";

//                requestSiamGiftCard("","");

                Intent i = new Intent(activity, SiamGiftCardPinActivity.class);
                i.putExtra("card_number" , "");
                startActivity(i);

            }
        });
    }
}