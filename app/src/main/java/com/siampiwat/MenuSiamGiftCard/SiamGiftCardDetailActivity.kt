package com.siampiwat.MenuSiamGiftCard

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.siampiwat.API.SiamGiftCardApi
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.BaseObject.SearchActivity.MODE_ADD_PRODUCT
import com.siampiwat.BaseObject.SearchFragment
import com.siampiwat.Extention.indeterminateProgress
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.Model.CustomerModel
import com.siampiwat.Model.SiamGiftCardModel
import com.siampiwat.Model.SiamGiftCardResponseModel
import com.siampiwat.R
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_siam_gift_card_detail.*

class SiamGiftCardDetailActivity : BaseOnActivity() {

    lateinit var siamGiftCardModel: SiamGiftCardModel
    private var dlgAlert: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_siam_gift_card_detail)

        var bundle = intent.extras
        if( !bundle.isEmpty )
        {
            var data = bundle.getString("card_number")
            var pin = bundle.getString("pin")
//            siamGiftCardModel = Gson().fromJson(data , SiamGiftCardModel::class.java)

            requestSiamGiftCard("123456789012345555" , "0000")
//            requestSiamGiftCard(data , pin)
        }
    }

    private fun bindView()
    {
        tvIdCard.text = siamGiftCardModel.CardNumber
        tvNameCustomer.text = siamGiftCardModel.Name
        tvTypeCustomer.text = siamGiftCardModel.CardType
        tvExpireDate.text = siamGiftCardModel.Expire
        tvSummaryPoint.text = siamGiftCardModel.Balance.toString2Digit()

        navigatorView.setListener { finish() }
    }

    private fun requestSiamGiftCard(cardNumber: String, pinNumber: String) {

        val dialog = indeterminateProgress("")
        dialog!!.show()

        SiamGiftCardApi.getSiamGiftCard(cardNumber, pinNumber).subscribe(object : SingleObserver<Result<SiamGiftCardResponseModel, FuelError>> {
            override fun onSubscribe(d: Disposable) {

            }

            override fun onSuccess(SiamGiftCardResponseModel: Result<SiamGiftCardResponseModel, FuelError>) {
                val model = SiamGiftCardResponseModel.component1()
                val Error = SiamGiftCardResponseModel.component2()

                dialog!!.dismiss()
                if (Error == null && model != null && model.Error.size == 0) {
                    siamGiftCardModel = model.CardData

                    val data = Gson().toJson(siamGiftCardModel)
                    siamGiftCardModel = Gson().fromJson(data , SiamGiftCardModel::class.java)
                    bindView()

                } else {
                    alertItemNotFound(model!!.ResponseMessage!!.Message!!,"")
                }
            }

            override fun onError(e: Throwable) {
                alertItemNotFound(e.message,"")
                dialog!!.dismiss()
            }
        })
    }

    private fun alertItemNotFound(message: String?, dataSearch: String) {
        dlgAlert = Dialog(this, R.style.dialog_theme)
        dlgAlert!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dlgAlert!!.setContentView(R.layout.dlg_search_again)
        dlgAlert!!.setCancelable(true)

        val tvTitle = dlgAlert!!.findViewById(R.id.tvTitle) as TextView
        tvTitle.setText(message)

        val tvOk = dlgAlert!!.findViewById(R.id.tvOk) as TextView
            tvOk.setVisibility(View.GONE)

        val tvClose = dlgAlert!!.findViewById(R.id.tvClose) as TextView
        tvClose.setOnClickListener(View.OnClickListener {
            dlgAlert!!.cancel()
        })
        dlgAlert!!.show()
    }
}
