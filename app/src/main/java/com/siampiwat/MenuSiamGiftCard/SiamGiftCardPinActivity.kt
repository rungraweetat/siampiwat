package com.siampiwat.MenuSiamGiftCard

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import com.google.gson.Gson
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.Model.CustomerModel
import com.siampiwat.R

import kotlinx.android.synthetic.main.activity_siam_gift_card_pin.*
import java.util.*
import kotlin.collections.ArrayList

class SiamGiftCardPinActivity : BaseOnActivity() {

    var data:String=""
    var pin:String=""
    var arLstNumber:ArrayList<TextView> = ArrayList<TextView>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_siam_gift_card_pin)

        var bundle = intent.extras
        if( !bundle.isEmpty )
        {
            data = bundle.getString("card_number")
        }

        bindView()
        bindEvent()
        bindValue();
    }

    private fun bindView()
    {
        mapText()
    }

    private fun bindEvent()
    {
        for (item in arLstNumber) {
            item.setOnClickListener {

                if( pin.length >= 4 ) return@setOnClickListener

                Log(item.text.toString())
                pin += item.text as String

                updateUI()
            }
        }

        ivDel.setOnClickListener {
            if(pin.isNotEmpty())
            {
                pin = pin.dropLast(1)
            }
            updateUI()
        }

        navigatorView.setListener { finish() }
        bottomView.setListener{ startSiamGiftCardDetailActivity() }
    }

    private fun bindValue()
    {
        updateUI()
    }

    private fun getNumber():ArrayList<String>
    {
        val arrayList = ArrayList<String>()
        arrayList.add("0")
        arrayList.add("1")
        arrayList.add("2")
        arrayList.add("3")
        arrayList.add("4")
        arrayList.add("5")
        arrayList.add("6")
        arrayList.add("7")
        arrayList.add("8")
        arrayList.add("9")
        arrayList.shuffle()

        return arrayList
    }
    private fun mapText()
    {
        val arLst = getNumber()
        tvNumber1.text = arLst[0]
        tvNumber2.text = arLst[1]
        tvNumber3.text = arLst[2]
        tvNumber4.text = arLst[3]
        tvNumber5.text = arLst[4]
        tvNumber6.text = arLst[5]
        tvNumber7.text = arLst[6]
        tvNumber8.text = arLst[7]
        tvNumber9.text = arLst[8]
        tvNumber0.text = arLst[9]

        arLstNumber.add(tvNumber1)
        arLstNumber.add(tvNumber2)
        arLstNumber.add(tvNumber3)
        arLstNumber.add(tvNumber4)
        arLstNumber.add(tvNumber5)
        arLstNumber.add(tvNumber6)
        arLstNumber.add(tvNumber7)
        arLstNumber.add(tvNumber8)
        arLstNumber.add(tvNumber9)
        arLstNumber.add(tvNumber0)
    }

    private fun startSiamGiftCardDetailActivity()
    {
        val intent = Intent( this,SiamGiftCardDetailActivity::class.java )
        intent.putExtra("card_number" , data)
        intent.putExtra("pin" , pin)
        startActivity(intent)
    }

    private fun updateUI()
    {
        if( pin.isEmpty() )
        {
            bottomView.visibility = View.INVISIBLE

            ivDot1.visibility = View.INVISIBLE
            ivDot2.visibility = View.INVISIBLE
            ivDot3.visibility = View.INVISIBLE
            ivDot4.visibility = View.INVISIBLE

            return
        }
        else bottomView.visibility = View.VISIBLE

        ivDot1.visibility = View.GONE
        ivDot2.visibility = View.GONE
        ivDot3.visibility = View.GONE
        ivDot4.visibility = View.GONE
        bottomView.visibility = View.GONE

        when {
            pin.length == 1 -> ivDot1.visibility = View.VISIBLE
            pin.length == 2 -> {
                ivDot1.visibility = View.VISIBLE
                ivDot2.visibility = View.VISIBLE
            }
            pin.length == 3 -> {
                ivDot1.visibility = View.VISIBLE
                ivDot2.visibility = View.VISIBLE
                ivDot3.visibility = View.VISIBLE
            }
            else -> {
                ivDot1.visibility = View.VISIBLE
                ivDot2.visibility = View.VISIBLE
                ivDot3.visibility = View.VISIBLE
                ivDot4.visibility = View.VISIBLE
                bottomView.visibility = View.VISIBLE
            }
        }
    }

}
