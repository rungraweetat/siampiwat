package com.siampiwat.MenuCart


import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import com.siampiwat.BaseObject.SearchActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.*
import android.widget.TextView
import com.google.gson.Gson
import com.siampiwat.*
import com.siampiwat.Adapter.CartNewAdapter
import com.siampiwat.EventBus.*
import com.siampiwat.Lib.SwipeController
import com.siampiwat.MainMenu.MainMenuActivity
import com.siampiwat.Model.InputSalesLineModel
import com.siampiwat.Model.LoyaltyCardModel
import com.siampiwat.State.Action.*
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.Survey.SurveyActivity
import com.siampiwat.Survey.SurveyConst
import com.siampiwat.Survey.SurveyConst.KEY_SURVEY_ACTION_MENU
import com.siampiwat.Survey.SurveyConst.KEY_SURVEY_JSON

import kotlinx.android.synthetic.main.fragment_cart.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.rekotlin.StoreSubscriber
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CartFragment : Fragment(), StoreSubscriber<CartInfoState> {

    var mParam1: String = ""
    var mParam2: String = ""

    enum class RemoveAction {
        none,
        REMOVE_CUSTOMER,
        REMOVE_PRODUCT,
        REMOVE_SALE
    }

//    var cartItemList: ArrayList<ItemInfoModel> = ArrayList()
//    var customerItemList: ArrayList<CustomerModel> = ArrayList()
//    var customerModel: CustomerModel? = null

    var cartItemList: ArrayList<InputSalesLineModel> = ArrayList()
    var customerModel: LoyaltyCardModel? = null

    private lateinit var cartItemListAdapter: CartNewAdapter

    lateinit var mActivity: Activity
    lateinit var fab: FloatingActionButton

    companion object {

        fun newInstance(param1: String, param2: String): CartFragment {
            val fragment = CartFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)
        if (EventBus.getDefault().isRegistered(this)) EventBus.getDefault().unregister(this)
    }

    override fun onDetach() {
        super.onDetach()
        mainStore.unsubscribe(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_cart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnNext.setListener {
            activity?.let {
                val i = Intent(it, DiscountActivity::class.java)
                startActivity(i)
            }
        }
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)

        fab = view.findViewById(R.id.fab)
        fab.setOnClickListener {

            val arLstSurvey = userInfo?.getPreSurvey("M2100")
            val surveyJson = Gson().toJson(arLstSurvey)
            if (arLstSurvey!!.size < 1) {
                startSearch(SearchActivity.MODE_ADD_PRODUCT, SearchActivity.PAGE_SCANNER)
            } else {
                val i = Intent(mActivity, SurveyActivity::class.java)
                i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2100_AddProductPreSurvey)
                startActivity(i)
            }
        }
        setupCartList()
        setupCartState()
    }

    private fun setupCartState() {
        mainStore.unsubscribe(this)
        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }
        }

    }


    private fun setupCartList() {
        val itemDecorator = DividerItemDecoration(context!!, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(context!!, R.drawable.line_divider)!!)
        recycleView.addItemDecoration(itemDecorator)
        recycleView.layoutManager = LinearLayoutManager(activity)
        recycleView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (dy > 0 && fab.visibility == View.VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.visibility != View.VISIBLE) {
                    fab.show();
                }
            }
        } )


        cartItemListAdapter = CartNewAdapter(cartItemList, object : CartNewAdapter.onClickListener {
            override fun onClickDecrease(position: Int) {

                mainStore.dispatch(DecreaseAmountProductInCart(position))
            }

            override fun onClickIncrease(position: Int) {

                mainStore.dispatch(IncreaseAmountProductInCart(position))

            }

            override fun onClickItem(position: Int) {
                //TODO: Navigate to PRODUCT DETAIL ACTIVITY
            }

            override fun onClickCustomer(customerModel: LoyaltyCardModel?) {

                var intent = Intent( mActivity , CustomerDetailActivity::class.java )
                val model = Gson().toJson(customerModel)

                intent.putExtra("data",model)

                startActivity(intent)

            }
        })

        if (customerModel != null) {
            cartItemListAdapter.setCustomer(customerModel!!)
        }

        recycleView.adapter = cartItemListAdapter

        setupSwipeRecycleView()
        validateDisplayEmptyAdapter()
    }

    private fun setupSwipeRecycleView() {
        val swipe = object : SwipeController(mActivity, recycleView) {

            override fun instantiateUnderlayButton(viewHolder: RecyclerView.ViewHolder, underlayButtons: ArrayList<UnderlayButton>) {

                if(viewHolder.adapterPosition == 0 && viewHolder.itemViewType == ITEM_TYPE.CUSTOMER ){
                    underlayButtons.add(UnderlayButton(getString(R.string.label_item_delete),  ResourcesCompat.getDrawable(getResources(), R.drawable.ico_trash, null), ContextCompat.getColor(mActivity, R.color.item_delete), object : UnderlayButtonClickListener {
                        override fun onClick(pos: Int) {
                            dlgConfirmRemove(RemoveAction.REMOVE_CUSTOMER , pos)
                        }
                    }))
                    return
                }

                underlayButtons.add(UnderlayButton(getString(R.string.label_item_delete),  ResourcesCompat.getDrawable(getResources(), R.drawable.ico_trash, null), ContextCompat.getColor(mActivity, R.color.item_delete), object : UnderlayButtonClickListener {
                    override fun onClick(pos: Int) {
                        dlgConfirmRemove(RemoveAction.REMOVE_PRODUCT , pos)
                    }
                }))
                underlayButtons.add(UnderlayButton(getString(R.string.label_item_edit_price),  ResourcesCompat.getDrawable(getResources(), R.drawable.ico_edit_price, null), ContextCompat.getColor(mActivity, R.color.item_edit_price), object : UnderlayButtonClickListener {
                    override fun onClick(pos: Int) {
                        var arLstSurvey = userInfo?.getPreSurvey("M2102")
                        if( arLstSurvey!!.size > 0 )
                        {
                            var index = viewHolder.adapterPosition
                            if( customerModel != null )
                            {
                                index = viewHolder.adapterPosition - 1
                            }

                            var dataSurvey = Gson().toJson(arLstSurvey)
                            var intent = Intent( mActivity , SurveyActivity::class.java )
                            intent.putExtra(KEY_SURVEY_JSON,dataSurvey)
                            intent.putExtra(KEY_SURVEY_ACTION_MENU, SurveyConst.M2103_PreSurvey)
                            intent.putExtra("ItemId", cartItemList[index].ItemId)
                            startActivity(intent)
                        }
                    }
                }))
                underlayButtons.add(UnderlayButton(getString(R.string.label_item_edit_discount),  ResourcesCompat.getDrawable(getResources(), R.drawable.ico_take_discount, null), ContextCompat.getColor(mActivity, R.color.item_edit_discount), object : UnderlayButtonClickListener {
                    override fun onClick(pos: Int) {
                        var arLstSurvey = userInfo?.getPreSurvey("M2102")
                        if( arLstSurvey!!.size > 0 )
                        {
                            var index = viewHolder.adapterPosition
                            if( customerModel != null )
                            {
                                index = viewHolder.adapterPosition - 1
                            }

                            var dataSurvey = Gson().toJson(arLstSurvey)
                            var intent = Intent( mActivity , SurveyActivity::class.java )
                            intent.putExtra(KEY_SURVEY_JSON,dataSurvey)
                            intent.putExtra(KEY_SURVEY_ACTION_MENU,SurveyConst.M2102_PreSurvey)
                            intent.putExtra("ItemId", cartItemList[index].ItemId)
                            startActivity(intent)
                        }
                    }
                }))

                var labelSeller = ""
                var drawableSeller = ResourcesCompat.getDrawable(getResources(), R.drawable.ico_add_sale_white, null)!!
                var colorSeller = ContextCompat.getColor(mActivity, R.color.item_add_seller)
                if( viewHolder.itemViewType == ITEM_TYPE.PRODUCT )
                {
                    var index = viewHolder.adapterPosition
                    if( customerModel != null )
                    {
                        index = viewHolder.adapterPosition - 1
                    }

                    if( TextUtils.isEmpty(cartItemList[index].SalesPersonName) && TextUtils.isEmpty(cartItemList[index].SalesPersonId) ) {
                        labelSeller = getString(R.string.label_item_add_seller)
                        drawableSeller = ResourcesCompat.getDrawable(getResources(), R.drawable.ico_add_sale_white, null)!!
                        colorSeller = ContextCompat.getColor(mActivity, R.color.item_add_seller)
                    }
                    else{
                        labelSeller = getString(R.string.label_item_delete_seller)
                        drawableSeller = ResourcesCompat.getDrawable(getResources(), R.drawable.ico_add_sale_white, null)!!
                        colorSeller = ContextCompat.getColor(mActivity, R.color.item_delete)
                    }
                }

                underlayButtons.add(UnderlayButton(labelSeller,  drawableSeller, colorSeller, object : UnderlayButtonClickListener {
                    override fun onClick(pos: Int) {

                        var index = viewHolder.adapterPosition
                        if( customerModel != null )
                        {
                            index = viewHolder.adapterPosition - 1
                        }
                        val arLstSurvey = userInfo?.getPreSurvey("M2101")
                        val surveyJson = Gson().toJson(arLstSurvey)
                        if (arLstSurvey!!.size < 1) {
                            if( cartItemList[index].SalesPersonId.isNullOrEmpty() || cartItemList[index].SalesPersonName.isNullOrEmpty())
                            {
                                //add
                                val intent = Intent(mActivity, SearchActivity::class.java)
                                intent.putExtra("mode", SearchActivity.MODE_ADD_SALE_BY_PRODUCT)
                                intent.putExtra("position" , index)
                                intent.putExtra("page" , SearchActivity.PAGE_SCANNER)
                                startActivity(intent)
                            }
                            else
                            {
                                //remove
                                dlgConfirmRemove(RemoveAction.REMOVE_SALE , index)
                            }
                        } else {
                            val i = Intent(mActivity, SurveyActivity::class.java)
                            i.putExtra("ItemId" , index)
                            i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                            i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2101_PreSurvey)
                            startActivity(i)
                        }
                    }
                }))
            }
        }
    }

    private fun startSearch(mode: Int, page: Int) {
        val i = Intent(activity, SearchActivity::class.java)
        i.putExtra("mode", mode)
        i.putExtra("page", page)
        startActivityForResult(i, REQUEST_CODE.ADD_PRODUCT)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        this.mActivity = activity!!
    }

    private fun validateDisplayEmptyAdapter() {
        if (customerModel != null && cartItemList.size == 0) {
            llEmptyCart.visibility = View.VISIBLE
            llTmpHeader.visibility = View.INVISIBLE
            recycleView.visibility = View.VISIBLE
            btnNext.visibility = View.GONE
        }
        else if (customerModel == null && cartItemList.size == 0) {
            llEmptyCart.visibility = View.VISIBLE
            llTmpHeader.visibility = View.GONE
            btnNext.visibility = View.GONE
            recycleView.visibility = View.GONE
        }
        else if( cartItemList.size > 0 )
        {
            llEmptyCart.visibility = View.GONE
            llTmpHeader.visibility = View.GONE
            recycleView.visibility = View.VISIBLE
            btnNext.visibility = View.VISIBLE
        }

    }

    override fun newState(state: CartInfoState) {
        cartItemList.clear()
        cartItemList.addAll(state.transactionData.InputSalesLine)

        customerModel = state.transactionData.LoyaltyCard
        cartItemListAdapter.setCustomer(customerModel)

        cartItemListAdapter.notifyDataSetChanged()
        this.validateDisplayEmptyAdapter()


        var summaryAmount:Double = summaryAmount()
        setProductCountTitle(summaryAmount.toInt())

        var main = mActivity as MainMenuActivity
        main.updateSumProductInCart( summaryAmount.toInt() , state )
    }

    private fun dlgConfirmRemove(removeAction:RemoveAction , position:Int) {
        val dlgConfirmRemove = Dialog(mActivity, R.style.dialog_theme)
        dlgConfirmRemove.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dlgConfirmRemove.setContentView(R.layout.dlg_confirm)
        dlgConfirmRemove.setCancelable(true)

        val tvTitle = dlgConfirmRemove.findViewById(R.id.tvTitle) as TextView
        tvTitle.text = resources.getString(R.string.common_alert_title_remove_product)

        val tvMessage = dlgConfirmRemove.findViewById(R.id.tvMessage) as TextView
        tvMessage.visibility = View.GONE

        val tvCanceler = dlgConfirmRemove.findViewById(R.id.tvCancel) as TextView
        tvCanceler.visibility = View.VISIBLE
        tvCanceler.setOnClickListener {
            dlgConfirmRemove.hide()
        }

        val tvOk = dlgConfirmRemove.findViewById(R.id.tvOk) as TextView
        tvOk.setOnClickListener(View.OnClickListener {

            if(removeAction == RemoveAction.REMOVE_CUSTOMER)
                mainStore.dispatch(RemoveCustomerInCart(position))
            else if( removeAction == RemoveAction.REMOVE_PRODUCT )
                mainStore.dispatch(RemoveProductInCart(position))
            else if( removeAction == RemoveAction.REMOVE_SALE )
                mainStore.dispatch(RemoveSellerInProduct(position))

            dlgConfirmRemove.hide()
        })


        dlgConfirmRemove.show()
    }

    private fun summaryAmount():Double
    {
        var sumAmount:Double = 0.0
        for ( i in cartItemList.iterator() )
            sumAmount += i.Qty

        return sumAmount
    }

    private fun setProductCountTitle(amount: Int) {

        getActionBar()?.title = getString(R.string.nav_cart) + " ($amount)"
    }

    private fun getActionBar(): ActionBar? {
        return (getActivity() as AppCompatActivity).supportActionBar
    }

    @Subscribe
    public fun onEvent( event:M2101PreSurveyModel )
    {
        if( cartItemList[event.index].SalesPersonId.isNullOrEmpty() || cartItemList[event.index].SalesPersonName.isNullOrEmpty())
        {
            //add
            val intent = Intent(mActivity, SearchActivity::class.java)
            intent.putExtra("mode", SearchActivity.MODE_ADD_SALE_BY_PRODUCT)
            intent.putExtra("position" , event.index)
            intent.putExtra("page" , SearchActivity.PAGE_SCANNER)
            startActivity(intent)
        }
        else
        {
            //remove
            dlgConfirmRemove(RemoveAction.REMOVE_SALE , event.index)
        }
    }

    @Subscribe
    public fun onEvent( event:M2102PresurveyModel )
    {
        var intent = Intent( mActivity , DiscountLineProductActivity::class.java )
        intent.putExtra("ItemId" , event.itemId )
        startActivity(intent)
    }

    @Subscribe
    public fun onEvent( event: M2103PresurveyModel)
    {
        var intent = Intent( mActivity , ChangPriceLineProductActivity::class.java )
        intent.putExtra("ItemId" , event.itemId )
        startActivity(intent)
    }

    @Subscribe
    public fun onEvent( event: M2100AddProductPreSurveyModel)
    {
        startSearch(SearchActivity.MODE_ADD_PRODUCT, SearchActivity.PAGE_SCANNER)
    }

    @Subscribe
    public fun onEvent( event: M2200PostSurveyModel)
    {
        val arLstSurvey = userInfo?.getPostSurvey("M2201")
        val surveyJson = Gson().toJson(arLstSurvey)
        if (arLstSurvey!!.size > 1) {
            val i = Intent(mActivity, SurveyActivity::class.java)
            i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
            i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2201_PostSurvey)
            startActivity(i)
        }
    }
}
