package com.siampiwat.MenuCart

import android.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.Model.ManualLineDiscountModel
import com.siampiwat.R
import com.siampiwat.State.Action.AddManualLineDiscount
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.mainStore
import kotlinx.android.synthetic.main.activity_discount_line_product.*
import org.rekotlin.StoreSubscriber

class DiscountSpecialActivity : BaseOnActivity() , StoreSubscriber<CartInfoState> {
    var itemId = ""
    override fun newState(state: CartInfoState) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discount_special)

        mainStore.unsubscribe(this)
        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }
        }

        try {
            itemId = intent.getStringExtra("ItemId")

        } catch(ex: Exception) {
            AlertDialog.Builder(this)
                    .setMessage("Don't have data")
                    .setPositiveButton("OK") { dialog, which ->
                        finish()
                    }
                    .setCancelable(false)
                    .show()
            return
        }

        bottomView.setListener{
            var flag = 0
            var value = edtDiscountSurvey.text.toString()

            if( rbPercentDiscountSurvey.isChecked && !rbBahtDiscountSurvey.isChecked )
                flag = 0
            else
                flag = 1

            val addManualLineDiscount = ManualLineDiscountModel( value.toDouble(), flag)
            mainStore.dispatch(AddManualLineDiscount(addManualLineDiscount,itemId))
            finish()
        }

        navigatorView.setListener {
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)
    }
}
