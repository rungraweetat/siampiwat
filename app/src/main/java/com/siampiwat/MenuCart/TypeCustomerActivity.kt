package com.siampiwat.MenuCart

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.siampiwat.API.AffiliationsApi
import com.siampiwat.API.DiscountApi
import com.siampiwat.API.ItemApi
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.BaseObject.SearchActivity
import com.siampiwat.EventBus.M2200PreSurveyModel
import com.siampiwat.EventBus.M2400PostSurveyModel
import com.siampiwat.Extention.indeterminateProgress
import com.siampiwat.Model.*
import com.siampiwat.R
import com.siampiwat.State.Action.AddCouponDiscount
import com.siampiwat.State.Action.AddProductInCart
import com.siampiwat.Survey.SurveyActivity
import com.siampiwat.Survey.SurveyConst
import com.siampiwat.mainStore
import com.siampiwat.userInfo
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_type_customer.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.toast
import java.util.*

class TypeCustomerActivity : BaseOnActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_type_customer)

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)

        requestAffiliations()

        btnNext.setListener {

            val arLstSurvey = userInfo?.getPostSurvey("M2400")
            val surveyJson = Gson().toJson(arLstSurvey)
            if (arLstSurvey!!.size < 1) {
                finish()
            } else {
                val i = Intent(this, SurveyActivity::class.java)
                i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2400_PostSurvey)
                startActivity(i)
            }
//            Toast.makeText(this , getString(R.string.layout_type_customer_toast) , Toast.LENGTH_SHORT).show()
        }

        navigatorView.setListener {
            finish()
        }
    }

    private fun requestAffiliations() {

        val dialog = this@TypeCustomerActivity.indeterminateProgress("")
        dialog!!.show()

        AffiliationsApi.getAffiliations().subscribe(object : SingleObserver<Result<AffiliationsResponseModel, FuelError>> {
            override fun onSubscribe(d: Disposable) {

            }

            override fun onSuccess(productInfoModelFuelErrorResult: Result<AffiliationsResponseModel, FuelError>) {
                val productInfoModel = productInfoModelFuelErrorResult.component1()
                val Error = productInfoModelFuelErrorResult.component2()

                dialog.dismiss()
                if (Error == null && productInfoModel != null && productInfoModel.Error.size == 0) {
                    if (productInfoModel.Affiliation.size > 0)
                    {
                        llContainer.removeAllViews()
                        for( i in productInfoModel.Affiliation.indices )
                        {
                            val vLayoutTypeCustomer = layoutInflater.inflate(R.layout.layout_type_customer , null)
                            val llLine = vLayoutTypeCustomer.findViewById(R.id.llLine) as View
                            val tvTypeCustomer = vLayoutTypeCustomer.findViewById(R.id.tvTypeCustomer) as TextView
                            llLine.visibility = View.VISIBLE

                            tvTypeCustomer.text = productInfoModel.Affiliation[i].Description

                            llContainer.addView(vLayoutTypeCustomer)
                        }
                    }
                } else {

                    Toast.makeText(this@TypeCustomerActivity ,
                            productInfoModel!!.ResponseMessage!!.Message ,
                            Toast.LENGTH_SHORT).show()
                }
            }

            override fun onError(e: Throwable) {
                dialog.dismiss()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()

        if (EventBus.getDefault().isRegistered(this)) EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: M2400PostSurveyModel) {
       finish()
    }
}
