package com.siampiwat.MenuCart

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import com.siampiwat.Model.CustomerModel
import com.siampiwat.Model.LoyaltyCardModel
import com.siampiwat.R
import kotlinx.android.synthetic.main.activity_customer_detail.*

class CustomerDetailActivity : AppCompatActivity() {

    private lateinit var customerModel:LoyaltyCardModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_detail)


        val bundle = intent.extras
        if( bundle != null )
        {
            if( bundle.containsKey("data") )
            {
                val data = bundle.getString("data")
                customerModel = Gson().fromJson(data , LoyaltyCardModel::class.java)

                bindData()
            }
        }
        else{
            finish()
        }

    }

    private fun bindData()
    {
        textMemberIdCard.text = customerModel.CardNumber
        txtMemberName.text = customerModel.CustomerName
        txtPoint.text = customerModel.BalancePoints.toString()
        tvExpireDate.text = customerModel.ExpireDate
        tvChanel.text = customerModel.Source

        navigatorView.setListener {
            finish()
        }
    }
}
