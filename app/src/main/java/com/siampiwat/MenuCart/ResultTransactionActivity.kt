package com.siampiwat.MenuCart

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.MainMenu.MainMenuActivity
import com.siampiwat.Model.TransactionDataModel
import com.siampiwat.Payment.PaymentMethodActivity
import com.siampiwat.R
import com.siampiwat.State.Action.CancelSeller
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.mainStore
import kotlinx.android.synthetic.main.activity_result_transaction.*
import org.rekotlin.StoreSubscriber

class ResultTransactionActivity : BaseOnActivity() , StoreSubscriber<CartInfoState> {
    private var isPayted = false
    private var parentMenuId = ""
    private var refTransaction = ""
    private var transactionDataCalculate: TransactionDataModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_transaction)
        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }
        }

        btnNext.setListener{

            if( isPayted  )
            {
                //จ่ายครบแล้ว
                onBackPressed()
                finish()
            }
            else{
                val intent = Intent(this, PaymentMethodActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

                finish()
            }

        }
        val bundle = intent.extras
        if( bundle != null )
        {
            if( bundle.containsKey("isPayted") )
            {
                isPayted = bundle.getBoolean("isPayted")
                if (isPayted){
                    llPaySuccess.visibility = View.VISIBLE
                    llPayPeriod.visibility = View.GONE
                }
                else{
                    llPaySuccess.visibility = View.GONE
                    llPayPeriod.visibility = View.VISIBLE
                }
            }
            else
            {
                llPaySuccess.visibility = View.GONE
                llPayPeriod.visibility = View.VISIBLE
            }

            if( bundle.containsKey("parentMenuId") )
            {
                parentMenuId = bundle.getString("parentMenuId")
            }
        }

        bindEvent()
    }

    private fun bindEvent() {
        tvTotal.text = transactionDataCalculate!!.SalesNetAmount.toString2Digit()
        tvRefTransaction.text = transactionDataCalculate!!.TransactionId;
    }

    override fun onBackPressed() {
       mainStore.dispatch(CancelSeller())
        val intent = Intent(this, MainMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun newState(state: CartInfoState) {
        transactionDataCalculate = state.transactionDataCalculate

    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)
    }
}
