package com.siampiwat.MenuCart

import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.google.gson.Gson
import com.siampiwat.Adapter.SaleAdapter
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.Model.SalePersonModel
import com.siampiwat.R
import com.siampiwat.State.Action.MatchingSalePersonWithProduct
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.mainStore
import kotlinx.android.synthetic.main.activity_pair_sale_to_product.*

import org.rekotlin.StoreSubscriber

class MatchingSalePersonWithProductActivity : BaseOnActivity(), StoreSubscriber<CartInfoState> {

    private lateinit var saleAdapter: SaleAdapter
    private lateinit var salePersonModel: SalePersonModel
    private lateinit var cartInfoState: CartInfoState
    private var modeSale = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pair_sale_to_product)

        var bundle = intent.extras
        if (bundle != null) {

            if (bundle.containsKey("mode_sale")) {
                modeSale = bundle.getString("mode_sale")
            }

            if( bundle.containsKey("data") )
            {
                var data = bundle.getString("data")
                salePersonModel = Gson().fromJson(data , SalePersonModel::class.java)
            }
        }

        bindEvent()
        bindUI()
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)
    }


    override fun newState(state: CartInfoState) {

        cartInfoState = state

        setupRecycleViewAdapter()
    }

    private fun setupRecycleViewAdapter() {
        this?.let {
            val layoutManager = LinearLayoutManager(this)
            val itemDecorator = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
            itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.line_divider)!!)

            cartListRecycleView.addItemDecoration( itemDecorator)
            cartListRecycleView.layoutManager = layoutManager as RecyclerView.LayoutManager?
            saleAdapter = SaleAdapter( cartInfoState.transactionData.InputSalesLine , object : SaleAdapter.onClickListener {
                override fun onClickCheckAll() {

                    for( i in cartInfoState.transactionData.InputSalesLine.indices )
                    {
                        cartInfoState.transactionData.InputSalesLine[i].isSelectedSalePerson = true
                    }

                    saleAdapter.notifyDataSetChanged()
                }
                override fun onClickUnCheckAll() {

                    for( i in cartInfoState.transactionData.InputSalesLine.indices )
                    {
                        cartInfoState.transactionData.InputSalesLine[i].isSelectedSalePerson = false
                    }

                    saleAdapter.notifyDataSetChanged()
                }

                override fun onSelect(position: Int) {

                    cartInfoState.transactionData.InputSalesLine[position].isSelectedSalePerson = true
                    saleAdapter.notifyDataSetChanged()
                }

                override fun onUnSelect(position: Int) {

                    cartInfoState.transactionData.InputSalesLine[position].isSelectedSalePerson = false
                    saleAdapter.notifyDataSetChanged()
                }

                override fun onShowButtonNext() {
                    btnNext.visibility = View.VISIBLE
                }

                override fun onHideButtonNext() {
                    btnNext.visibility = View.GONE
                }

            } )
            saleAdapter.initial()

            if(!isModeRemoveSale())
                saleAdapter.setSale(salePersonModel)

            saleAdapter.setModeSale(isModeRemoveSale())

            cartListRecycleView.adapter = saleAdapter
        }
    }

    private fun isModeRemoveSale():Boolean
    {
        return (modeSale == "remove")
    }

    private fun bindUI()
    {
        if(isModeRemoveSale())
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                navigatorView.iconDrawable = getDrawable(R.drawable.ico_close)
            }
        }
    }

    private fun bindEvent() {
        navigatorView?.setListener{
            finish()
        }

        btnNext.visibility = View.GONE
        btnNext?.setListener{
            this?.let {

                for( i in cartInfoState.transactionData.InputSalesLine.indices )
                {
                    if( isModeRemoveSale() )
                    {
                        if( cartInfoState.transactionData.InputSalesLine[i].isSelectedSalePerson )
                        {
                            cartInfoState.transactionData.InputSalesLine[i].isSelectedSalePerson = false //set false กลับ

                            cartInfoState.transactionData.InputSalesLine[i].clearSalesPersonData()
                        }
                        else
                        {
                            cartInfoState.transactionData.InputSalesLine[i].isSelectedSalePerson = true //set true กลับ
                        }
                    }
                    else{
                        if( cartInfoState.transactionData.InputSalesLine[i].isSelectedSalePerson )
                        {
                            cartInfoState.transactionData.InputSalesLine[i].setSalesPersonData(salePersonModel)
                        }
                    }
                }

                mainStore.dispatch(MatchingSalePersonWithProduct(cartInfoState.transactionData.InputSalesLine))
                finish()
            }
        }

        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }
        }
    }
}