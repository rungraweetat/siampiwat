package com.siampiwat.MenuCart

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.Gson
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.Model.CalSalesLineModel
import com.siampiwat.R
import kotlinx.android.synthetic.main.activity_detail_calculate_product.*
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.activity_detail_calculate_product.view.*


class DetailCalculateProduct : AppCompatActivity() {

    lateinit var data:String
    lateinit var calSalesLineInfo:CalSalesLineModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_calculate_product)

        var bundle = intent.extras

        if( bundle == null ){
            finish()
            return
        }

        if( !bundle.isEmpty )
        {
            data = bundle.getString("data")
        }

        if( data.isEmpty() ){
            finish()
            return
        }

        calSalesLineInfo = Gson().fromJson(data , CalSalesLineModel::class.java)
        bindData()
    }

    private fun bindData()
    {
        navigatorView.setListener {
            finish()
        }

        txtProductDescription.text = calSalesLineInfo.ItemDescription
        txtProductBarcode.text = calSalesLineInfo.ItemBarcode
        txtProductId.text = calSalesLineInfo.ItemId
        txtProductQty.text = calSalesLineInfo.Qty.toString2Digit()
        txtProductSaleName.text = calSalesLineInfo.SalesPersonName
        txtProductPrice.text = calSalesLineInfo.Price.toString2Digit()
        txtNetPrice.text = calSalesLineInfo.NetAmountInclTax.toString2Digit()

        calSalesLineInfo.LineDiscount.forEach {
            val llTemplateDiscount = LayoutInflater.from(this@DetailCalculateProduct).inflate(R.layout.item_discount_calculate_product, null)
            val textDiscountName =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountName)
            val textDiscountAmount =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountAmount)

            textDiscountName.text = it.DiscountName
            textDiscountAmount.text = "-" + it.DiscountAmount.toString2Digit()

            llRootProduct.addView(llTemplateDiscount)
        }
    }
}