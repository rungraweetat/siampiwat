package com.siampiwat.MenuCart

import android.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.Model.ManualLineDiscountModel
import com.siampiwat.R
import com.siampiwat.State.Action.AddManualLineDiscount
import com.siampiwat.State.Action.ChangePrice
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.mainStore
import kotlinx.android.synthetic.main.activity_chang_price_line_product.*
import org.greenrobot.eventbus.EventBus
import org.rekotlin.StoreSubscriber

class ChangPriceLineProductActivity : BaseOnActivity() , StoreSubscriber<CartInfoState> {

    var itemId = ""
    override fun newState(state: CartInfoState) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chang_price_line_product)

        mainStore.unsubscribe(this)
        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }
        }

        try {
            itemId = intent.getStringExtra("ItemId")

        } catch(ex: Exception) {
            AlertDialog.Builder(this)
                    .setMessage("Don't have data")
                    .setPositiveButton("OK") { dialog, which ->
                        finish()
                    }
                    .setCancelable(false)
                    .show()
            return
        }

        bottomView.setListener{
            var value = edtChangePriceSurvey.text.toString()
            if( TextUtils.isEmpty(value) )
            {
                mainStore.dispatch(ChangePrice(0.0,itemId))
            }
            else{
                mainStore.dispatch(ChangePrice(value.toDouble(),itemId))
            }


            finish()
        }

        navigatorView.setListener {
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)
    }
}
