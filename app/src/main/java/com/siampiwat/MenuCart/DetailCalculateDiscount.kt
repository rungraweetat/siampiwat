package com.siampiwat.MenuCart

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.widget.TextView
import com.google.gson.Gson
import com.siampiwat.ClassOfDiscount
import com.siampiwat.R
import kotlinx.android.synthetic.main.activity_detail_calculate_discount.*
import com.google.gson.reflect.TypeToken
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.Model.*


class DetailCalculateDiscount : AppCompatActivity() {

    var classOfDiscount:Int = 0
    lateinit var data:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_calculate_discount)

        var bundle = intent.extras

        if( bundle == null ){
            finish()
            return
        }

        if( !bundle.isEmpty )
        {
            data = bundle.getString("data")
            classOfDiscount = bundle.getInt("classOfDiscount")
        }

        if( data.isEmpty() ){
            finish()
            return
        }


        bindData()
    }

    private fun bindData()
    {
        navigatorView.setListener {
            finish()
        }
        var title = ""


        if(classOfDiscount == ClassOfDiscount.DiscountBarcodeModel){
            val discountData = Gson().fromJson<ArrayList<DiscountBarcodeModel>>(data , object : TypeToken<ArrayList<DiscountBarcodeModel>>() {}.type)
            discountData.forEach {
                val llTemplateDiscount = LayoutInflater.from(this@DetailCalculateDiscount).inflate(R.layout.item_discount_calculate_product, null)
                val textDiscountName =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountName)
                val textDiscountAmount =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountAmount)

                textDiscountName.text = it.Name

                llRootDiscount.addView(llTemplateDiscount)
            }

            title = "ส่วนลดบาร์โค๊ด"
        }
        else if(classOfDiscount == ClassOfDiscount.BottomLineDiscountModel){
            val discountData = Gson().fromJson<ArrayList<BottomLineDiscountModel>>(data , object : TypeToken<ArrayList<BottomLineDiscountModel>>() {}.type)
            discountData.forEach {
                val llTemplateDiscount = LayoutInflater.from(this@DetailCalculateDiscount).inflate(R.layout.item_discount_calculate_product, null)
                val textDiscountName =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountName)
                val textDiscountAmount =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountAmount)

                textDiscountName.text = it.DiscountName
                textDiscountAmount.text = it.DiscountAmount.toString2Digit()
                llRootDiscount.addView(llTemplateDiscount)
            }

            title = "ส่วนลด"
        }
        else if(classOfDiscount == ClassOfDiscount.CouponDiscountModel){
            val discountData = Gson().fromJson<ArrayList<CouponDiscountModel>>(data , object : TypeToken<ArrayList<CouponDiscountModel>>() {}.type)
            discountData.forEach {
                val llTemplateDiscount = LayoutInflater.from(this@DetailCalculateDiscount).inflate(R.layout.item_discount_calculate_product, null)
                val textDiscountName =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountName)
                val textDiscountAmount =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountAmount)

                textDiscountName.text = it.LabelTH
                textDiscountAmount.text = it.CouponDiscountAmount

                llRootDiscount.addView(llTemplateDiscount)
            }

            title = "ส่วนลดตูปอง"
        }
        else if(classOfDiscount == ClassOfDiscount.RedemptionDiscountModel){
            val discountData = Gson().fromJson(data , RedemptionDiscountModel::class.java)

            val llTemplateDiscount = LayoutInflater.from(this@DetailCalculateDiscount).inflate(R.layout.item_discount_calculate_product, null)
            val textDiscountName =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountName)
            val textDiscountAmount =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountAmount)

            textDiscountName.text = "แลกคะแนนสมาชิก"
            textDiscountAmount.text = discountData.DiscountAmount.toString2Digit()
            llRootDiscount.addView(llTemplateDiscount)

            title = "ส่วนลด"
        }
        else if(classOfDiscount == ClassOfDiscount.SubTotalDiscountModel){
            val discountData = Gson().fromJson<ArrayList<SubTotalDiscountModel>>(data , object : TypeToken<ArrayList<SubTotalDiscountModel>>() {}.type)
            discountData.forEach {
                val llTemplateDiscount = LayoutInflater.from(this@DetailCalculateDiscount).inflate(R.layout.item_discount_calculate_product, null)
                val textDiscountName =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountName)
                val textDiscountAmount =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountAmount)

                textDiscountName.text = it.LabelTH
                textDiscountAmount.text = it.DiscountAmount?.toString2Digit()!!

                llRootDiscount.addView(llTemplateDiscount)
            }

            title = "ส่วนลด"
        }
        else if(classOfDiscount == ClassOfDiscount.ManualDiscount ){
            val discountData = Gson().fromJson(data , ManualLineDiscountModel::class.java)

            val llTemplateDiscount = LayoutInflater.from(this@DetailCalculateDiscount).inflate(R.layout.item_discount_calculate_product, null)
            val textDiscountName =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountName)
            val textDiscountAmount =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountAmount)

            textDiscountName.text = "ลดพิเศษ"
            textDiscountAmount.text = discountData.DiscountValue.toString2Digit()
            llRootDiscount.addView(llTemplateDiscount)

            title = "ส่วนลด"
        }
        else if(classOfDiscount == ClassOfDiscount.RequestManualTotalDiscount ){
            val discountData = Gson().fromJson(data , RequestManualTotalDiscount::class.java)

            val llTemplateDiscount = LayoutInflater.from(this@DetailCalculateDiscount).inflate(R.layout.item_discount_calculate_product, null)
            val textDiscountName =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountName)
            val textDiscountAmount =  llTemplateDiscount.findViewById<TextView>(R.id.txtDiscountAmount)

            textDiscountName.text = "ลดพิเศษ"
            textDiscountAmount.text = discountData.DiscountValue.toString2Digit()
            llRootDiscount.addView(llTemplateDiscount)

            title = "ส่วนลด"
        }
        navigatorView.navigatorTitle =title

    }
}