package com.siampiwat.MenuCart

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import com.google.gson.Gson
import com.siampiwat.Adapter.CheckTransactionAdapter
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.DetailProduct.DetailProductActivity
import com.siampiwat.ITEM_TYPE
import com.siampiwat.Model.*
import com.siampiwat.Payment.PaymentMethodActivity
import com.siampiwat.R
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.mainStore
import kotlinx.android.synthetic.main.activity_check_transaction.*

import org.rekotlin.StoreSubscriber

class CheckListTransactionActivity : BaseOnActivity(), StoreSubscriber<CartInfoState> {


    private var transactionData: TransactionDataModel? = null

    private lateinit var checkTransactionAdapter: CheckTransactionAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_transaction)
        bindRecycleView()
        bindEvent()
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)
    }


    override fun newState(state: CartInfoState) {
        checkTransactionAdapter.transactionData = state.transactionDataCalculate
        checkTransactionAdapter.notifyDataSetChanged()

    }


    private fun bindEvent() {
        navigatorView?.setListener{
            finish()
        }

        btnNext?.setListener{
            this.let {
                val i = Intent(it, PaymentMethodActivity::class.java)
                startActivity(i)
            }
        }

        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }.skipRepeats()
        }
    }

    private fun bindRecycleView(){
        checkTransactionAdapter = CheckTransactionAdapter(transactionData, object : CheckTransactionAdapter.onClickListener{
            override fun onClickCustomer(customerModel: LoyaltyCardModel?) {
                var intent = Intent( this@CheckListTransactionActivity , CustomerDetailActivity::class.java )
                val model = Gson().toJson(customerModel)
                intent.putExtra("data",model)

                startActivity(intent)
            }

            override fun onClickProductInfo(calculateInputLinesSale: CalSalesLineModel) {
                val i = Intent(this@CheckListTransactionActivity, DetailCalculateProduct::class.java)
                val detailCalculateProduct = Gson().toJson(calculateInputLinesSale)
                i.putExtra("data", detailCalculateProduct)
                startActivity(i)
            }

            override fun onClickDiscount(discountItemSelected: Any, discountItemGroup: Any, classOfDiscount: Int) {
                val i = Intent(this@CheckListTransactionActivity, DetailCalculateDiscount::class.java)
                val discountItemGroupInfo = Gson().toJson(discountItemGroup)
                i.putExtra("data", discountItemGroupInfo)
                i.putExtra("classOfDiscount", classOfDiscount)
                startActivity(i)
            }
        })

        val itemDecorator = DividerItemDecoration(this@CheckListTransactionActivity, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(this@CheckListTransactionActivity, R.drawable.line_divider)!!)

        recycleView.apply {
            layoutManager = LinearLayoutManager(this@CheckListTransactionActivity)
            addItemDecoration(itemDecorator)
            adapter = checkTransactionAdapter
        }

        checkTransactionAdapter.notifyDataSetChanged()
    }

}