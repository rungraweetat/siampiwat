package com.siampiwat.MenuCart

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.widget.*
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.siampiwat.API.DiscountApi
import com.siampiwat.API.RoyaltyApi
import com.siampiwat.API.TransactionApi
import com.siampiwat.Adapter.BillDiscountAdapter
import com.siampiwat.Adapter.DiscountBarcodeAdapter
import com.siampiwat.Adapter.DiscountCouponAdapter
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.BaseObject.SearchActivity
import com.siampiwat.EventBus.M2102ManageDiscountPresurveyModel
import com.siampiwat.EventBus.M2800PresurveyModel
import com.siampiwat.EventBus.PreSurveyCouponDiscountsModel
import com.siampiwat.EventBus.RedemtionDiscountPresurvey
import com.siampiwat.Extention.gone
import com.siampiwat.Extention.indeterminateProgress
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.Extention.visible
import com.siampiwat.Lib.SwipeController
import com.siampiwat.ManualTotalDiscount.MainManualTotalDiscountActivity
import com.siampiwat.Model.*
import com.siampiwat.R
import com.siampiwat.State.Action.*
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.Survey.SurveyActivity
import com.siampiwat.Survey.SurveyConst
import com.siampiwat.mainStore
import com.siampiwat.userInfo
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_discount.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.longToast
import org.rekotlin.StoreSubscriber
import java.util.*


class DiscountActivity : BaseOnActivity(), StoreSubscriber<CartInfoState> {

    private lateinit var discountBarcodeAdapter: DiscountBarcodeAdapter
    private lateinit var discountCouponAdapter: DiscountCouponAdapter

    var discountBarcodeItemList: ArrayList<DiscountBarcodeModel> = ArrayList()
    var discountCouponItemList: ArrayList<CouponDiscountModel> = ArrayList()
    var transactionData: TransactionDataModel? = null
    var points = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discount)

        setupRecycleViewAdapter()
        setupLayout()
        setupListener()

        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }
        }
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)

//        if(transactionData?.ManualTotalDiscount != null) {
//            toggleVisible(flContentSpacialDiscount, imgSpacialDiscountArrow)
//        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)
        if (EventBus.getDefault().isRegistered(this)) EventBus.getDefault().unregister(this)
    }

    override fun newState(state: CartInfoState) {
        transactionData = state.transactionData
        discountBarcodeItemList.clear()
        discountCouponItemList.clear()

        discountBarcodeItemList.addAll(state.transactionData.DiscountBarcodes)
        discountCouponItemList.addAll(state.transactionData.CouponDiscount)

        discountBarcodeAdapter.notifyDataSetChanged()
        discountCouponAdapter.notifyDataSetChanged()

        updateContentSpacialDiscount()
    }

    private fun setupRecycleViewAdapter() {

        val itemDecorator = DividerItemDecoration(this@DiscountActivity, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(this@DiscountActivity, R.drawable.line_divider)!!)

        discountBarcodeAdapter = DiscountBarcodeAdapter(discountBarcodeItemList)
        discountCouponAdapter = DiscountCouponAdapter(discountCouponItemList)


        recycleViewBarcodeDiscount?.apply {
            val llLayoutManager = LinearLayoutManager(this@DiscountActivity)
            layoutManager = llLayoutManager
            addItemDecoration(itemDecorator)
            adapter = discountBarcodeAdapter

            discountBarcodeAdapter.notifyDataSetChanged()
        }

        recycleViewCouponDiscount?.apply {
            val llLayoutManager = LinearLayoutManager(this@DiscountActivity)
            layoutManager = llLayoutManager
            addItemDecoration(itemDecorator)
            adapter = discountCouponAdapter

            discountCouponAdapter.notifyDataSetChanged()
        }
    }

    private fun setupListener() {

        navigatorView.setListener {
            finish()
        }

        btnNext.setListener {
            this.let {

                var redemptionDiscount = RedemptionDiscountModel(0.0 ,
                        false ,
                        0.0 ,
                        0.0 ,
                        0.0 ,
                        0.0)

                var currentDiscountPoints = 0.0
                if( txtCurrentDiscountPoint.visibility == View.VISIBLE )
                {
                    currentDiscountPoints = points
                }

                redemptionDiscount.BalanceBefRedemption = currentDiscountPoints
                redemptionDiscount.isPatialRedemption = this@DiscountActivity.chkPartialDiscountPoint.isChecked
                redemptionDiscount.DiscountPCT = this@DiscountActivity.spinnerDiscountPoint.selectedItem.toString().replace("%", "").toDouble()

                mainStore.dispatch(AddRedemption(redemptionDiscount))

                this.requestTransactionCalculate()
            }
        }

        flBarcode.setOnClickListener {
            toggleVisible(flContentBarcode, imgBarcodeArrow)
        }

        flCoupon.setOnClickListener {
            toggleVisible(flContentCoupon, imgCouponArrow)
        }

        flDiscountPoint.setOnClickListener {
            toggleVisible(flContentDiscountPoint, imgDiscountPointArrow)
        }

        flBillDiscount.setOnClickListener {

            toggleVisible(flContentBillDiscount, imgBillDiscountArrow)
        }

        flSpacialDiscount.setOnClickListener {
            toggleVisible(flContentSpacialDiscount, imgSpacialDiscountArrow)
        }

        llBtnAddCouponBarcode.setOnClickListener {

            this.requestCheckCouponDiscount()
        }

        llBtnAddDiscountBarcode.setOnClickListener {
            startActivityScanBarcode()
        }

        bindViewManualTotalDiscount()

        setupSwipeRecycleViewBarcodeDiscount()
        setupSwipeRecycleViewCouponDiscount()
    }

    private fun setupLayout() {


        userInfo?.InitScreenData?.RedemtionDiscounts?.let {

            var discounts = arrayListOf<String>()
            for (item in it) {
                discounts.add(item.DiscountPct.toString2Digit() + "%")
            }

            val adapterDiscount = ArrayAdapter<String>(this@DiscountActivity, android.R.layout.simple_spinner_item, discounts)
            spinnerDiscountPoint.adapter = adapterDiscount

            spinnerDiscountPoint.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            spinnerDiscountPoint.isEnabled = false
            chkPartialDiscountPoint.isEnabled = false
            txtCurrentDiscountPoint.visibility = View.GONE

            chkFullDiscountPoint.setOnCheckedChangeListener { compoundButton, b ->

                if (TextUtils.isEmpty(userInfo?.getDataPreSurvey("M2800"))) {
                    spinnerDiscountPoint.isEnabled = compoundButton.isChecked
                    chkPartialDiscountPoint.isEnabled = compoundButton.isChecked
                } else {
                    if (compoundButton.isChecked) {
                        val i = Intent(this, SurveyActivity::class.java)
                        i.putExtra(SurveyConst.KEY_SURVEY_JSON, userInfo?.getDataPreSurvey("M2800"))
                        i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.ReemtionDiscountPresurvey)
                        startActivity(i)
                    }
                }
            }

            chkPartialDiscountPoint.setOnCheckedChangeListener { compoundButton, b ->

                if (compoundButton.isChecked) {

                    if (transactionData?.LoyaltyCard != null) {
                        val i = Intent(this, SurveyActivity::class.java)
                        i.putExtra(SurveyConst.KEY_SURVEY_JSON, userInfo?.getDataPreSurvey("M2800"))
                        i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2800_PreSurvey)
                        startActivity(i)
                    }
                } else {
                    txtCurrentDiscountPoint.text = ""
                    txtCurrentDiscountPoint.visibility = View.GONE
                }
            }
        }

        userInfo?.InitScreenData?.SubtotalDiscounts?.let {

            val itemDecorator = DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL)
            itemDecorator.setDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.line_divider)!!)

            val billDiscountAdapter = BillDiscountAdapter(it, object : BillDiscountAdapter.OnClickListener {
                override fun onCheckChange(position: Int, subTotalDiscount: SubTotalDiscountModel, isChecked: Boolean) {
//                    if (isChecked) {
//                        mainStore.dispatch(AddSubTotalDiscount(subTotalDiscount))
//                    } else {
//                        mainStore.dispatch(RemoveSubTotalDiscount(subTotalDiscount))
//                    }

                    if (isChecked) {
//                        mainStore.dispatch(AddSubTotalDiscount(subTotalDiscount))
                        requestDiscountBarcode( subTotalDiscount.DiscountBarcode )
                    } else {
                        mainStore.dispatch(RemoveDiscountBarcodeById(subTotalDiscount.DiscountBarcode))
//                        mainStore.dispatch(RemoveSubTotalDiscount(subTotalDiscount))
                    }
                }
            })

            recycleViewBillDiscount.apply {
                layoutManager = LinearLayoutManager(applicationContext)
                addItemDecoration(itemDecorator)
                adapter = billDiscountAdapter
            }

            billDiscountAdapter.notifyDataSetChanged()
        }

        userInfo?.InitScreenData?.ManualTotalDiscounts?.let {
            for (item in it) {
                println("=====ManualTotalDiscounts==> ${item.LabelTH}")
            }
        }

    }

    private fun toggleVisible(viewContent: View, viewImageArrow: ImageView) {
        if (viewContent.visibility == View.GONE) {
            viewContent.visible(true)
            viewImageArrow.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ico_arrow_up))
        } else {
            viewContent.gone(true)
            viewImageArrow.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ico_arrow_down))
        }
    }

    private fun setupSwipeRecycleViewBarcodeDiscount() {
        val swipe = object : SwipeController(this@DiscountActivity, recycleViewBarcodeDiscount) {

            override fun instantiateUnderlayButton(viewHolder: RecyclerView.ViewHolder, underlayButtons: java.util.ArrayList<UnderlayButton>) {

                underlayButtons.add(UnderlayButton(getString(R.string.label_item_delete), ResourcesCompat.getDrawable(getResources(), R.drawable.ico_trash, null), ContextCompat.getColor(this@DiscountActivity, R.color.item_delete), object : UnderlayButtonClickListener {
                    override fun onClick(pos: Int) {
                        mainStore.dispatch(RemoveDiscountBarcode(pos))
                    }
                }))

            }
        }
    }

    private fun setupSwipeRecycleViewCouponDiscount() {
        val swipe = object : SwipeController(this@DiscountActivity, recycleViewCouponDiscount) {

            override fun instantiateUnderlayButton(viewHolder: RecyclerView.ViewHolder, underlayButtons: java.util.ArrayList<UnderlayButton>) {

                underlayButtons.add(UnderlayButton(getString(R.string.label_item_delete), ResourcesCompat.getDrawable(resources, R.drawable.ico_trash, null), ContextCompat.getColor(this@DiscountActivity, R.color.item_delete), object : UnderlayButtonClickListener {
                    override fun onClick(pos: Int) {
                        mainStore.dispatch(RemoveCouponDiscount(pos))
                    }
                }))
            }
        }
    }

    private fun startActivityScanCoupon() {

        val intent = Intent(this, SearchActivity::class.java)
        intent.putExtra("mode", SearchActivity.MODE_ADD_COUPON_DISCOUNT)
        intent.putExtra("page", SearchActivity.PAGE_SCANNER)
        startActivity(intent)
    }

    private fun startActivityScanBarcode()
    {
        val intent = Intent(this, SearchActivity::class.java)
        intent.putExtra("mode", SearchActivity.MODE_ADD_BAR_CODE_DISCOUNT)
        intent.putExtra("page", SearchActivity.PAGE_SCANNER)
        startActivity(intent)
    }

    private fun shoOptionCouponDiscountDialog() {
        val array: ArrayList<String> = arrayListOf()

        userInfo?.InitScreenData?.CouponDiscounts?.let {
            for (item in it) {
//                if (item.CouponOfferId.isNotEmpty()) {
//                    array.add(item.LabelTH)
//                }

                array.add(item.LabelTH)
            }
        }

        if (array.size > 0) {

            val builder = AlertDialog.Builder(this)

            builder.setTitle(getString(R.string.common_select))
            builder.setItems(array.toTypedArray()) { _, which ->
                val couponDiscountSelected = userInfo?.InitScreenData?.CouponDiscounts!![which]

//                val arLstSurvey = couponDiscountSelected.PreSurvey
//                val surveyJson = Gson().toJson(arLstSurvey)
//                if (arLstSurvey!!.size < 1) {
//                    this.startActivityScanCoupon()
//                } else {
//                    val intent = Intent(this, SurveyActivity::class.java)
//                    intent.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
//                    intent.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.CouponDiscount_PreSurvey)
//                    startActivity(intent)
//                }

                this.startActivityScanCoupon()
            }

            val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                    }
                    DialogInterface.BUTTON_NEGATIVE -> {
                    }
                    DialogInterface.BUTTON_NEUTRAL -> {
                    }
                }
            }
            builder.setNegativeButton(getString(R.string.common_cancel), dialogClickListener)

            val dialog = builder.create()
            dialog.show()
        } else {
            this.startActivityScanCoupon()
        }
    }

    private fun requestDiscountBarcode(barcode:String)
    {

        val dialog = indeterminateProgress("")
        dialog!!.show()

        Objects.requireNonNull<Single<Result<DiscountBarcodeResponseModel, FuelError>>>(DiscountApi.getDiscountBarcode(barcode)).subscribe(object : SingleObserver<Result<DiscountBarcodeResponseModel, FuelError>> {
            override fun onSubscribe(d: Disposable) {

            }

            override fun onSuccess(discountBarcodeResponseModelFuelErrorResult: Result<DiscountBarcodeResponseModel, FuelError>) {
                val productInfoModel = discountBarcodeResponseModelFuelErrorResult.component1()
                val Error = discountBarcodeResponseModelFuelErrorResult.component2()

                dialog!!.dismiss()
                if (Error == null && productInfoModel != null && productInfoModel.Error.size == 0) {

                    val discountBarcodeModel = productInfoModel.DiscountBarcode
                    mainStore.dispatch(AddDiscountBarcode(discountBarcodeModel))

                } else {
                    longToast(productInfoModel!!.ResponseMessage!!.Message.toString())
                }
            }

            override fun onError(e: Throwable) {
                dialog!!.dismiss()
            }
        })
    }

    @SuppressLint("CheckResult")
    private fun requestCheckCouponDiscount() {
        this.shoOptionCouponDiscountDialog()
    }

    @SuppressLint("CheckResult")
    private fun requestTransactionCalculate() {

        val dialog = this.indeterminateProgress("")
        dialog!!.show()

        TransactionApi
                .calculate("" , transactionData!!)
                .subscribe { Response ->
                    dialog.dismiss()
                    val (transactionDataResponse, Error) = Response
                    if (Error === null && !transactionDataResponse?.ResponseMessage!!.isError()) {
                        mainStore.dispatch(AddTransactionDataCalculate(transactionDataResponse.Transaction))
                        val i = Intent(this, CheckListTransactionActivity::class.java)
                        startActivity(i)



                    } else {
                        var errorMessage = ""

                        Error?.let {
                            errorMessage = it.exception.message.toString()
                        }

                        if( transactionDataResponse?.ResponseMessage != null )
                        {
                            if (transactionDataResponse.ResponseMessage.isError()) {
                                errorMessage = transactionDataResponse.ResponseMessage.Message!!
                            }
                        }

                        longToast(errorMessage)

//                        val i = Intent(this , PaymentMethodActivity::class.java)
//                        startActivity(i)
                    }
                }
    }

    private fun requestPointsBalance() {
        if (transactionData?.LoyaltyCard != null) {
            val dialog = this.indeterminateProgress("")

            val loyaltyCardModel = transactionData?.LoyaltyCard
            val cardNumber = loyaltyCardModel!!.CardNumber

            //Fix code
//            val cardNumber = "0000006918"
            RoyaltyApi.getPoint(cardNumber).subscribe(object : SingleObserver<Result<PointModel, FuelError>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onSuccess(productInfoModelFuelErrorResult: Result<PointModel, FuelError>) {
                    val productInfoModel = productInfoModelFuelErrorResult.component1()
                    val error = productInfoModelFuelErrorResult.component2()
                    var message = resources.getText(R.string.discount_activity_current_discount_point)


                    if (error == null && productInfoModel != null && productInfoModel.Error.size == 0) {
                        points = productInfoModel.Point

                        this@DiscountActivity.txtCurrentDiscountPoint.text = "$message $points"
                        this@DiscountActivity.txtCurrentDiscountPoint.visibility = View.VISIBLE
                    } else {
                        var errorMessage = ""

                        error?.let {
                            errorMessage = it.exception.message.toString()
                        }

                        if( productInfoModel?.ResponseMessage != null )
                        {
                            if (productInfoModel.ResponseMessage.isError()) {
                                errorMessage = productInfoModel.ResponseMessage.Message!!
                            }
                        }

                        longToast(errorMessage)
                    }

                    dialog!!.dismiss()
                }

                override fun onError(e: Throwable) {
                    dialog!!.dismiss()
                }
            })
        }
    }

    @Subscribe
    public fun onEvent(event: M2800PresurveyModel) {
        requestPointsBalance()

    }

    @Subscribe
    public fun onEvent(event: PreSurveyCouponDiscountsModel) {
        startActivityScanCoupon()

    }

//    @Subscribe
//    public fun onEvent(event: BarcodeDiscountsPreSurveyModel) {
//        startActivityScanBarcode()
//
//    }

    @Subscribe
    public fun onEvent(event: M2102ManageDiscountPresurveyModel) {

        var intent = Intent( this , MainManualTotalDiscountActivity::class.java )
        val data = Gson().toJson(event.getModel())
        intent.putExtra("data" , data)
        startActivity(intent)
    }

    @Subscribe
    public fun onEvent(event: RedemtionDiscountPresurvey) {
        spinnerDiscountPoint.isEnabled = true
        chkPartialDiscountPoint.isEnabled = true
    }

    private fun bindViewManualTotalDiscount() {
        flContentSpacialDiscount.removeAllViews()
        val arLstSurvey = userInfo?.InitScreenData!!.ManualTotalDiscounts
        for (i in arLstSurvey.indices) {
            var vLayout = layoutInflater.inflate(R.layout.layout_menu_total_discount, null)
            var btnManageDiscount = vLayout.findViewById(R.id.btnManageDiscount) as Button
            var tvDiscountAmount = vLayout.findViewById(R.id.tvDiscountAmount) as TextView

            btnManageDiscount.tag = arLstSurvey[i].DiscountOfferId
            btnManageDiscount.text = arLstSurvey[i].LabelTH
            btnManageDiscount.setTextColor(resources.getColor(R.color.white))
            btnManageDiscount.setOnClickListener {
                val dataSurvey = Gson().toJson(userInfo?.getPreSurvey("M2102"))
                val intent = Intent(this, SurveyActivity::class.java)
                var manualTotalDiscounts = Gson().toJson(arLstSurvey[i])

                intent.putExtra("ItemId", manualTotalDiscounts)
                intent.putExtra(SurveyConst.KEY_SURVEY_JSON, dataSurvey)
                intent.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2102ManageDiscountPresurvey)
                startActivity(intent)
            }

            flContentSpacialDiscount!!.addView(vLayout)
        }
    }

    private fun updateContentSpacialDiscount() {
        Log( "flContentSpacialDiscount.childCount = "+flContentSpacialDiscount.childCount )

        for (i in 0..(flContentSpacialDiscount.childCount-1)) {
            var vRootContainer = flContentSpacialDiscount.getChildAt(i)
            var tvDiscountAmount = vRootContainer.findViewById(R.id.tvDiscountAmount) as TextView
            var btnManageDiscount = vRootContainer.findViewById(R.id.btnManageDiscount) as Button

            if (btnManageDiscount.tag != null) {
                if (btnManageDiscount.tag.toString() == transactionData?.ManualTotalDiscount?.DiscountOfferID) {
                    var prefix = ""
                    if (transactionData!!.ManualTotalDiscount != null) {
                        prefix = if (transactionData!!.ManualTotalDiscount!!.DiscountAttribute == 0)
                            getString(R.string.manual_total_discount_activity_percent)
                        else
                            getString(R.string.manual_total_discount_activity_bath)

                        tvDiscountAmount.text = transactionData!!.ManualTotalDiscount!!.DiscountValue.toString() + " " + prefix
                    }
                }
                else
                {
                    tvDiscountAmount.text = ""
                }
            }

        }
    }
}
