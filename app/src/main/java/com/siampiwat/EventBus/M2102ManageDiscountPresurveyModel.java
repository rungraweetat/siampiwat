package com.siampiwat.EventBus;

import com.google.gson.Gson;
import com.siampiwat.Model.ManualTotalDiscount;

public class M2102ManageDiscountPresurveyModel {

    public ManualTotalDiscount model;
    public M2102ManageDiscountPresurveyModel( String manualTotalDiscountModel )
    {
        this.model = new Gson().fromJson(manualTotalDiscountModel , ManualTotalDiscount.class);
    }

    public ManualTotalDiscount getModel()
    {
        return model;
    }

}
