package com.siampiwat.DetailProduct

import android.os.Bundle
import com.google.gson.Gson
import com.omega_r.libs.OmegaCenterIconButton
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.R
import com.siampiwat.Adapter.TabAdapter
import com.siampiwat.BaseObject.BottomView
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.Extention.toast
import com.siampiwat.State.Action.AddProductInCart
import com.siampiwat.mainStore
import kotlinx.android.synthetic.main.activity_detail_product.*
import kotlinx.android.synthetic.main.layout_button_ok.*
import kotlinx.android.synthetic.main.layout_button_ok.view.*
import android.view.Gravity
import android.widget.Toast
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.siampiwat.API.ItemApi
import com.siampiwat.EventBus.AddProductFromSearchProductModel
import com.siampiwat.Extention.convertToString
import com.siampiwat.Extention.indeterminateProgress
import com.siampiwat.Model.*
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import org.jetbrains.anko.toast
import org.greenrobot.eventbus.EventBus




class DetailProductActivity : BaseOnActivity() {

    lateinit var data:String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_product)

        var bundle = intent.extras

        if( bundle == null ){
            finish()
            return
        }

        if( !bundle.isEmpty )
        {
            data = bundle.getString("data")
        }

        if( data.isEmpty() ){
            finish()
            return
        }

        var model = Gson().fromJson(data , SearchProductItemModel::class.java)
        requestProductInfo(model.ProductId,0)

        llHeader.setListener{
            hideKeyboard()
            finish()
        }

        hideKeyboard()
    }

    private fun toastMessage( message:String )
    {
        val toast = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
//        toast.setGravity(Gravity.BOTTOM or Gravity.LEFT, 0, 0)
        toast.show()
    }

    private fun requestProductInfo(barcode: String, sourceOfSearch: Int) {

        val dialog = this@DetailProductActivity.indeterminateProgress("")
        dialog!!.show()

        ItemApi.getProductInfo(barcode).subscribe(object : SingleObserver<Result<ProductInfoResponseModel, FuelError>> {
            override fun onSubscribe(d: Disposable) {

            }

            override fun onSuccess(productInfoModelFuelErrorResult: Result<ProductInfoResponseModel, FuelError>) {
                val productInfoModel = productInfoModelFuelErrorResult.component1()
                val Error = productInfoModelFuelErrorResult.component2()

                var model = Gson().toJson(productInfoModel , ProductInfoResponseModel::class.java)
                dialog!!.dismiss()
                if (Error == null && productInfoModel != null && productInfoModel.Error.size == 0) {
                    val adapter = TabAdapter(supportFragmentManager)
                    adapter.setData(model)
                    adapter.setMode(TabAdapter.MODE_PRODUCT_DETAIL)
                    adapter.setActivity(this@DetailProductActivity)
                    pager.adapter = adapter
                    tabLayout.setupWithViewPager(pager)

                    bottomView.setListener {
                        val searchProductItemInfoModel = Gson().fromJson(data, SearchProductItemModel::class.java)
                        val productInfo = searchProductItemInfoModel.toItemInfoModel()

                        val itemInfoModel = ItemInfoModel(productInfo.ItemId,
                                productInfo.ItemBarcode,
                                searchProductItemInfoModel.ProductName,
                                "",
                                false,
                                searchProductItemInfoModel.Price.convertToString(),
                                null,
                                true)

                        mainStore.dispatch(AddProductInCart(itemInfoModel))
                        toastMessage(resources.getString(R.string.toast_message_add_product_success))

                        EventBus.getDefault().post(AddProductFromSearchProductModel())
                    }
                }
            }

            override fun onError(e: Throwable) {
                dialog!!.dismiss()
            }
        })
    }
}
