package com.siampiwat.DetailProduct

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.Model.ItemInfoModel
import com.siampiwat.Model.ProductInfoModel
import com.siampiwat.Model.ProductInfoResponseModel
import com.siampiwat.Model.SearchProductItemModel

import com.siampiwat.R
import kotlinx.android.synthetic.main.fragment_tab_detail_product.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class TabDetailProductFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var mActivity:Activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

//            this.mActivity = this!!.activity!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_tab_detail_product, container, false)
    }

    companion object {
        fun newInstance(param1: String, param2: String) =
                TabDetailProductFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var data = Gson().fromJson(param1 , ProductInfoResponseModel::class.java)
        var model = data.ProductInfo

        tvIdProduct.text = model.ProductId
        tvName.text = model.ProductName
        tvPrice.text = model.Price.toString2Digit()

        tvCategory.text = getCategory( model )
        tvBarCode.text = getBarcode( model )
    }

    private fun getCategory( model: ProductInfoModel ):String
    {
        var category = ""
        for( i in model.ProductCategory!!.indices )
        {
            category += model.ProductCategory!![i].Class
            category += "\n"
            category += model.ProductCategory!![i].Department
            category += "\n"
            category += model.ProductCategory!![i].Division
            category += "\n"
            category += model.ProductCategory!![i].SubClass
            category += "\n"
            category += model.ProductCategory!![i].SubDepartment
            category += "\n"
        }

        return category

    }

    private fun getBarcode( model: ProductInfoModel ):String
    {
        var barcode = ""
        for( i in model.ProductBarcode!!.indices )
        {
            barcode += model.ProductBarcode!![i].Barcode
            barcode += "\n"
        }

        return barcode

    }
}
