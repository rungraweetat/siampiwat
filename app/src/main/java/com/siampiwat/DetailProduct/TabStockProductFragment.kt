package com.siampiwat.DetailProduct


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.gson.Gson
import com.siampiwat.Model.ProductInfoModel
import com.siampiwat.Model.ProductInfoResponseModel
import com.siampiwat.Model.SearchProductItemModel

import com.siampiwat.R
import kotlinx.android.synthetic.main.fragment_tab_detail_product.*
import kotlinx.android.synthetic.main.fragment_tab_stock_product.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class TabStockProductFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab_stock_product, container, false)
    }


    companion object {

        fun newInstance(param1: String, param2: String) =
                TabStockProductFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var model = Gson().fromJson(param1, ProductInfoResponseModel::class.java)
        var productInfo = model.ProductInfo
        for (i in productInfo.Inventory!!.indices) {
            var view = LayoutInflater.from(activity).inflate(R.layout.layout_stock_product, null)

            var tvBranch = view.findViewById(R.id.tvBranch) as TextView
            var tvAmount = view.findViewById(R.id.tvAmount) as TextView

            tvBranch.text = productInfo.Inventory!![i].StoreName
            tvAmount.text = productInfo.Inventory!![i].Qty.toString()
            llContainer.addView(view)
        }

    }
}
