
package com.siampiwat.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.siampiwat.Model.CustomerModel;
import com.siampiwat.Model.LoyaltyCardModel;
import com.siampiwat.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by user on 2/6/2016.
 */
public class ResultCustomerAdapter extends RecyclerView.Adapter<ResultCustomerAdapter.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;
    private  List<LoyaltyCardModel> arrLstItem = null;

    private Activity mActivity;

    public interface OnItemClickListener {
        public void onItemClick(int position, LoyaltyCardModel model);
        public void onSelected(int position, LoyaltyCardModel model);
    }

    ResultCustomerAdapter(OnItemClickListener onItemClickListener) {

        mOnItemClickListener = onItemClickListener;
    }

    public void setListenerItemClick(OnItemClickListener onItemListener) {
        mOnItemClickListener = onItemListener;
    }

    public void setItem(ArrayList<LoyaltyCardModel> arLstAlbum) {
        this.arrLstItem = arLstAlbum;
    }

    public ResultCustomerAdapter(Activity activity ) {

        mActivity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                               .inflate(R.layout.item_result_search_customer,
                                       viewGroup,
                                       false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        if( arrLstItem.size() == 0 ) return;

        viewHolder.tvNameCustomer.setText(arrLstItem.get(i).getCustomerName());
        viewHolder.tvTypeCustomer.setText(""+arrLstItem.get(i).getCardType());
        viewHolder.ivSelectedCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( mOnItemClickListener != null )
                {
                    mOnItemClickListener.onSelected(i,arrLstItem.get(i));
                    setCheck(viewHolder , i);
                    notifyDataSetChanged();
                }
                else
                {
                    Log.e("ResulrCustomerAdapter","Please setListener first!!");
                }
            }
        });

        if( arrLstItem.get(i).isSelected() ) {
            viewHolder.ivSelectedCustomer.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_checked));
        }
        else {
            viewHolder.ivSelectedCustomer.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_unchecked));
        }
    }

    private void setCheck(ViewHolder vh , int position )
    {
        for( int n=0 ;n<arrLstItem.size();n++ )
        {
            arrLstItem.get(n).setSelected(false);
        }

        if( arrLstItem.size() > position )
        {
            arrLstItem.get(position).setSelected(true);
            vh.ivSelectedCustomer.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_checked));
        }
    }

    @Override
    public int getItemCount() {
        return arrLstItem.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvNameCustomer;
        private TextView tvTypeCustomer;
        private ImageView ivSelectedCustomer;


        ViewHolder(View v) {
            super(v);

            tvNameCustomer = v.findViewById(R.id.tvNameCustomer);
            tvTypeCustomer= v.findViewById(R.id.tvTypeCustomer);
            ivSelectedCustomer= v.findViewById(R.id.ivSelectedCustomer);
        }
    }

}
