package com.siampiwat.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import com.siampiwat.BaseObject.SearchActivity
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.ITEM_TYPE
import com.siampiwat.Model.InputSalesLineModel
import com.siampiwat.Model.LoyaltyCardModel
import com.siampiwat.R
import kotlinx.android.synthetic.main.fragment_cart.view.*
import kotlinx.android.synthetic.main.item_cart_header.view.*
import kotlinx.android.synthetic.main.item_cart_product.view.*

class CartNewAdapter(val productList: ArrayList<InputSalesLineModel>, val onClick: onClickListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val SIZE_CUSTOMER = 1

    var customerModel:LoyaltyCardModel? = null
    private var mode:Int = SearchActivity.MODE_SEARCH_PRODUCTION

    public fun setCustomer( customerModel: LoyaltyCardModel? )
    {
        this.customerModel = customerModel
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if( customerModel == null && productList == null) return

        if( getItemViewType(position) ==  ITEM_TYPE.CUSTOMER )
        {
            var holderCustomer: vhCustomer = holder as vhCustomer
            holderCustomer.memberIdCard.text = customerModel!!.CardNumber
            holderCustomer.memberName.text = customerModel!!.CustomerName
            holderCustomer.point.text = customerModel!!.BalancePoints.toString()
            holderCustomer.llRootCustomer.setOnClickListener {
                onClick.onClickCustomer(customerModel)
            }
        }
        else
        {
            var index = position
            if( customerModel != null ){
                //กรณีที่เพิ่มลูกค้าแล้ว ให้เอาเอา position -1 เพราะ index จะเกิน( เพราะจาก getItemCount ไป +1 ไว้ )
                index = position - 1
            }
            var holderProduct: vhProduct = holder as vhProduct
            val product = productList[index]
            holderProduct.name.text = product.ItemDescription
            holderProduct.price.text = product.Price.toString2Digit()
            holderProduct.barcode.text = product.ItemBarcode
            holderProduct.amount.text = product.Qty.toString()

            if( (product.SalesPersonName ==null && product.SalesPersonId == null) ||
                    (TextUtils.isEmpty(product.SalesPersonId) && TextUtils.isEmpty(product.SalesPersonName)))
            {
                holderProduct.llSeller.visibility = View.GONE
            }
            else{
                holderProduct.llSeller.visibility = View.VISIBLE
                holderProduct.tvSeller.text = product.SalesPersonName
            }

            if ( product.ManualLineDiscount == null){
                holderProduct.llManualLineDiscount.visibility = View.GONE
            }
            else{
                holderProduct.llManualLineDiscount.visibility = View.VISIBLE

                var manualLineDiscount = product.ManualLineDiscount?.DiscountValue
                if( product.ManualLineDiscount?.DiscountAttribute == 0){ // 0 = Percentage, 1 = Baht
                    manualLineDiscount =  (product.Qty * product.Price) * (product.ManualLineDiscount?.DiscountValue!! / 100 )
                }
                holderProduct.txtManualLineDiscount.text = "-" + manualLineDiscount?.toString2Digit()
            }

            if( product.Qty >= 2 )
                holderProduct.btnDecrease.setImageResource(R.drawable.ico_remove_item)
            else
                holderProduct.btnDecrease.setImageResource(R.drawable.ico_remove_item_none)

            if( index == 0 )
                holderProduct.title.visibility = View.VISIBLE
            else
                holderProduct.title.visibility = View.GONE

            holderProduct.btnIncrease.setOnClickListener {
                onClick.onClickIncrease(index)
            }

            holderProduct.btnDecrease.setOnClickListener {
                onClick.onClickDecrease(index)
            }

            holderProduct.itemView.setOnClickListener {
                onClick.onClickItem(index)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {

        return if( position == 0 ) {
            if( customerModel == null ) ITEM_TYPE.PRODUCT
            else  ITEM_TYPE.CUSTOMER
        }
        else {  ITEM_TYPE.PRODUCT }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        var v : View?
        return if( viewType == ITEM_TYPE.PRODUCT ){
            v = LayoutInflater.from(parent.context).inflate(R.layout.item_cart_product, parent, false)
            vhProduct(itemView = v)
        }
        else {
            v = LayoutInflater.from(parent.context).inflate(R.layout.item_cart_header, parent, false)
            vhCustomer(itemView = v)
        }
    }

    override fun getItemCount(): Int {

        return if (customerModel == null) productList.size
        else (productList.size + SIZE_CUSTOMER)

    }

    class vhProduct(itemView: View): RecyclerView.ViewHolder(itemView){

        val title = itemView.txtTitle!!
        val name = itemView.txtName!!
        val price = itemView.textPrice!!
        val barcode = itemView.txtBarcode!!
        val amount = itemView.txtAmount!!
        val tvSeller = itemView.tvSeller!!
        val llSeller = itemView.llSeller!!
        val llManualLineDiscount = itemView.llManualLineDiscount!!
        val txtManualLineDiscount = itemView.txtManualLineDiscount!!
        val btnIncrease = itemView.btnIncrease!!
        val btnDecrease = itemView.btnDecrease!!
    }

    class vhCustomer(itemView: View): RecyclerView.ViewHolder(itemView){
        val memberIdCard = itemView.textMemberIdCard!!
        val memberName = itemView.txtMemberName!!
        val point = itemView.txtPoint!!
        val llRootCustomer = itemView.llRootCustomer!!

    }

    interface onClickListener {
        fun onClickIncrease(position: Int)
        fun onClickDecrease(position: Int)
        fun onClickItem(position: Int)
        fun onClickCustomer(customerModel: LoyaltyCardModel?)
    }
}