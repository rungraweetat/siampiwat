package com.siampiwat.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.siampiwat.Model.DiscountCouponModel;
import com.siampiwat.R;

import java.util.ArrayList;


/**
 * Created by user on 2/6/2016.
 */
public class ResultDiscountCouponAdapter extends RecyclerView.Adapter<ResultDiscountCouponAdapter.vhSale> {

    private OnItemClickListener mOnItemClickListener;
    private Activity mActivity = null;
    private ArrayList<DiscountCouponModel> arrLstItem = null;
    private ArrayList<Boolean> arLstSelect = new ArrayList<Boolean>();

    private int mode = 0;

    ResultDiscountCouponAdapter(OnItemClickListener onItemClickListener) {

        mOnItemClickListener = onItemClickListener;
    }

    public ResultDiscountCouponAdapter(Activity activity) {

        mActivity = activity;
    }

    public void setMode(int mode) {

        this.mode = mode;
    }

    public void setListenerItemClick(OnItemClickListener onItemListener) {
        mOnItemClickListener = onItemListener;
    }

    public void setItem(ArrayList<DiscountCouponModel> arLstModel) {
        this.arrLstItem = arLstModel;
        for( int n=0;n<arrLstItem.size();n++ )
        {
            arLstSelect.add(false);
        }
    }

    @NonNull
    @Override
    public vhSale onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_result_search_barcode_discount,
                        viewGroup,
                        false);

        return new vhSale(v);
    }

    @NonNull
    @Override
    public void onBindViewHolder(@NonNull final vhSale viewHolder, final int i) {

        if (arrLstItem.size() == 0) return;
        if( arrLstItem.get(i) == null ) return;

        viewHolder.ivSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {

                    setCheck(viewHolder , i );
                    notifyDataSetChanged();

                    mOnItemClickListener.onSelected(i, arrLstItem.get(i));

                } else {
                    Log.e("ResultProductAdapter", "Please setListener first!!");
                }
            }
        });

        if( arLstSelect.get(i) ) {
            viewHolder.ivSelect.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_checked));
        }
        else{
            viewHolder.ivSelect.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_unchecked));
        }

        viewHolder.tvName.setText(arrLstItem.get(i).getCouponName());
    }

    private void setCheck(vhSale vh , int position )
    {
        for( int n=0 ;n<arLstSelect.size();n++ )
        {
            arLstSelect.set(n,false);
        }

        if( arLstSelect.size() > position )
        {
            arLstSelect.set(position , true);
            vh.ivSelect.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_checked));
        }
    }

    @Override
    public int getItemCount() {
        return arrLstItem.size();
    }

    public interface OnItemClickListener {
        void onSelected(int position, DiscountCouponModel model);
    }

    class vhSale extends RecyclerView.ViewHolder {

        private TextView tvName;

        private ImageView ivSelect;

        vhSale(View v) {
            super(v);

            tvName = v.findViewById(R.id.tvName);

            ivSelect = v.findViewById(R.id.ivSelect);
        }
    }

}
