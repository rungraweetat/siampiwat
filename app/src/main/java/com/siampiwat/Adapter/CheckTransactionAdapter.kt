package com.siampiwat.Adapter

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.siampiwat.ClassOfDiscount
import com.siampiwat.Extention.setDrawableLeft
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.ITEM_TYPE
import com.siampiwat.Model.*
import com.siampiwat.R
import kotlinx.android.synthetic.main.item_cart_header.view.*
import kotlinx.android.synthetic.main.item_view_discount.view.*
import kotlinx.android.synthetic.main.item_view_net_price.view.*
import kotlinx.android.synthetic.main.item_view_product.view.*


class CheckTransactionAdapter(var transactionData: TransactionDataModel?, val onClick: onClickListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var totalDiscountItem = arrayListOf<Any>()
    var totalDiscount = 0.0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if( getItemViewType(position) ==  ITEM_TYPE.CUSTOMER )
        {

                var holderCustomer: vhCustomer = holder as vhCustomer
                holderCustomer.memberIdCard.text = transactionData?.LoyaltyCard?.CardNumber
                holderCustomer.memberName.text = transactionData?.LoyaltyCard?.CustomerName
                holderCustomer.point.text = transactionData?.LoyaltyCard?.BalancePoints.toString()
                holderCustomer.llRootCustomer.setOnClickListener {
                    onClick.onClickCustomer(transactionData?.LoyaltyCard)
                }
        }
        else if( getItemViewType(position) ==  ITEM_TYPE.VIEW_PRODUCT ) {
            var holderProduct: vhProduct = holder as vhProduct

            val itemIndex = when (position) {
                0 -> position
                else -> position - 1
            }

            var calculateSaleLine = transactionData?.CalculatedSalesLines!![itemIndex]
            var inputSaleLine = transactionData?.InputSalesLine!![itemIndex]
            holderProduct.name.text = calculateSaleLine.ItemDescription
            holderProduct.amount.text = "x " + calculateSaleLine.Qty.toString()
            holderProduct.subTotal.text = (calculateSaleLine.Qty * calculateSaleLine.Price).toString2Digit()

            holderProduct.llRootProductManualDiscount.visibility =  View.GONE
            inputSaleLine.ManualLineDiscount?.let {

                if(it.DiscountValue > 0){
                    holderProduct.llRootProductManualDiscount.visibility =  View.VISIBLE
                    holderProduct.manualLineDiscount.text = "-" + it.DiscountValue.toString2Digit()
                }
            }

            holderProduct.llViewFooter.visibility = View.GONE
            holderProduct.llRootProduct.setOnClickListener {
                onClick.onClickProductInfo(calculateSaleLine)
            }
            if (itemIndex != 0) {
                holderProduct.title.visibility = View.GONE
            }

            if (position == transactionData?.CalculatedSalesLines!!.size){
                holderProduct.llTotal.visibility = View.VISIBLE
                holderProduct.llViewFooter.visibility = View.VISIBLE

                holderProduct.total.text = transactionData?.SalesGrossAmount?.toString2Digit()
            }
            else{
                holderProduct.llTotal.visibility = View.GONE
            }
        }
        else if( getItemViewType(position) ==  ITEM_TYPE.VIEW_NET_PRICE ) {
            var holderNetPrice = holder as vhNetPrice

            holderNetPrice.netPrice.text = transactionData?.SalesNetAmount?.toString2Digit()
        }
        else {
            var holderDiscount = holder as vhDiscount
            var countLoyaltyCardAndSalesLine = 1+ transactionData?.CalculatedSalesLines!!.size

            if( position == countLoyaltyCardAndSalesLine ) {
                holderDiscount.discountTitle.visibility = View.VISIBLE
            }
            else{
                holderDiscount.discountTitle.visibility = View.GONE
            }

            var disCountItem = totalDiscountItem[position  - countLoyaltyCardAndSalesLine]
            var disCountItemGroup: Any = ""
            var classOfDiscount = 0
            var discountName = ""
            var discountAmount =""
            var imageDrawable:Int = 0

            if (disCountItem is BottomLineDiscountModel ){
                discountName =  "ส่วนลดท้ายรายการ"
                discountAmount=disCountItem.DiscountAmount.toString2Digit()
                imageDrawable = R.drawable.ico_spacial_discount
                disCountItemGroup = transactionData?.BottomLineDiscount!!
                classOfDiscount = ClassOfDiscount.BottomLineDiscountModel
            }

            if (disCountItem is CouponDiscountModel ){
                discountName =  "คูปองส่วนลด"
                discountAmount =disCountItem.CouponDiscountAmount
                imageDrawable = R.drawable.ico_coupon
                disCountItemGroup = transactionData?.CouponDiscount!!
                classOfDiscount = ClassOfDiscount.CouponDiscountModel
            }

            if (disCountItem is RedemptionDiscountModel ){
                discountName =  "แลกคะแนนสมาชิก"
                discountAmount =disCountItem.DiscountAmount.toString2Digit()
                imageDrawable = R.drawable.ico_shuffle
                disCountItemGroup = transactionData?.RedemtionDiscount!!
                classOfDiscount = ClassOfDiscount.RedemptionDiscountModel
            }

            if (disCountItem is SubTotalDiscountModel ){
                discountName =  "ส่วนลดท้ายบิล"
                discountAmount =disCountItem.DiscountAmount?.toString2Digit()!!
                imageDrawable = R.drawable.ico_tag
                disCountItemGroup = transactionData?.SubTotalDiscounts!!
                classOfDiscount = ClassOfDiscount.SubTotalDiscountModel
            }

            if (disCountItem is RequestManualTotalDiscount ){
                discountName =  "ลดพิเศษ"
                discountAmount = disCountItem.DiscountValue.toString2Digit()
                imageDrawable = R.drawable.ico_tag
                disCountItemGroup = transactionData?.ManualTotalDiscount!!
                classOfDiscount = ClassOfDiscount.RequestManualTotalDiscount
            }

            holderDiscount.discountName.setDrawableLeft(imageDrawable)
            holderDiscount.discountName.text = discountName
            holderDiscount.discountAmount.text = "-"+discountAmount
            holderDiscount.llRootDiscount.setOnClickListener {
                onClick.onClickDiscount(disCountItem, disCountItemGroup, classOfDiscount)
            }

            if( position == countLoyaltyCardAndSalesLine + (totalDiscountItem.size -1) ) {
                holderDiscount.llDiscountTotal.visibility = View.VISIBLE
                holderDiscount.discountTotal.text = totalDiscount.toString2Digit()
            }
            else{
                holderDiscount.llDiscountTotal.visibility = View.GONE
            }
        }
    }

    private fun isEmptyLoyaltyCard():Boolean
    {
        if( transactionData?.LoyaltyCard != null  )
        {
            if( TextUtils.isEmpty(transactionData?.LoyaltyCard!!.CardNumber) &&
                    TextUtils.isEmpty(transactionData?.LoyaltyCard!!.CustomerName))
            {
                return true
            }
        }

        return false
    }

    override fun getItemViewType(position: Int): Int {

        var viewType: Int

        if(position == 0 ){
            viewType = ITEM_TYPE.CUSTOMER
        }
        else {
            if( position <= transactionData?.CalculatedSalesLines!!.size){
                viewType = ITEM_TYPE.VIEW_PRODUCT
            }
            else{
                viewType = ITEM_TYPE.VIEW_DISCOUNT

            }
        }

        if( position +1 == itemCount){
            viewType = ITEM_TYPE.VIEW_NET_PRICE
        }
        return viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        var v : View?

        return when (viewType){

            ITEM_TYPE.CUSTOMER -> {

                v = LayoutInflater.from(parent.context).inflate(R.layout.item_cart_header, parent, false)
                if( transactionData?.LoyaltyCard == null || isEmptyLoyaltyCard() ) {
                    v.visibility = View.GONE
                    v.setLayoutParams(RecyclerView.LayoutParams(0, 0))
                }
                vhCustomer(v)

            }
            ITEM_TYPE.VIEW_PRODUCT ->  {
                v = LayoutInflater.from(parent.context).inflate(R.layout.item_view_product, parent, false)
                vhProduct(v)
            }
            ITEM_TYPE.VIEW_DISCOUNT -> {
                v = LayoutInflater.from(parent.context).inflate(R.layout.item_view_discount, parent, false)
                vhDiscount(v)
            }
            ITEM_TYPE.VIEW_NET_PRICE -> {
                v = LayoutInflater.from(parent.context).inflate(R.layout.item_view_net_price, parent, false)
                vhNetPrice(v)
            }
            else -> {
                v = LayoutInflater.from(parent.context).inflate(R.layout.item_view_discount, parent, false)
                vhDiscount(v)
            }
        }


    }

    override fun getItemCount(): Int {
        var itemCount = 0

        // transactionData?.LoyaltyCard
        itemCount += 1
        totalDiscount = 0.0

        transactionData?.CalculatedSalesLines?.let {
            itemCount += it.size
        }

        totalDiscountItem.clear()

        // MARK: ส่วนลดท้ายรายการ
        transactionData?.BottomLineDiscount?.let {
            if(!it.isEmpty()) {
                totalDiscountItem.addAll(it)
                it.forEach { discount ->
                    totalDiscount += discount.DiscountAmount
                }
            }
        }

        // MARK: คูปองส่วนลด
        transactionData?.CouponDiscount?.let {
            if(!it.isEmpty()) {
                totalDiscountItem.addAll(it)
                it.forEach { discount ->
                    totalDiscount += discount.CouponDiscountAmount.toDouble()
                }
            }
        }

        // MARK: แลกคะแนนสมาชิก
        transactionData?.RedemtionDiscount?.let {
            if(it.DiscountAmount > 0){
                totalDiscountItem.add(it)
                totalDiscount += it.DiscountAmount
            }

        }

        // MARK: ลดท้ายบิล
        transactionData?.SubTotalDiscounts?.let {
            if(!it.isEmpty()) {
                totalDiscountItem.addAll(it)
                it.forEach { discount ->
                    discount.DiscountAmount?.let { discountAmount ->
                        totalDiscount += discountAmount
                    }
                }
            }
        }

        // MARK: ลดพิเศษ
        transactionData?.ManualTotalDiscount?.let {
            if(it.DiscountAmount!! > 0) {
                totalDiscountItem.add(it)
                it.DiscountAmount?.let { discountAmount ->
                    totalDiscount += discountAmount
                }
            }
        }

        itemCount += totalDiscountItem.size
        itemCount += 1 // LastItem

        return  itemCount
    }

    class vhProduct(itemView: View): RecyclerView.ViewHolder(itemView){

        val title = itemView.txtTitle!!
        val name = itemView.txtName!!
        val amount = itemView.txtAmount!!
        val subTotal = itemView.txtSubTotal!!
        val llTotal = itemView.llTotal!!
        val total = itemView.txtTotal!!
        val manualLineDiscount = itemView.txtProductDiscountAmount!!
        val llRootProductManualDiscount = itemView.llRootProductManualDiscount!!
        val llViewFooter = itemView.llViewFooter!!
        val llRootProduct = itemView.llRootProduct!!

    }

    class vhCustomer(itemView: View): RecyclerView.ViewHolder(itemView){
        val memberIdCard = itemView.textMemberIdCard!!
        val memberName = itemView.txtMemberName!!
        val point = itemView.txtPoint!!
        val llRootCustomer = itemView.llRootCustomer!!

    }

    class vhNetPrice(itemView: View): RecyclerView.ViewHolder(itemView){
        val netPrice = itemView.txtNetPrice!!

    }


    class vhDiscount(itemView: View): RecyclerView.ViewHolder(itemView){
        val discountName = itemView.txtDiscountName!!
        val discountAmount = itemView.txtDiscountAmount!!
        val discountTitle = itemView.txtTitleDiscount!!
        val llDiscountTotal = itemView.llDiscountTotal!!
        val discountTotal = itemView.txtDiscountTotal!!
        val llRootDiscount = itemView.llRootDiscount!!
    }


    interface onClickListener {
        fun onClickCustomer(customerModel: LoyaltyCardModel?)
        fun onClickProductInfo(calculateInputLinesSale: CalSalesLineModel)
        fun onClickDiscount(discountItemSelected: Any, discountItemGroup: Any, classOfDiscount: Int)
    }
}