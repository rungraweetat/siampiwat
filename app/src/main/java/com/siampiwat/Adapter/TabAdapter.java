package com.siampiwat.Adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.siampiwat.DetailProduct.TabDetailProductFragment;
import com.siampiwat.DetailProduct.TabStockProductFragment;
import com.siampiwat.Model.SearchProductItemModel;
import com.siampiwat.R;
import com.siampiwat.SearchCustomer.TabNFCFragment;
import com.siampiwat.SearchCustomer.TabSearchCustomerFragment;

import org.jetbrains.annotations.NotNull;


public class TabAdapter extends FragmentStatePagerAdapter {

    public final static int MODE_SEARCH_CUSTOMER = 0;
    public final static int MODE_PRODUCT_DETAIL = 1;

    OnClickButton onClickButton;
    int mode = 0;
    Activity mActivity;
    String data = "";

    public TabAdapter(FragmentManager fm){
        super(fm);
    }

    public void setActivity(Activity activity)
    {
        this.mActivity = activity;
    }
    public void setData(String data)
    {
        this.data = data;
    }
    public void setMode( int mode )
    {
this.mode = mode;
    }
    public void setListener( OnClickButton onClickButton )
    {
        this.onClickButton = onClickButton;
    }

    @Override
    public Fragment getItem(int position) {

        if( mode == MODE_PRODUCT_DETAIL )
        {
            switch (position)
            {
//                case 0: return new TabDetailProductFragment();
//                case 1: return new TabStockProductFragment();

                case 0: return TabDetailProductFragment.Companion.newInstance(data,"");
                case 1: return TabStockProductFragment.Companion.newInstance(data,"");
            }
        }
        else if( mode == MODE_SEARCH_CUSTOMER )
        {
            switch (position)
            {
                case 0:
                {
                    TabNFCFragment fragment = new TabNFCFragment();
                    return fragment;
                }
                case 1: {
                    TabSearchCustomerFragment fragment = new TabSearchCustomerFragment();
                    fragment.setListenerOnClickButton(new TabSearchCustomerFragment.OnClickButton() {
                        @Override
                        public void onSearchCustomer(@NotNull String keyword) {
                            if( onClickButton != null )onClickButton.onSearchCustomer(keyword);
                        }

                        @Override
                        public void onClickScanBarcode() {
                            if( onClickButton != null )onClickButton.onScanBarCode();
                        }
                    });

                    return fragment;
                }
            }
        }

        return null;
    }
    @Override
    public int getCount() {

        if( mode == MODE_SEARCH_CUSTOMER )
           return 2;
        else if( mode == MODE_PRODUCT_DETAIL )
            return 2;

        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if( mActivity == null ) return "";

        if( mode == MODE_PRODUCT_DETAIL )
        {
            switch (position) {
                case 0:
                    return mActivity.getResources().getString(R.string.detail_product_activity_tab_detail_product);
                case 1:
                    return mActivity.getResources().getString(R.string.detail_product_activity_tab_stock_product);
                default:
                    return null;
            }
        }
        else if( mode == MODE_SEARCH_CUSTOMER )
        {
            switch (position) {
                case 0:
                    return mActivity.getResources().getString(R.string.search_customer_activity_tab_nfc_reader);
                case 1:
                    return mActivity.getResources().getString(R.string.search_customer_activity_tab_search_customer);
                default:
                    return null;
            }
        }
        return "";
    }

    public interface OnClickButton
    {
        void onScanBarCode();
        void onSearchCustomer( String keyword );
    }
}
