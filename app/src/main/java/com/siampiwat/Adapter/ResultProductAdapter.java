package com.siampiwat.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.siampiwat.BaseObject.SearchActivity;
import com.siampiwat.Extention.DoubleKt;
import com.siampiwat.Model.SearchProductItemModel;
import com.siampiwat.R;

import java.util.ArrayList;


/**
 * Created by user on 2/6/2016.
 */
public class ResultProductAdapter extends RecyclerView.Adapter<ResultProductAdapter.vhProduct> {

    private OnItemClickListener mOnItemClickListener;
    private Activity mActivity = null;
    public ArrayList<SearchProductItemModel> arrLstItem = null;

    private int mode = 0;

    ResultProductAdapter(OnItemClickListener onItemClickListener) {

        mOnItemClickListener = onItemClickListener;
    }

    public ResultProductAdapter(Activity activity) {

        mActivity = activity;
    }

    public void setMode(int mode) {

        this.mode = mode;
    }

    public void setListenerItemClick(OnItemClickListener onItemListener) {
        mOnItemClickListener = onItemListener;
    }

    public void setItem(ArrayList<SearchProductItemModel> arLstAlbum) {
        this.arrLstItem = arLstAlbum;
    }

    @Override
    public vhProduct onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_result_search_product,
                        viewGroup,
                        false);

        return new vhProduct(v);
    }

    @Override
    public void onBindViewHolder(final vhProduct viewHolder, final int i) {

        if (arrLstItem.size() == 0) return;

        if( mode == SearchActivity.MODE_SEARCH_PRODUCTION)
        {
            viewHolder.tvDetailProduct.setVisibility(View.VISIBLE);
            viewHolder.tvDetailProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onClickDetail(i, arrLstItem.get(i));
                    } else {
                        Log.e("ResultProductAdapter", "Please setListener first!!");
                    }
                }
            });
        }
        else
        {
            viewHolder.tvDetailProduct.setVisibility(View.GONE);
        }

        viewHolder.ivSelectedProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {

                    setCheck(viewHolder , i );
                    notifyDataSetChanged();

                    mOnItemClickListener.onSelected(i, arrLstItem.get(i));

                } else {
                    Log.e("ResultProductAdapter", "Please setListener first!!");
                }
            }
        });

        if( arrLstItem.get(i).isSelected() ) {
            viewHolder.ivSelectedProduct.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_checked));
        }
        else{
            viewHolder.ivSelectedProduct.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_unchecked));
        }

        viewHolder.tvIdProduct.setText(arrLstItem.get(i).getProductId());
        viewHolder.tvNameProduct.setText(arrLstItem.get(i).getProductName());
        viewHolder.tvPriceProduct.setText(DoubleKt.toString2Digit( arrLstItem.get(i).getPrice()) );

    }

    private void setCheck(vhProduct vh , int position )
    {
        for( int n=0 ;n<arrLstItem.size();n++ )
        {
            arrLstItem.get(n).setSelected(false);
        }

        if( arrLstItem.size() > position )
        {
            arrLstItem.get(position).setSelected(true);
            vh.ivSelectedProduct.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_checked));
        }
    }

    @Override
    public int getItemCount() {
        return arrLstItem.size();
    }

    public interface OnItemClickListener {
        public void onClickDetail(int position, SearchProductItemModel model);
        public void onSelected(int position, SearchProductItemModel model);
    }

    public class vhProduct extends RecyclerView.ViewHolder {

        private TextView tvIdProduct;
        private TextView tvNameProduct;
        private TextView tvPriceProduct;
        private TextView tvDetailProduct;

        private ImageView ivSelectedProduct;

        vhProduct(View v) {
            super(v);

            tvDetailProduct = (TextView) v.findViewById(R.id.tvDetailProduct);
            tvPriceProduct = (TextView) v.findViewById(R.id.tvPriceProduct);
            tvIdProduct = (TextView) v.findViewById(R.id.tvIdProduct);
            tvNameProduct = (TextView) v.findViewById(R.id.tvNameProduct);

            ivSelectedProduct = (ImageView) v.findViewById(R.id.ivSelectedProduct);
        }
    }

}
