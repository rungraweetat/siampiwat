package com.siampiwat.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.siampiwat.Model.SalePersonModel;
import com.siampiwat.R;

import java.util.ArrayList;


/**
 * Created by user on 2/6/2016.
 */
public class ResultSaleAdapter extends RecyclerView.Adapter<ResultSaleAdapter.vhSale> {

    private OnItemClickListener mOnItemClickListener;
    private Activity mActivity = null;
    public ArrayList<SalePersonModel> arrLstItem = null;
    public ArrayList<Boolean> arLstSelect = new ArrayList<Boolean>();

    private int mode = 0;

    ResultSaleAdapter(OnItemClickListener onItemClickListener) {

        mOnItemClickListener = onItemClickListener;
    }

    public ResultSaleAdapter(Activity activity) {

        mActivity = activity;
    }

    public void setMode(int mode) {

        this.mode = mode;
    }

    public void setListenerItemClick(OnItemClickListener onItemListener) {
        mOnItemClickListener = onItemListener;
    }

    public void setItem(ArrayList<SalePersonModel> arLstAlbum) {
        this.arrLstItem = arLstAlbum;
        for( int n=0;n<arrLstItem.size();n++ )
        {
            arLstSelect.add(false);
        }
    }

    @Override
    public vhSale onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_result_search_sale,
                        viewGroup,
                        false);

        return new vhSale(v);
    }

    @Override
    public void onBindViewHolder(final vhSale viewHolder, final int i) {

        if (arrLstItem.size() == 0) return;

        viewHolder.ivSaleSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {

                    setCheck(viewHolder , i );
                    notifyDataSetChanged();

                    mOnItemClickListener.onSelected(i, arrLstItem.get(i));

                } else {
                    Log.e("ResultProductAdapter", "Please setListener first!!");
                }
            }
        });

        if( arLstSelect.get(i) ) {
            viewHolder.ivSaleSelect.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_checked));
        }
        else{
            viewHolder.ivSaleSelect.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_unchecked));
        }

        viewHolder.tvIdSale.setText(arrLstItem.get(i).getId());
        viewHolder.tvNameSale.setText(arrLstItem.get(i).getName());

    }

    private void setCheck(vhSale vh , int position )
    {
        for( int n=0 ;n<arLstSelect.size();n++ )
        {
            arLstSelect.set(n,false);
        }

        if( arLstSelect.size() > position )
        {
            arLstSelect.set(position , true);
            vh.ivSaleSelect.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_checked));
        }
    }

    @Override
    public int getItemCount() {
        return arrLstItem.size();
    }

    public interface OnItemClickListener {
        public void onSelected(int position, SalePersonModel model);
    }

    public class vhSale extends RecyclerView.ViewHolder {

        private TextView tvIdSale;
        private TextView tvNameSale;

        private ImageView ivSaleSelect;

        vhSale(View v) {
            super(v);

            tvIdSale = (TextView) v.findViewById(R.id.tvIdSale);
            tvNameSale = (TextView) v.findViewById(R.id.tvNameSale);

            ivSaleSelect = (ImageView) v.findViewById(R.id.ivSaleSelect);
        }
    }

}
