package com.siampiwat.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.support.v7.widget.RecyclerView
import android.view.View
import com.siampiwat.Extention.convertToString
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.ITEM_TYPE
import com.siampiwat.Model.InputSalesLineModel
import com.siampiwat.Model.ItemInfoModel
import com.siampiwat.Model.SalePersonModel
import com.siampiwat.R
import kotlinx.android.synthetic.main.item_matching_sale_person_with_product.view.*
import kotlinx.android.synthetic.main.item_result_search_sale_header.view.*

class SaleAdapter(val productList: ArrayList<InputSalesLineModel>, val onClick: onClickListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val SIZE_CUSTOMER = 1
    var salePersonModel:SalePersonModel? = null
    var isRemove:Boolean = false

    public fun initial()
    {

    }

    fun setModeSale( isRemove:Boolean )
    {
        this.isRemove = isRemove
    }

    fun setSale( salePersonModel: SalePersonModel )
    {
        this.salePersonModel = salePersonModel
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if( productList.size == 0 && salePersonModel == null) return

        if( getItemViewType(position) ==  ITEM_TYPE.CUSTOMER )
        {
            val holderSale: vhSale = holder as vhSale
            holderSale.tvNameSale.text = salePersonModel!!.Name
        }
        else
        {
            val holder: vhProduct = holder as vhProduct
            var index = position
            if( salePersonModel != null ){
                //กรณีที่เพิ่มลูกค้าแล้ว ให้เอาเอา position -1 เพราะ index จะเกิน( เพราะจาก getItemCount ไป +1 ไว้ )
                index = position - 1
            }

            if( index == 0 ) holder.flRootHeader.visibility = View.VISIBLE
            else holder.flRootHeader.visibility = View.GONE

            val product = productList[index]
            holder.name.text = product.ItemDescription
            holder.tvPriceProduct.text = product.SystemPrice.toString2Digit()
            holder.tvAmountProduct.text = product.Qty.toString()
            holder.tvSale.text = product.SalesPersonName

            if( isRemove )
            {
                holder.chkAll.visibility = View.GONE
                holder.tvSelectAll.text = holder.tvSelectAll.context.getString(R.string.common_unselect_all)
                holder.tvSelectAll.setOnClickListener {
                    onClick.onClickCheckAll()
                    onClick.onShowButtonNext()
                }

                //default ให้ show ปุ่ม
                onClick.onShowButtonNext()
            }
            else
            {
                holder.tvSelectAll.text = holder.tvSelectAll.context.getString(R.string.common_select_all)
                holder.chkAll.visibility = View.VISIBLE
            }

            holder.chkSelect.isChecked = product.isSelectedSalePerson
            holder.chkSelect.setOnClickListener {

                if( holder.chkSelect.isChecked )
                    onClick.onSelect(index)
                else
                    onClick.onUnSelect(index)

                if( isShow() ) onClick.onShowButtonNext()
                else onClick.onHideButtonNext()
            }

            holder.chkAll.setOnClickListener {

                if( holder.chkAll.isChecked )
                {
                    onClick.onClickCheckAll()
                    onClick.onShowButtonNext()
                }
                else{

                    onClick.onClickUnCheckAll()
                    onClick.onHideButtonNext()
                }
            }
        }
    }

    private fun isShow():Boolean
    {
        val isShow = false
        for( i in productList.indices )
        {
            if( productList[i].isSelectedSalePerson ) return true
        }

        return isShow
    }

    override fun getItemViewType(position: Int): Int {

        return  return if( position == 0 ) {
            if( salePersonModel == null ) ITEM_TYPE.PRODUCT
            else  ITEM_TYPE.CUSTOMER
        }
        else {  ITEM_TYPE.PRODUCT }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        var v : View?=null
        return if( viewType == ITEM_TYPE.PRODUCT ){
            v = LayoutInflater.from(parent.context).inflate(R.layout.item_matching_sale_person_with_product, parent, false)
            vhProduct(itemView = v)
        }
        else {
            v = LayoutInflater.from(parent.context).inflate(R.layout.item_result_search_sale_header, parent, false)
            vhSale(itemView = v)
        }
    }

    override fun getItemCount(): Int {

        return if (salePersonModel == null) productList.size
        else (productList.size + SIZE_CUSTOMER)

    }

    class vhProduct(itemView: View): RecyclerView.ViewHolder(itemView){

        val name = itemView.tvNameProduct!!
        val chkAll = itemView.chkAll!!
        val chkSelect = itemView.chkSelect!!
        val tvAmountProduct = itemView.tvAmountProduct!!
        val tvPriceProduct = itemView.tvPriceProduct!!
        val tvSale = itemView.tvSale!!
        val tvSelectAll = itemView.tvSelectAll!!
        val flRootHeader = itemView.flRootHeader!!
    }

    class vhSale(itemView: View): RecyclerView.ViewHolder(itemView){
        val tvNameSale = itemView.tvNameSale!!
    }

    interface onClickListener {
        fun onClickCheckAll()
        fun onClickUnCheckAll()
        fun onSelect(position: Int)
        fun onUnSelect(position: Int)
        fun onShowButtonNext()
        fun onHideButtonNext()
    }
}