package com.siampiwat.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.support.v7.widget.RecyclerView
import android.view.View
import com.siampiwat.Model.CustomerModel
import com.siampiwat.R
import kotlinx.android.synthetic.main.item_cart_header.view.*
import kotlinx.android.synthetic.main.item_cart_product.view.*

class CartCustomerAdapter(val customerList: ArrayList<CustomerModel>, val onClick: onClickListener): RecyclerView.Adapter<CartCustomerAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val customer = customerList[position]
        holder.memberIdCard.text = customer.id
        holder.memberName.text = customer.name
        holder.point.text = customer.point

        holder.itemView.setOnClickListener {
            onClick.onClickItem(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_cart_header, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return customerList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val memberIdCard = itemView.textMemberIdCard
        val memberName = itemView.txtMemberName
        val point = itemView.txtPoint


    }

    interface onClickListener {
        fun onClickItem(position: Int)
    }
}