package com.siampiwat.Adapter


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.siampiwat.Model.CouponDiscountModel
import com.siampiwat.Model.DiscountBarcodeModel
import com.siampiwat.R
import kotlinx.android.synthetic.main.item_barcode_discount.view.*
import kotlinx.android.synthetic.main.item_coupon_discount.view.*


class DiscountCouponAdapter(private val couponDiscountList: ArrayList<CouponDiscountModel>): RecyclerView.Adapter<DiscountCouponAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{

        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_coupon_discount, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return couponDiscountList.size
    }

    override fun onBindViewHolder(holder: DiscountCouponAdapter.ViewHolder, position: Int) {
        val couponDiscount = couponDiscountList[position]

        holder.txtCouponOfferId.text = couponDiscount.CouponOfferId
        holder.txtCouponName.text = couponDiscount.LabelTH
        holder.txtCouponAmount.text = "1 ใบ"
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        val txtCouponOfferId = itemView.txtCouponOfferId
        val txtCouponName = itemView.txtCouponName
        val txtCouponAmount = itemView.txtCouponAmount

    }
}