package com.siampiwat.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.siampiwat.Model.DiscountBarcodeModel;
import com.siampiwat.R;

import java.util.ArrayList;


/**
 * Created by user on 2/6/2016.
 */
public class ResultDiscountBarcodeAdapter extends RecyclerView.Adapter<ResultDiscountBarcodeAdapter.vhSale> {

    private OnItemClickListener mOnItemClickListener;
    private Activity mActivity = null;
    private ArrayList<DiscountBarcodeModel> arrLstItem = null;
    private ArrayList<Boolean> arLstSelect = new ArrayList<Boolean>();

    private int mode = 0;

    ResultDiscountBarcodeAdapter(OnItemClickListener onItemClickListener) {

        mOnItemClickListener = onItemClickListener;
    }

    public ResultDiscountBarcodeAdapter(Activity activity) {

        mActivity = activity;
    }

    public void setMode(int mode) {

        this.mode = mode;
    }

    public void setListenerItemClick(OnItemClickListener onItemListener) {
        mOnItemClickListener = onItemListener;
    }

    public void setItem(ArrayList<DiscountBarcodeModel> arLstAlbum) {
        this.arrLstItem = arLstAlbum;
        for( int n=0;n<arrLstItem.size();n++ )
        {
            arLstSelect.add(false);
        }
    }

    @NonNull
    @Override
    public vhSale onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_result_search_barcode_discount,
                        viewGroup,
                        false);

        return new vhSale(v);
    }

    @NonNull
    @Override
    public void onBindViewHolder(@NonNull final vhSale viewHolder, final int i) {

        if (arrLstItem.size() == 0) return;

        viewHolder.ivSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {

                    setCheck(viewHolder , i );
                    notifyDataSetChanged();

                    mOnItemClickListener.onSelected(i, arrLstItem.get(i));

                } else {
                    Log.e("ResultProductAdapter", "Please setListener first!!");
                }
            }
        });

        if( arLstSelect.get(i) ) {
            viewHolder.ivSelect.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_checked));
        }
        else{
            viewHolder.ivSelect.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_unchecked));
        }

        viewHolder.tvBarCodeId.setText(arrLstItem.get(i).getBarcode());
        viewHolder.tvBarCodeName.setText(arrLstItem.get(i).getName());
    }

    private void setCheck(vhSale vh , int position )
    {
        for( int n=0 ;n<arLstSelect.size();n++ )
        {
            arLstSelect.set(n,false);
        }

        if( arLstSelect.size() > position )
        {
            arLstSelect.set(position , true);
            vh.ivSelect.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_radio_button_checked));
        }
    }

    @Override
    public int getItemCount() {
        return arrLstItem.size();
    }

    public interface OnItemClickListener {
        void onSelected(int position, DiscountBarcodeModel model);
    }

    class vhSale extends RecyclerView.ViewHolder {

        private TextView tvBarCodeId;
        private TextView tvBarCodeName;

        private ImageView ivSelect;

        vhSale(View v) {
            super(v);

            tvBarCodeId = v.findViewById(R.id.tvBarCodeId);
            tvBarCodeName =  v.findViewById(R.id.tvNameBarcode);

            ivSelect = v.findViewById(R.id.ivSelect);
        }
    }

}
