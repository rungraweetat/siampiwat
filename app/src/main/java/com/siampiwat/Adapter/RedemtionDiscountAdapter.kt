package com.siampiwat.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.siampiwat.Model.RedemptionDiscountModel
import com.siampiwat.R
import kotlinx.android.synthetic.main.item_discount_checkbox.view.*


class RedemtionDiscountAdapter(private val redemptionDiscounts: ArrayList<RedemptionDiscountModel>, val onClick: OnClickListener): RecyclerView.Adapter<RedemtionDiscountAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{

        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_discount_checkbox, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return redemptionDiscounts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val subTotalDiscount = redemptionDiscounts[position]


        holder.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            onClick.onCheckChange(position, subTotalDiscount, isChecked)
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        val checkBox = itemView.chkItem
    }

    interface OnClickListener {
        fun onCheckChange(position: Int, redemptionDiscountModel: RedemptionDiscountModel, isChecked: Boolean)
    }
}