package com.siampiwat.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.support.v7.widget.RecyclerView
import android.view.View
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.Model.ItemInfoModel
import com.siampiwat.R
import kotlinx.android.synthetic.main.item_cart_product.view.*

class CartAdapter(val productList: ArrayList<ItemInfoModel>, val onClick: onClickListener): RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = productList[position]
        holder.name.text = product.ItemDescription
        holder.price.text = product.SystemPrice
        holder.barcode.text = product.ItemBarcode
        holder.amount.text = product.Amount.toString()
        if( position == 0 )
            holder.title.visibility = View.VISIBLE
        else
            holder.title.visibility = View.GONE

        holder.btnIncrease.setOnClickListener {
            onClick.onClickIncrease(position)
        }

        holder.btnDecrease.setOnClickListener {
            onClick.onClickDecrease(position)
        }

        holder.itemView.setOnClickListener {
            onClick.onClickItem(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_cart_product, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val title = itemView.txtTitle
        val name = itemView.txtName
        val price = itemView.textPrice
        val barcode = itemView.txtBarcode
        val amount = itemView.txtAmount
        val btnIncrease = itemView.btnIncrease
        val btnDecrease = itemView.btnDecrease

    }

    interface onClickListener {
        fun onClickIncrease(position: Int)
        fun onClickDecrease(position: Int)
        fun onClickItem(position: Int)
    }
}