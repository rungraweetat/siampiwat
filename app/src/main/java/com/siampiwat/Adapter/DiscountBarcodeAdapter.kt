package com.siampiwat.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.siampiwat.Model.DiscountBarcodeModel
import com.siampiwat.R
import kotlinx.android.synthetic.main.item_barcode_discount.view.*


class DiscountBarcodeAdapter(private val discountBarcodeList: ArrayList<DiscountBarcodeModel>): RecyclerView.Adapter<DiscountBarcodeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{

        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_barcode_discount, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return discountBarcodeList.size
    }

    override fun onBindViewHolder(holder: DiscountBarcodeAdapter.ViewHolder, position: Int) {
        val discountBarcode = discountBarcodeList[position]

        holder.txtBarcode.text = discountBarcode.Barcode
        holder.txtBarcodeName.text = discountBarcode.Name

    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        val txtBarcode = itemView.txtBarcode
        val txtBarcodeName = itemView.txtBarcodeName

    }
}