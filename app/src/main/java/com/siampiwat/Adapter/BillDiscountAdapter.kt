package com.siampiwat.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.siampiwat.Model.SubTotalDiscountModel
import com.siampiwat.R
import kotlinx.android.synthetic.main.item_discount_checkbox.view.*


class BillDiscountAdapter(private val subTotalDiscounts: ArrayList<SubTotalDiscountModel>, val onClick: OnClickListener): RecyclerView.Adapter<BillDiscountAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{

        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_discount_checkbox, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return subTotalDiscounts.size
    }

    override fun onBindViewHolder(holder: BillDiscountAdapter.ViewHolder, position: Int) {
        val subTotalDiscount = subTotalDiscounts[position]

        holder.checkBox.text = subTotalDiscount.LabelTH
        holder.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            onClick.onCheckChange(position, subTotalDiscount, isChecked)
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

      val checkBox = itemView.chkItem
    }

    interface OnClickListener {
        fun onCheckChange(position: Int, subTotalDiscount: SubTotalDiscountModel, isChecked: Boolean)
    }
}