package com.siampiwat.MenuSearchProduct

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.google.gson.Gson
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.BaseObject.ResultFragment
import com.siampiwat.BaseObject.SearchActivity.*
import com.siampiwat.BaseObject.SearchFragment
import com.siampiwat.EventBus.AddProductFromSearchProductModel
import com.siampiwat.EventBus.M2102PresurveyModel
import com.siampiwat.Extention.toast
import com.siampiwat.Model.CouponDiscountModel
import com.siampiwat.Model.DiscountBarcodeModel
import com.siampiwat.Model.ItemInfoModel
import com.siampiwat.Model.LoyaltyCardModel
import com.siampiwat.R
import com.siampiwat.State.Action.AddCouponDiscount
import com.siampiwat.State.Action.AddCustomerInCart
import com.siampiwat.State.Action.AddDiscountBarcode
import com.siampiwat.State.Action.AddProductInCart
import com.siampiwat.mainStore
import org.greenrobot.eventbus.EventBus

class ResultSearchProductActivity : BaseOnActivity() {

    private var fmManager: FragmentManager? = null
    private var keyword = ""
    private var data = ""
    private var page = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_search_product)

        val bundle = intent.extras
        if (bundle != null) {
            page = bundle.getInt("page")
            data = bundle.getString("data")


            if (bundle.containsKey("keyword")) {
                keyword = bundle.getString("keyword")
            }
        }

        fmManager = supportFragmentManager
        createFragment(ResultFragment.newInstance(MODE_SEARCH_PRODUCTION, data, keyword, ""), "ResultFragment")
    }

    private fun replaceFragment(fragment: Fragment?, tag: String) {
        if (fragment == null) {
            moveTaskToBack(true)
            System.exit(1)

            return
        }

        val transaction = fmManager!!.beginTransaction()
        transaction.replace(R.id.flContainer, fragment, tag)
        transaction.addToBackStack(tag)
        transaction.commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        finish()
    }

    private fun createFragment(fragment: Fragment, tag: String) {
        replaceFragment(fragment, tag)

        if (fragment is ResultFragment) {

            fragment.setListener(object : ResultFragment.onClick {
                override fun onClickBack() {
                    finish()
                }

                override fun onSelectItem(data: String) {
                    val productInfoModel = Gson().fromJson(data, ItemInfoModel::class.java)
                    mainStore.dispatch(AddProductInCart(productInfoModel))
                    EventBus.getDefault().post( AddProductFromSearchProductModel() )
                    this@ResultSearchProductActivity.toast(getString(R.string.toast_message_add_product_success))
                }
            })
        }
    }
}
