package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class CloseShiftModel(
        val ReportText: String,
        val SurveyAction: SurveyModel): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<CloseShiftModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, CloseShiftModel::class.java)
    }

}