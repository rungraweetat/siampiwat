package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class HHPOS_ConfigModel(
        val STOREID: String,
        val TERMINALID: String,
        val GROUPNAME: String,
        val KEY: String,
        val VALUE: String,
        val CREATEDDATE: String,
        val CREATEBY: String,
        val MODIFYDATE: String,
        val MODIFYBY: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()


    class Deserializer : ResponseDeserializable<HHPOS_ConfigModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, HHPOS_ConfigModel::class.java)
    }

}