package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class HHPOS_SessionModel(
        val DEVICEID: String,
        val TERMINALID: String,
        val DESCRIPTION: String,
        val PROCRESSID: String,
        val PROCRESSSTATUS: String,
        val PROCRESSSTARTTIME: String,
        val TOKENID: String,
        val STOREID: String,
        val DATEAAREAID: String,
        val SHIFTID: String,
        val OPERATIONNAME: String,
        val LASTLOGONTIME: String,
        val TRANSACTIONID: String,
        val TRANSACTIONDATA: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()


    class Deserializer : ResponseDeserializable<HHPOS_SessionModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, HHPOS_SessionModel::class.java)
    }

}