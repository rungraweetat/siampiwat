package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class DiscountBarcodeModel(val LineNum: Int, val Barcode: String, val Name: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null
    class Deserializer : ResponseDeserializable<DiscountBarcodeModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, DiscountBarcodeModel::class.java)
    }

}
data class DiscountBarcodeResponseModel(
        val DiscountBarcode: DiscountBarcodeModel): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<DiscountBarcodeResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, DiscountBarcodeResponseModel::class.java)
    }

}