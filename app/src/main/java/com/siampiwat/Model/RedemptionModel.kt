package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class RedemptionModel(
        val Id: Int,
        val OperationId: String,
        val DiscountPct: Double,
        val IsPartialRedeemAllow: Boolean,
        val PostSurvey: ArrayList<SurveyModel> = arrayListOf(),
        val PreSurvey: ArrayList<SurveyModel> = arrayListOf()): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<RedemptionModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, RedemptionModel::class.java)
    }

}
