package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class LinePAYConfirmPaymentResponseModel(
        val returnCode: String,
        val returnMessage: String,
        val info: LinePAYPaymentInfoModel)  {

    class Deserializer : ResponseDeserializable<LinePAYConfirmPaymentResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, LinePAYConfirmPaymentResponseModel::class.java)
    }

}

data class LinePAYConfirmPaymentInfoModel(
        val payInfo: LinePAYPaymentInfoModel,
        val transactionId: String,
        val orderId:  String)  {

    class Deserializer : ResponseDeserializable<LinePAYConfirmPaymentInfoModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, LinePAYConfirmPaymentInfoModel::class.java)
    }

}


data class LinePAYPaymentInfoModel(
        val method: String,
        val amount:  Double)  {

    class Deserializer : ResponseDeserializable<LinePAYPaymentInfoModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, LinePAYPaymentInfoModel::class.java)!!
    }

}

//{
//    "returnCode": "0000",
//    "returnMessage": "Success.",
//    "info": {
//    "transactionId": 2018112156419918300,
//    "orderId": "1542815506",
//    "payInfo": [
//    {
//        "method": "BALANCE",
//        "amount": 1
//    }
//    ]
//}
//}