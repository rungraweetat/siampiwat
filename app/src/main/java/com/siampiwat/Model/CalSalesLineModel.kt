package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class CalSalesLineModel(
        val LineNum: Int,
        val LineNumRef: Int,
        val ItemId: String,
        val ItemBarcode: String,
        val ItemDescription: String,
        val Price: Double,
        val Qty: Double,
        val UnitOfMeasure: String,
        val GrossAmount: Double,
        val NetAmountInclTax: Double,
        val TaxAmount: Double,
        val SalesPersonId: String,
        val SalesPersonName: String,
        val LineDiscount: ArrayList<LineDiscountModel> = arrayListOf()): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<CalSalesLineModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, CalSalesLineModel::class.java)
    }

}