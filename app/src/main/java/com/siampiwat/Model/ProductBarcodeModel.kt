package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class ProductBarcodeModel(
        val Barcode: String): ErrorInterface {

    override val Error: ArrayList<ErrorModel> = arrayListOf()
    class Deserializer : ResponseDeserializable<ProductBarcodeModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, ProductBarcodeModel::class.java)
    }

}