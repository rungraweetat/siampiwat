package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class HHPOS_ServiceLogModel(
        val LOGID: String,
        val TERMINALID: String,
        val TRANSACTIONID: String,
        val REQUESTURL: String,
        val REQUESTPARAMS: String,
        val REQUESTTIME: String,
        val RESPONSERESULT: String,
        val RESPONSESTATUS: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()


    class Deserializer : ResponseDeserializable<HHPOS_ServiceLogModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, HHPOS_ServiceLogModel::class.java)
    }

}