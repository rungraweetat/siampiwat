package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class SiamGiftCardModel(
        val CardNumber: String,
        val CardType: String,
        val Name: String,
        val Expire: String,
        val Balance: Double): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<SiamGiftCardModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SiamGiftCardModel::class.java)
    }

}

data class SiamGiftCardResponseModel(
        val CardData: SiamGiftCardModel): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<SiamGiftCardResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SiamGiftCardResponseModel::class.java)
    }

}