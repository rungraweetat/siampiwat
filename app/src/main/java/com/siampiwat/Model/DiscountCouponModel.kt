package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class DiscountCouponModel(val CouponName: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null
    class Deserializer : ResponseDeserializable<DiscountCouponModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, DiscountCouponModel::class.java)
    }

}

data class DiscountCouponResponseModel(
        val DiscountCouponModel: DiscountCouponModel): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<DiscountCouponResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, DiscountCouponResponseModel::class.java)
    }

}