package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class RequestManualTotalDiscount(
        var DiscountOfferID: String = "",
        var DiscountValue: Double = 0.0,
        var DiscountAttribute: Int = 0,
        var DiscountAmount: Double? = 0.0): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()


    class Deserializer : ResponseDeserializable<RequestManualTotalDiscount> {
        override fun deserialize(content: String) = Gson().fromJson(content, RequestManualTotalDiscount::class.java)
    }

}