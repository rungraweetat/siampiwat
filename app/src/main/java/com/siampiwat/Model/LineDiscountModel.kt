package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class LineDiscountModel(
        val LineNum: Int,
        val IsManualDiscount: Boolean,
        val OfferId: String,
        val DiscountName: String,
        val DiscountValue: Double,
        val DiscountAttribute: Int,
        val DiscountAmount: Double): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<LoyaltyCardModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, LoyaltyCardModel::class.java)
    }

}