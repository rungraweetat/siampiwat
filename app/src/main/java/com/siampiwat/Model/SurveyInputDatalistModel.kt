package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class SurveyInputDatalistModel(
        val Id: String,
        val Value: String,
        val SurveyInput: String
        ): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    class Deserializer : ResponseDeserializable<SurveyInputDatalistModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SurveyInputDatalistModel::class.java)
    }

}
