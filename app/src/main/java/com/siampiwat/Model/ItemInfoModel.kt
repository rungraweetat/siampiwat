package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class ItemInfoModel(
        val ItemId: String,
        val ItemBarcode: String,
        val ItemDescription: String,
        val UnitOfMeasure: String = "",
        val IsKeyInPrice: Boolean = false,
        val SystemPrice: String = "",
        val Variance: VarianceModel? = null,
        var isSelected: Boolean =false,
        var salePersonModel: SalePersonModel? = null,
        var isSelectedSalePerson: Boolean = false): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null


    var Amount: Int = 1

    class Deserializer : ResponseDeserializable<ItemInfoModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, ItemInfoModel::class.java)
    }

}


data class ItemInfoResponseModel(val ItemInfo: ArrayList<ItemInfoModel> = arrayListOf() ): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel = ErrorModel()

    class Deserializer : ResponseDeserializable<ItemInfoResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, ItemInfoResponseModel::class.java)
    }

}
