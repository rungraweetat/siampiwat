package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class ManualTotalDiscount(
        val Id: Int,
        val OperationId: String,
        val DiscountOfferId: String,
        var DiscountVal: Double,
        val DiscountAttribute: Int,
        val LabelTH: String,
        val LabelEN: String,
        var DiscountAmount: Double?,
        val preSurvey: ArrayList<SurveyModel> = arrayListOf(),
        val postSurvey: ArrayList<SurveyModel> = arrayListOf()): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()


    class Deserializer : ResponseDeserializable<ManualTotalDiscount> {
        override fun deserialize(content: String) = Gson().fromJson(content, ManualTotalDiscount::class.java)
    }

}