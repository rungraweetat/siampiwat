package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class ProductCategoryModel(
        val Division: String,
        val Department: String,
        val SubDepartment: String,
        val Class: String,
        val SubClass: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()


    class Deserializer : ResponseDeserializable<ProductCategoryModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, ProductCategoryModel::class.java)
    }

}