package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.util.ArrayList


data class InitDataModel (
        val CouponDiscounts: ArrayList<CouponDiscountModel> = arrayListOf(),
        val ManualTotalDiscounts: ArrayList<ManualTotalDiscount> = arrayListOf(),
        val MenuActions: ArrayList<MenuActions> = arrayListOf(),
        val Payment: ArrayList<PaymentModel> = arrayListOf(),
        val RedemtionDiscounts: ArrayList<RedemptionModel> = arrayListOf(),
        val SubtotalDiscounts: ArrayList<SubTotalDiscountModel> = arrayListOf()
        ): ErrorInterface {

    override val Error: ArrayList<ErrorModel> = arrayListOf()
    class Deserializer : ResponseDeserializable<InitDataModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, InitDataModel::class.java)
    }


    fun getPayment(parentId: String): List<PaymentModel>{

        return Payment.asSequence().filter { payment -> payment.ParentId == parentId }.toList()
    }

    fun getPaymentId( id:String ):PaymentModel{
        return Payment.asSequence().filter { payment -> payment.Id == id }.first()
    }

}


