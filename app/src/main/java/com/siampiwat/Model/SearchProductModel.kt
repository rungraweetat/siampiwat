package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class SearchProductListModel(
        val Products: ArrayList<SearchProductItemModel> = arrayListOf()): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage: ErrorModel?= null

    class Deserializer : ResponseDeserializable<SearchProductListModel> {
        
        override fun deserialize(content: String) = Gson().fromJson(content, SearchProductListModel::class.java)
    }
}
