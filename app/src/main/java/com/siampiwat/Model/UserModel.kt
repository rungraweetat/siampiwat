package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class UserModel(
                     val Token: String = "",
                     val InitScreenData: InitDataModel = InitDataModel(),
                     val ShiftId: String = "",
                     val OperatorName: String = "",
                     var OperatorId: String = "",
                     var TerminalId: String = "",
                     val ResponseMessage: ErrorModel = ErrorModel()) {

    class Deserializer : ResponseDeserializable<UserModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, UserModel::class.java)
    }

    fun getPreSurvey(menuId: String): ArrayList<SurveyModel> {
        val menuAction = this.getMenuAction(menuId)
        return menuAction.PreSurvey
    }

    fun getDataPreSurvey( menuId:String ):String{

        return if (getPreSurvey(menuId).size > 0) Gson().toJson(getPreSurvey(menuId))
        else ""
    }

    fun getDataPostSurvey( menuId:String ):String{

        return if (getPostSurvey(menuId).size > 0) Gson().toJson(getPostSurvey(menuId))
        else ""
    }

    fun getPostSurvey(menuId: String): ArrayList<SurveyModel> {
        val menuAction = this.getMenuAction(menuId)
        return menuAction.PostSurvey
    }

    private  fun getMenuAction(menuId: String): MenuActions {
        return  this.InitScreenData.MenuActions.asSequence().filter { menuActions -> menuActions.MenuId.startsWith(menuId) }.first()
    }
}
