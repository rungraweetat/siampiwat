package com.siampiwat.Model

interface  ErrorInterface{
    val Error: ArrayList<ErrorModel>?
    val ResponseMessage: ErrorModel?
        get() = null
}

class  ErrorModel{
    var Level: Int? = null
    var Message: String? = null

    fun isError(): Boolean {
        return Message?.length!! > 0
    }
}
