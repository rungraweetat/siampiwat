package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class VarianceModel(
        val Color: String,
        val Size: String,
        val Style: String,
        val Config: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<VarianceModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, VarianceModel::class.java)
    }

}
