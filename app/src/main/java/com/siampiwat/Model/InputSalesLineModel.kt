package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class InputSalesLineModel(
        val LineNum: Int,
        val ItemId: String,
        val ItemBarcode: String,
        val ItemDescription: String,
        var Price: Double,
        var SystemPrice: Double,
        var Qty: Int,
        val UnitOfMeasure: String,
        var IsKeyInPrice: Boolean,
        var IsChangePrice: Boolean,
        var SalesPersonId: String?,
        var SalesPersonName: String?,
        var ManualLineDiscount: ManualLineDiscountModel?,
        val LineSurveyItems: ArrayList<SurveyDataModel>? = arrayListOf(),
        var isSelectedSalePerson: Boolean = false)  {

    class Deserializer : ResponseDeserializable<InputSalesLineModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, InputSalesLineModel::class.java)
    }

    fun clearSalesPersonData(){
        this.isSelectedSalePerson = false
        this.SalesPersonName = null
        this.SalesPersonId = null
    }

    fun setSalesPersonData(salesPerson : SalePersonModel){
        this.SalesPersonName = salesPerson.Name
        this.SalesPersonId = salesPerson.Id
    }
}

//data class InputSalesLineResponseModel(val SalesPerson: SalePersonModel): ErrorInterface  {
//    override val Error: ArrayList<ErrorModel> = arrayListOf()
//
//    class Deserializer : ResponseDeserializable<InputSalesLineResponseModel> {
//        override fun deserialize(content: String) = Gson().fromJson(content, InputSalesLineResponseModel::class.java)
//    }
//
//}