package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class MenuActions(
        val Id: String,
        val OperationId: String,
        val PostSurvey: ArrayList<SurveyModel> = arrayListOf(),
        val PreSurvey: ArrayList<SurveyModel> = arrayListOf(),
        val MenuId: String
): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<MenuActions> {
        override fun deserialize(content: String) = Gson().fromJson(content, MenuActions::class.java)
    }

}