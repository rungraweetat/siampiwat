package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class PointModel(
       val Point: Double): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null


    class Deserializer : ResponseDeserializable<PointModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, PointModel::class.java)
    }

}