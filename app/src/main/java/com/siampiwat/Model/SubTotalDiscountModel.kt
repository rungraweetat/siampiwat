package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class SubTotalDiscountModel(
        val Id: Int,
        val OperationId: String,
        val PostSurvey: ArrayList<SurveyModel> = arrayListOf(),
        val PreSurvey: ArrayList<SurveyModel> = arrayListOf(),
        val DiscountBarcode: String,
        val DiscountAmount: Double? ,
        val OfferId: String,
        val LabelEN: String,
        val LabelTH: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<SubTotalDiscountModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SubTotalDiscountModel::class.java)
    }

}
