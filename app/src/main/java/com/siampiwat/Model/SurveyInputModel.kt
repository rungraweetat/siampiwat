package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class SurveyInputModel(
        val Id: String,
        val ControlType: String,
        val DataListItem: ArrayList<SurveyInputDatalistModel>,
        val DefaultValue: String,
        val InputKey: String,
        val IsMandatory: Boolean,
        val IsReadOnly: Boolean,
        val LabelEN: String,
        val LabelTH: String,
        val MaxDecimalLength: Int,
        val MaxLength: Int): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<SurveyInputModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SurveyInputModel::class.java)
    }

}
