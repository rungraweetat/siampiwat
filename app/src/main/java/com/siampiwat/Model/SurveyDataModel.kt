package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class SurveyDataModel(
        val SurveyResult: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<SurveyDataModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SurveyDataModel::class.java)
    }

}
