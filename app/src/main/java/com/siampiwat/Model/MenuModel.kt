package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class MenuModel(
        val Token: String,
        val InitialScreenData: InitDataModel,
        val ShiftID: String,
        val OperatorName: String) {

    class Deserializer : ResponseDeserializable<MenuModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, MenuModel::class.java)
    }

}