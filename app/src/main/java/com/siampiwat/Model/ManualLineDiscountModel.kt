package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class ManualLineDiscountModel(
       val DiscountValue: Double,
       val DiscountAttribute: Int): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<ManualLineDiscountModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, ManualLineDiscountModel::class.java)
    }

}