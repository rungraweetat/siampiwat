package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class SurveyModel(
        val ActionUri: String,
        val Description: String,
        val IsTransactionRequire: Boolean,
        val Key: String,
        val Type: String,
        val SurveyGroup: ArrayList<SurveyGroupModel> = arrayListOf()): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<SurveyModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SurveyModel::class.java)
    }

}
