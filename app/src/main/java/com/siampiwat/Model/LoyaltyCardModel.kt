package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader


data class LoyaltyCardModel(
        val CardNumber: String,
        val CardType: String,
        val CustomerName: String,
        val BalanceType: Int,
        val BalancePoints: Int,
        val ExpireDate: String,
        val Source: String,
        var isSelected:Boolean) : ErrorInterface {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null


    class Deserializer : ResponseDeserializable<LoyaltyCardModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, LoyaltyCardModel::class.java)
    }
}

data class LoyaltyModelResponseModel(val Loyalty: ArrayList<LoyaltyCardModel> = arrayListOf()) : ErrorInterface {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<LoyaltyModelResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, LoyaltyModelResponseModel::class.java)
    }
}

