package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class CouponDiscountModel(
        val LineNum: Int = 1,
        val Id: Int,
        val OperationId: String,
        val CouponOfferId: String,
        val LabelTH: String,
        val LabelEN: String,
        val PreSurvey: ArrayList<SurveyModel> = arrayListOf(),
        val PostSurvey:  ArrayList<SurveyModel> = arrayListOf(),
        val CouponDiscountAmount: String = ""): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<CouponDiscountModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, CouponDiscountModel::class.java)
    }

}