package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class CartModel(val products: List<ProductInfoModel>? = arrayListOf(),
                     val customer: CustomerModel? = CustomerModel()) {

    class Deserializer : ResponseDeserializable<CartModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, CartModel::class.java)
    }


}