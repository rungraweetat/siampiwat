package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class LinePAYPaymentRequestModel(
        val returnCode: String,
        val returnMessage: String,
        val info: LinePAYPaymentRequestInfoModel)  {

    class Deserializer : ResponseDeserializable<LinePAYPaymentRequestModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, LinePAYPaymentRequestModel::class.java)
    }

}

data class LinePAYPaymentRequestInfoModel(
        val paymentUrl: LinePAYPaymentURLModel,
        val transactionId: String,
        val paymentAccessToken:  String)  {

    class Deserializer : ResponseDeserializable<LinePAYPaymentRequestInfoModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, LinePAYPaymentRequestInfoModel::class.java)
    }

}


data class LinePAYPaymentURLModel(
        val web: String,
        val app:  String)  {

    class Deserializer : ResponseDeserializable<LinePAYPaymentURLModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, LinePAYPaymentURLModel::class.java)
    }

}