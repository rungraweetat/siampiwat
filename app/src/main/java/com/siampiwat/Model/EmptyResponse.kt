package com.siampiwat.Model


import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class EmptyResponse(
        val Survey: SurveyModel? = null,
        override val Error: ArrayList<ErrorModel> = arrayListOf()
        ): ErrorInterface  {


    class Deserializer : ResponseDeserializable<EmptyResponse> {
        override fun deserialize(content: String) = Gson().fromJson(content, EmptyResponse::class.java)
    }

}