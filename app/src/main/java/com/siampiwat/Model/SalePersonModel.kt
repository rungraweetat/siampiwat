package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class SalePersonModel(
        val Id: String,
        val Name: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<SalePersonModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SalePersonModel::class.java)
    }

}

data class SalePersonResponseModel(val SalesPerson: SalePersonModel): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<SalePersonResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SalePersonResponseModel::class.java)
    }

}
