package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import org.json.JSONObject

data class RequestPaymentModel(

        var OperationName: String? = "",
        var PaymentId: String? = "",
        val SerialNumber: String? = "",
        val CardNumber: String? = "",
        val ApproveCode: String? = "",
        val EDCBankAccount: String? = "",
        val EDCTerminalId: String? = "",
        val TransactionRef1: String = "",
        val TransactionRef2: String = "",
        var TransactionRef3: String = "",
        val TransactionRef4: String = "",
        var PaymentAmount: Double = 0.0,
        var IsBlankOperation: Boolean = false) : ErrorInterface {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<TransactionDataResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, TransactionDataResponseModel::class.java)

//        override fun deserialize(content: String):TransactionServiceRoute {
//            var data = Gson().fromJson(content , TransactionServiceRoute::class.java)
//            return data
//        }
    }

    fun toJSONObject(): JSONObject{
        return  JSONObject(Gson().toJson(this).toString())
    }
}

//data class RequestPaymentResponseModel(val PaymentModel: RequestPaymentModel) : ErrorInterface {
//
//    override val Error: ArrayList<ErrorModel> = arrayListOf()
//    override val ResponseMessage:ErrorModel = ErrorModel()
//
//    class Deserializer : ResponseDeserializable<RequestPaymentResponseModel> {
//        override fun deserialize(content: String) = Gson().fromJson(content, RequestPaymentResponseModel::class.java)
//    }
//}



