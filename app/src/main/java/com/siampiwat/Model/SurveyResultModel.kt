package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import org.json.JSONObject

data class SurveyResultModel(
        val ActionUri: String,
        val Description: String,
        val IsTransactionRequire: Boolean,
        val Key: String,
        val Type: String,
        val Result: ArrayList<SurveyResultDatalistModel> = arrayListOf()){

    fun toJSONObject(): JSONObject {
        return  JSONObject(Gson().toJson(this).toString())
    }

    class Deserializer : ResponseDeserializable<SurveyResultModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SurveyResultModel::class.java)
    }

    companion object {
        fun build(key: String, user:String, pass: String): SurveyResultModel {
            return SurveyResultModel(
                    "",
                    "",
                    false,
                    key,
                    "",
                    arrayListOf(
                            SurveyResultDatalistModel("USERNAME",user),
                            SurveyResultDatalistModel("PASSWORD",pass)
                    )
            )
        }

        fun build2(keyApi: String, listInputKey: List<String>, listResult:List<String>): SurveyResultModel {
            val mapsInput = arrayListOf<SurveyResultDatalistModel>()
            for (index in 0..listInputKey.size) {
                mapsInput.add(SurveyResultDatalistModel(listInputKey[index], listResult[index]))
            }
            return SurveyResultModel(
                    "",
                    "",
                    false,
                    keyApi,
                    "",
                    mapsInput
            )
        }
    }
}


data class SurveyResultDatalistModel(
        val InputKey: String,
        val Value: String
): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    class Deserializer : ResponseDeserializable<SurveyResultDatalistModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SurveyResultDatalistModel::class.java)
    }

}
