package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class AffiliationsModel(
        val Name: String,
        val Description: String): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<AffiliationsModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, AffiliationsModel::class.java)
    }
}

data class AffiliationsResponseModel(val Affiliation: ArrayList<AffiliationsModel> = arrayListOf() ): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null

    class Deserializer : ResponseDeserializable<AffiliationsResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, AffiliationsResponseModel::class.java)
    }

}