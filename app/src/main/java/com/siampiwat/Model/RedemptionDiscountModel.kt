package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class RedemptionDiscountModel(
       var BalanceBefRedemption: Double = 0.0,
       var isPatialRedemption: Boolean= false,
       var DiscountPCT: Double = 0.0,
       var RedeemedPoints: Double = 0.0,
       var RedeemedDiscountPCT: Double = 0.0,
       var DiscountAmount: Double = 0.0): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()


    class Deserializer : ResponseDeserializable<RedemptionDiscountModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, RedemptionDiscountModel::class.java)
    }

}