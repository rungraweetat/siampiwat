package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import org.json.JSONObject

data class TransactionDataModel(

        val TransactionId: String? = null,
        val ReceiptId: String? = null,
        val StoreId: String? = null,
        val POSTerminalId: String? = null,
        val ShiftId: Int? = null,
        val OperatorId: String? = null,
        val OperatorName: String? = null,
        val IsReturnSales: Boolean? = null,
        val SalesGrossAmount: Double? = null,
        val SalesTotalDiscountAmount: Double = 0.0,
        val SalesNetAmount: Double = 0.0,
        val SalesTaxAmount: Double = 0.0,
        val PaymentDueAmount: Double = 0.0,
        val PaymentAmount: Double = 0.0,
        val ChangeAmount: Double = 0.0,
        val SalesTotalQty: Int = 0,
        var LoyaltyCard: LoyaltyCardModel? = null,
        val Affiliations: ArrayList<AffiliationsModel> = arrayListOf(),
        var InputSalesLine: ArrayList<InputSalesLineModel> = arrayListOf(),
        val CalculatedSalesLines: ArrayList<CalSalesLineModel> = arrayListOf(),
        val DiscountBarcodes: ArrayList<DiscountBarcodeModel> = arrayListOf(),
        val BottomLineDiscount: ArrayList<BottomLineDiscountModel> = arrayListOf(),
        val CouponDiscount: ArrayList<CouponDiscountModel> = arrayListOf(),
        var RedemtionDiscount: RedemptionDiscountModel? = null,
        var SubTotalDiscounts: ArrayList<SubTotalDiscountModel> = arrayListOf(),
        var ManualTotalDiscount: RequestManualTotalDiscount? = null,
        val DiscountSurveyItems: ArrayList<SurveyDataModel> = arrayListOf(),
        val TransactionSurveyItems: ArrayList<SurveyModel> = arrayListOf(),
        val Payment: ArrayList<RequestPaymentModel> = arrayListOf())  {
    class Deserializer : ResponseDeserializable<TransactionDataModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, TransactionDataModel::class.java)

//        override fun deserialize(content: String):TransactionServiceRoute {
//            var data = Gson().fromJson(content , TransactionServiceRoute::class.java)
//            return data
//        }
    }

    fun totalAlreadyPayment():Double{

        var totalPayment = 0.0
        for( i in Payment.indices )
        {
            totalPayment+= Payment[i].PaymentAmount
        }
        return totalPayment
    }

    fun toJSONObject(): JSONObject{
        return  JSONObject(Gson().toJson(this).toString())
    }
}

data class TransactionDataResponseModel(val Transaction: TransactionDataModel, val ResponseMessage: ErrorModel = ErrorModel()){

    class Deserializer : ResponseDeserializable<TransactionDataResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, TransactionDataResponseModel::class.java)
    }

}


