package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.siampiwat.Extention.convertToString
import com.siampiwat.Extention.toString2Digit

data class SearchProductItemModel(
        val Price: Double,
        val ProductId: String,
        val ProductName: String
): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    var isSelected: Boolean =false
    class Deserializer : ResponseDeserializable<SearchProductItemModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SearchProductItemModel::class.java)
    }


    fun toItemInfoModel(): ItemInfoModel {

        return  ItemInfoModel(ProductId, "", ProductName, "", false, Price.convertToString(), null, true , null)
    }

}