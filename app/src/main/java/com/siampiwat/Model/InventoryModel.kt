package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class InventoryModel(
        val StoreName: String,
        val Qty: Int): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()


    class Deserializer : ResponseDeserializable<InventoryModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, InventoryModel::class.java)
    }

}
