package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class ProductInfoModel(
        var ProductId: String,
        var ProductName: String,
        var Price: Double,
        var ProductBarcode: ArrayList<ProductBarcodeModel>? = arrayListOf(),
        var ProductCategory: ArrayList<ProductCategoryModel>? = arrayListOf(),
        var Inventory: ArrayList<InventoryModel>?,
        var isSelected: Boolean =false): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    var Amount: Int = 1
    class Deserializer : ResponseDeserializable<ProductInfoModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, ProductInfoModel::class.java)
    }


    fun toSearchProductListModel(): SearchProductListModel {
        val searchProductListModel = SearchProductListModel()
        val searchProductItemModel = SearchProductItemModel( Price, ProductId, ProductName )

        searchProductListModel.apply {
            Products.add(searchProductItemModel)
        }

        return searchProductListModel
    }

}

data class ProductInfoResponseModel(var ProductInfo: ProductInfoModel): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    class Deserializer : ResponseDeserializable<ProductInfoResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, ProductInfoResponseModel::class.java)
    }

}