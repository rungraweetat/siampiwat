package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class BottomLineDiscountModel(
        val LineNum: Int,
        val OfferID: String,
        val DiscountName: String,
        val DiscountAmount: Double): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()

    class Deserializer : ResponseDeserializable<BottomLineDiscountModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, BottomLineDiscountModel::class.java)
    }

}