package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.util.*


data class PaymentModel(
        val Id: String,
        val OperationId: String,
        val BlankOperationParam: String,
        val HHPaymentModule: String,
        val HHPaymentParam: String,
        val Image: String,
        val IsBlankOperation: Boolean,
        val LabelEN: String,
        val LabelTH: String,
        val OperationName: String,
        val ParentId: String? ,
        val PaymentId: String,
        val PostSurvey: ArrayList<SurveyModel> = arrayListOf(),
        val PreSurvey: ArrayList<SurveyModel> = arrayListOf()
        ): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel? = null


    class Deserializer : ResponseDeserializable<PaymentModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, PaymentModel::class.java)
    }
}

data class PaymentDataResponseModel(val Payment: PaymentModel): ErrorInterface {
    override val Error: ArrayList<ErrorModel> = arrayListOf()
    override val ResponseMessage:ErrorModel = ErrorModel()

    class Deserializer : ResponseDeserializable<PaymentDataResponseModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, PaymentDataResponseModel::class.java)
    }

}