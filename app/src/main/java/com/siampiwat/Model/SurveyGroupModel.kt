package com.siampiwat.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class SurveyGroupModel(
        val Id: String,
        val TitleEN: String,
        val TitleTH: String,
        val SurveyInputs: ArrayList<SurveyInputModel> = arrayListOf()): ErrorInterface  {
    override val Error: ArrayList<ErrorModel> = arrayListOf()


    class Deserializer : ResponseDeserializable<SurveyGroupModel> {
        override fun deserialize(content: String) = Gson().fromJson(content, SurveyGroupModel::class.java)
    }

}
