package com.siampiwat.MainMenu

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.GravityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.ActionBarDrawerToggle
import com.siampiwat.MenuCart.CartFragment
import com.siampiwat.R
import kotlinx.android.synthetic.main.activity_main_menu.*
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetBehavior.from
import android.text.TextUtils
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.siampiwat.*
import com.siampiwat.API.*
import com.siampiwat.API.Routing.TransactionServiceRoute
import com.siampiwat.BaseObject.*
import com.siampiwat.BaseObject.KeyboardUtils.SoftKeyboardToggleListener
import com.siampiwat.BaseObject.SearchActivity.*
import com.siampiwat.EventBus.*
import com.siampiwat.Extention.indeterminateProgress
import com.siampiwat.Extention.toast
import com.siampiwat.Login.LoginActivity
import com.siampiwat.MenuCart.CheckListTransactionActivity
import com.siampiwat.MenuCart.MatchingSalePersonWithProductActivity
import com.siampiwat.MenuCart.TypeCustomerActivity
import com.siampiwat.MenuChangePassword.ChangePasswordFragment
import com.siampiwat.MenuClose.CloseFragment
import com.siampiwat.MenuReturnProduct.ReturnProductFragment
import com.siampiwat.MenuSearchProduct.ResultSearchProductActivity
import com.siampiwat.MenuSearchProduct.SearchProductFragment
import com.siampiwat.MenuSiamGiftCard.SiamGiftCardFragment
import com.siampiwat.Model.*
import com.siampiwat.REQUEST_CODE
import com.siampiwat.SearchCustomer.SearchCustomerActivity
import com.siampiwat.Setting.SettingActivity
import com.siampiwat.State.Action.AddTransactionDataCalculate
import com.siampiwat.State.Action.CancelSeller
import com.siampiwat.State.Action.LogOffSuccessAction
import com.siampiwat.State.Action.SetSellerInProduct
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.Survey.SurveyActivity
import com.siampiwat.Survey.SurveyConst
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.layout_badge.*
import kotlinx.android.synthetic.main.layout_menu.*
import kotlinx.android.synthetic.main.nav_header_main_menu.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.longToast
import org.rekotlin.StoreSubscriber
import java.util.*


class MainMenuActivity : BaseOnActivity(), View.OnClickListener, StoreSubscriber<CartInfoState> {
    override fun onClick(v: View?) {
        isInitial = false
        initialFragment(v!!.id)
    }

    private lateinit var bottomSheetDialog: BottomSheetDialog
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    private lateinit var inputSaleLines: String
    private lateinit var dlgSale: Dialog
    private lateinit var dlgAlert: Dialog
    private lateinit var dlgEmptyCart: Dialog
    private lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    private var isShowIconSetting = true
    private var isInitial = true
    private var optionSearchProduct = ""
    private var page = -1;
    private var scannerView: ScannerView? = null
    private var isResume = false


    private fun getActiveFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.flRootContainer)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        setSupportActionBar(toolbar)
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this)

//        actionBarDrawerToggle = ActionBarDrawerToggle(
//                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        actionBarDrawerToggle = object : ActionBarDrawerToggle(this, drawer_layout, toolbar , R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            override fun onDrawerClosed(view: View) {
                super.onDrawerClosed(view)

                if( !isCheckMenu(getString(R.string.nav_search_product)) &&
                        !isCheckMenu(getString(R.string.nav_return_product)))
                {
                    //โชว์ keyboard 2 ครั้งเพราะว่า initialFragment เสร็จแล้ว แต่ drawer_layout.closeDrawer เพิ่งสั่ง ทำให้ทำงานไม่พร้อมกัน
//                    showKeyboard()
//                    Log("onDrawerClosed")

                    hideKeyboard()
                }
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)

                hideKeyboard()
            }
        }
        drawer_layout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()

        supportActionBar!!.setDisplayShowTitleEnabled(true)

        bindEvent()
        initialFragment(R.id.nav_cart)

        bindEventBottomDialog(bindViewBottomDialog())
        setOnClickSettingListener()
        setupNavigateOperatorName()

        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }.skipRepeats()
        }

        KeyboardUtils.addKeyboardToggleListener( this , object : SoftKeyboardToggleListener {
            override fun onToggleSoftKeyboard(isVisible: Boolean) {
                if( isVisible )
                {
                    val fragment = getActiveFragment()
                    if (fragment != null) {

                        if( fragment is SearchProductFragment )
                        {
                            if( flRootScanner.childCount <= 0 ) {
                                //ถ้าอยู่หน้า scan barcode ให้ clear scannerView ออก
//                                hideKeyboard()
                            }
                        }
                        else if( fragment is ReturnProductFragment )
                        {

                        }
                        else
                        {
                            hideKeyboard()

                        }
                    }
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)

        if (EventBus.getDefault().isRegistered(this)) EventBus.getDefault().unregister(this)
    }

    public override fun onPause() {
        super.onPause()

        if (scannerView != null)
            scannerView!!.onPause()
    }

    public override fun onResume() {
        super.onResume()

        if (scannerView != null) {
            scannerView!!.onResume()
            isResume = true
        }
    }

    fun updateSumProductInCart(amount: Int, state: CartInfoState) {
        if (amount > 0) {
            flRootBadge.visibility = View.VISIBLE
            updateUI(getString(R.string.nav_cart) + " (${amount})")
            tvCount.text = amount.toString()
        } else {
            flRootBadge.visibility = View.GONE
        }

        inputSaleLines = Gson().toJson(state.transactionData.InputSalesLine)
    }

    override fun newState(state: CartInfoState) {
        var productCount = state.transactionData.InputSalesLine.size
        if (productCount > 0) {
            flRootBadge.visibility = View.VISIBLE
            updateUI(getString(R.string.nav_cart) + " ($productCount)")
            tvCount.text = productCount.toString()
        } else {
            flRootBadge.visibility = View.GONE
        }
    }

    @SuppressLint("InflateParams")
    private fun bindViewBottomDialog(): View {
        val bottomSheetView = layoutInflater.inflate(R.layout.layout_menu_cart, null)
        bottomSheetDialog = BottomSheetDialog(this@MainMenuActivity, R.style.SheetDialog)
        bottomSheetDialog.window.setBackgroundDrawableResource(android.R.color.transparent)
        bottomSheetDialog.setContentView(bottomSheetView)
        bottomSheetBehavior = from(bottomSheetView.parent as View)


        return bottomSheetView
    }

    private fun bindEvent() {
        nav_cart.setOnClickListener(this)
        nav_change_password.setOnClickListener(this)
        nav_return_product.setOnClickListener(this)
        nav_search_product.setOnClickListener(this)
        nav_siam_gift_card.setOnClickListener(this)
        nav_close.setOnClickListener(this)
        nav_signout.setOnClickListener(this)


        if (edtSearchProduct.visibility == View.VISIBLE) {
            edtSearchProduct.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch()
                    return@OnEditorActionListener true
                }
                false
            })
        }

    }

    private fun bindEventBottomDialog(bottomSheetView: View) {
        (bottomSheetView.findViewById(R.id.llAddMember) as LinearLayout).setOnClickListener {
            bottomSheetDialog.hide()

            startSearch(REQUEST_CODE.ADD_CUSTOMER)

        }
        (bottomSheetView.findViewById(R.id.llAddSale) as LinearLayout).setOnClickListener {
            bottomSheetDialog.hide()

            val arLstSurvey = userInfo?.getPreSurvey("M2200")
            val surveyJson = Gson().toJson(arLstSurvey)
            if (arLstSurvey!!.size < 1) {
                if (isHasProduct()) {
                    if (isHasSale()) showDialogSale()
                    else startPairSaleWithProduct(SearchActivity.MODE_SEARCH_SALE, SearchActivity.PAGE_SCANNER, inputSaleLines)
                } else {
                    showDialogEmptyCart()
                }
            } else {
                val i = Intent(this, SurveyActivity::class.java)
                i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2200_PreSurvey)
                startActivity(i)
            }
        }
        (bottomSheetView.findViewById(R.id.llSelectTypeCustomer) as LinearLayout).setOnClickListener {
            bottomSheetDialog.hide()

            val arLstSurvey = userInfo?.getPreSurvey("M2400")
            val surveyJson = Gson().toJson(arLstSurvey)
            if (arLstSurvey!!.size < 1) {
                startTypeCustomer()
            } else {
                val i = Intent(this, SurveyActivity::class.java)
                i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2400_PreSurvey)
                startActivity(i)
            }

        }
        (bottomSheetView.findViewById(R.id.llCancelOrder) as LinearLayout).setOnClickListener {
            bottomSheetDialog.hide()

            if (isHasProduct() || isHasRoyaltyCard()) {
                showDialogCancelSeller()
            } else {
                Log("not have product and ")
            }
        }
    }


    private fun startTypeCustomer() {
        val i = Intent(this, TypeCustomerActivity::class.java)
        startActivity(i)
    }

    private fun startPairSaleWithProduct(mode: Int, page: Int, productList: String) {
        val i = Intent(this, SearchActivity::class.java)
        i.putExtra("mode", mode)
        i.putExtra("page", page)
        i.putExtra("data", productList)
        startActivityForResult(i, REQUEST_CODE.SEARCH_PRODUCT)
    }

    private fun startSearch(requestCode: Int) {
        val i = Intent(this, SearchCustomerActivity::class.java)
        startActivityForResult(i, requestCode)
    }

    private fun startSearchProduct(mode: Int, page: Int) {
        val i = Intent(this, SearchActivity::class.java)
        i.putExtra("mode", mode)
        i.putExtra("page", page)
        startActivityForResult(i, REQUEST_CODE.SEARCH_PRODUCT)
    }

    private fun setOnClickSettingListener() {
        ivSetting?.setOnClickListener {
            this.let {
                drawer_layout.closeDrawer(GravityCompat.START)
                val i = Intent(this, SettingActivity::class.java)
                startActivity(i)
            }
        }
    }

    private fun setupNavigateOperatorName() {
        val operatorId = getString(R.string.nav_header_id_sale) + " ${userInfo?.ShiftId}"
        val terminalId = getString(R.string.nav_header_id_device) + " $terminalID"
        tvName?.text = userInfo?.OperatorName
        tvId?.text = operatorId
        tvIdDevice?.text = terminalId
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            val fragment = getActiveFragment()
            if (fragment != null) {

                if( fragment is SearchProductFragment )
                {
                    if( flRootScanner.childCount > 0 ) {
                        //ถ้าอยู่หน้า scan barcode ให้ clear scannerView ออก
                        clearScannerView()
                    }
                }
                return
            }
            super.onBackPressed()
        }
    }

    private fun summaryAmount():Int
    {
        var sumAmount:Int = 0
        for ( i in mainStore.state.cartInfoState.transactionData.InputSalesLine.iterator() )
            sumAmount += i.Qty

        return sumAmount
    }

    @Subscribe
    fun onEvent(event: AddProductFromSearchProductModel) {
        Log("onEvent")
        var productCount = summaryAmount()
        if (productCount > 0) {
            flRootBadge.visibility = View.VISIBLE
            updateUI(getString(R.string.nav_cart) + " ($productCount)")
            tvCount.text = productCount.toString()
        } else {
            flRootBadge.visibility = View.GONE
        }
    }

    @Subscribe
    fun onEvent(event: M2200PreSurveyModel) {
        if (isHasProduct()) {
            if (isHasSale()) showDialogSale()
            else startPairSaleWithProduct(SearchActivity.MODE_SEARCH_SALE, SearchActivity.PAGE_SCANNER, inputSaleLines)
        } else {
            showDialogEmptyCart()
        }
    }

    @Subscribe
    fun onEvent(event: M4000PreSurveyModel) {
        var fragment = SearchProductFragment.newInstance("", "")
        addFragmentToActivity(
                supportFragmentManager,
                fragment,
                R.id.flRootContainer, getString(R.string.nav_search_product))

        fragment.setOnSelectOptionSearch(object : SearchProductFragment.onClickObject {
            override fun onChangeMode(isSearch: Boolean) {
                setPage(PAGE_SCANNER)
            }

            override fun onGetOptionSearch(searchOption: String?) {
                optionSearchProduct = searchOption!!
            }
        })
    }

    @Subscribe
    fun onEvent(event: M6000PreSurveyModel) {
        addFragmentToActivity(
                supportFragmentManager,
                CloseFragment.newInstance("", ""),
                R.id.flRootContainer, getString(R.string.nav_close))

        dialogConfirm(getString(R.string.close_fragment_title), getString(R.string.close_fragment_message), object : OnClickDialog {
            override fun onClickConfirm() {

                requestCloseShift()
            }

            override fun onClickCancel() {
                initialFragment(R.id.nav_cart)
            }
        })
    }

    @Subscribe
    fun onEvent(event: M2000PreSurveyModel) {
        addFragmentToActivity(
                supportFragmentManager,
                CartFragment.newInstance("", ""),
                R.id.flRootContainer, getString(R.string.nav_cart))
    }

    @Subscribe
    fun onEvent(event: M2400PreSurveyModel) {
        startTypeCustomer()
    }
    @Subscribe
    fun onEvent(event: M2500PreSurveyModel) {
        mainStore.dispatch(CancelSeller())

        val arLstSurvey = userInfo?.getPostSurvey("M2500")
        val surveyJson = Gson().toJson(arLstSurvey)
        if (arLstSurvey!!.size < 1) {

        } else {
            val i = Intent(this, SurveyActivity::class.java)
            i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
            i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2500_PostSurvey)
            startActivity(i)
        }
    }
    @Subscribe
    fun onEvent(event: M2500PostSurveyModel) {

        Toast.makeText(this, getString(R.string.dlg_cancel_seller), Toast.LENGTH_SHORT).show()
    }

    @Subscribe
    fun onEvent(event: M7000PreSurveyModel) {

        addFragmentToActivity(
                supportFragmentManager,
                ChangePasswordFragment.newInstance("", ""),
                R.id.flRootContainer, getString(R.string.nav_change_password))
    }

    @Subscribe
    fun onEvent(event: M8000PreSurveyModel) {

        dialogConfirm(getString(R.string.log_out_fragment_title), getString(R.string.log_out_fragment_message), object : OnClickDialog {
            override fun onClickConfirm() {
                requestLogOff()
            }

            override fun onClickCancel() {
                initialFragment(R.id.nav_cart)
            }
        })
    }

    @Subscribe
    fun onEvent(event: M8000PostSurveyModel) {

        mainStore.dispatch(LogOffSuccessAction(applicationContext))
        startLoginActivity()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {

        invalidateOptionsMenu()
        menu!!.findItem(R.id.action_settings).isVisible = isShowIconSetting

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetDialog.show()

        return super.onOptionsItemSelected(item)

    }

    private fun initialFragment(menuId: Int): Boolean {
        val fragment = getActiveFragment()
        fragment?.onDestroy()

        toolbar.visibility = View.VISIBLE
        edtSearchProduct.visibility = View.GONE
        edtSearchProduct.isEnabled = false
        edtSearchProduct.setText("")
        actionBarDrawerToggle.drawerArrowDrawable.color = resources.getColor(android.R.color.white)
        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary))

        var title = ""
        when (menuId) {
            R.id.nav_cart -> {

                title = getString(R.string.nav_cart)
                if (isInitial) {
                    addFragmentToActivity(
                            supportFragmentManager,
                            CartFragment.newInstance("", ""),
                            R.id.flRootContainer, title)
                } else {
                    val arLstSurvey = userInfo?.getPreSurvey("M2000")
                    val surveyJson = Gson().toJson(arLstSurvey)
                    if (arLstSurvey!!.size < 1) {
                        addFragmentToActivity(
                                supportFragmentManager,
                                CartFragment.newInstance("", ""),
                                R.id.flRootContainer, title)
                    } else {
                        val i = Intent(this, SurveyActivity::class.java)
                        i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                        i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2000_PreSurvey)
                        startActivity(i)
                    }
                }
            }
            R.id.nav_return_product -> {
                showKeyboard()

                title = getString(R.string.nav_return_product)
                val fragment = ReturnProductFragment.newInstance("","")
                addFragmentToActivity(
                        supportFragmentManager,
                        fragment,
                        R.id.flRootContainer, title)

                fragment.setOnSelectOptionSearch {
                    setPage(PAGE_SCANNER)
                }
            }
            R.id.nav_search_product -> {
                showKeyboard()

                title = getString(R.string.nav_search_product)

                val arLstSurvey = userInfo?.getPreSurvey("M4000")
                val surveyJson = Gson().toJson(arLstSurvey)
                if (arLstSurvey!!.size < 1) {
                    var fragment = SearchProductFragment.newInstance("", "")

                    addFragmentToActivity(
                            supportFragmentManager,
                            fragment,
                            R.id.flRootContainer, title)

                    fragment.setOnSelectOptionSearch(object : SearchProductFragment.onClickObject {
                        override fun onChangeMode(isSearch: Boolean) {
                            setPage(PAGE_SCANNER)
                        }

                        override fun onGetOptionSearch(searchOption: String?) {
                            optionSearchProduct = searchOption!!
                        }
                    })
                } else {
                    val i = Intent(this, SurveyActivity::class.java)
                    i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                    i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M4000_PreSurvey)
                    startActivity(i)
                }
            }
            R.id.nav_siam_gift_card -> {
                title = getString(R.string.nav_siam_gift_card)
                addFragmentToActivity(
                        supportFragmentManager,
                        SiamGiftCardFragment.newInstance("", ""),
                        R.id.flRootContainer, title)
            }
            R.id.nav_close -> {
                title = getString(R.string.nav_close)
                val arLstSurvey = userInfo?.getPreSurvey("M6000")
                val surveyJson = Gson().toJson(arLstSurvey)
                if (arLstSurvey!!.size < 1) {
                    addFragmentToActivity(
                            supportFragmentManager,
                            CloseFragment.newInstance("", ""),
                            R.id.flRootContainer, title)

                    dialogConfirm(getString(R.string.close_fragment_title), getString(R.string.close_fragment_message), object : OnClickDialog {
                        override fun onClickConfirm() {

                            requestLogOff()
                            finish()
                            startLoginActivity()
                        }

                        override fun onClickCancel() {
                            initialFragment(R.id.nav_cart)
                        }
                    })
                } else {
                    val i = Intent(this, SurveyActivity::class.java)
                    i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                    i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M6000_PreSurvey)
                    startActivity(i)
                }
            }
            R.id.nav_change_password -> {
                title = getString(R.string.nav_change_password)

                val arLstSurvey = userInfo?.getPreSurvey("M7000")
                val surveyJson = Gson().toJson(arLstSurvey)
                if (arLstSurvey!!.size < 1) {
                    addFragmentToActivity(
                            supportFragmentManager,
                            ChangePasswordFragment.newInstance("", ""),
                            R.id.flRootContainer, title)
                } else {
                    val i = Intent(this, SurveyActivity::class.java)
                    i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                    i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M7000_PreSurvey)
                    startActivity(i)
                }
            }
            R.id.nav_signout -> {
                val arLstSurvey = userInfo?.getPreSurvey("M8000")
                val surveyJson = Gson().toJson(arLstSurvey)
                if (arLstSurvey!!.size < 1) {
                    dialogConfirm(getString(R.string.log_out_fragment_title), getString(R.string.log_out_fragment_message), object : OnClickDialog {
                        override fun onClickConfirm() {
                            requestLogOff()
                        }

                        override fun onClickCancel() {
                            initialFragment(R.id.nav_cart)
                        }
                    })
                } else {
                    val i = Intent(this, SurveyActivity::class.java)
                    i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                    i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M8000_PreSurvey)
                    startActivity(i)
                }
            }
        }

        isShowIconSetting = (menuId == R.id.nav_cart)
        updateUI(title)
        setMenuSelected(menuId)

        if (menuId != R.id.nav_signout && menuId != R.id.nav_close ) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        if (menuId == R.id.nav_search_product || menuId == R.id.nav_return_product) {
            toolbar.visibility = View.VISIBLE
            edtSearchProduct.visibility = View.VISIBLE
            edtSearchProduct.isEnabled = true
            actionBarDrawerToggle.drawerArrowDrawable.color = resources.getColor(R.color.colorPrimary)
            toolbar.setBackgroundColor(resources.getColor(android.R.color.white))
        }

        return true
    }

    private fun updateUI(titleBar: String) {
        supportActionBar!!.title = titleBar
    }

    @SuppressLint("ResourceType")
    private fun setMenuSelected(idMenu: Int) {
        nav_cart.setBackgroundColor(resources.getColor(R.color.transparent))
        nav_change_password.setBackgroundColor(resources.getColor(R.color.transparent))
        nav_return_product.setBackgroundColor(resources.getColor(R.color.transparent))
        nav_search_product.setBackgroundColor(resources.getColor(R.color.transparent))
        nav_siam_gift_card.setBackgroundColor(resources.getColor(R.color.transparent))
        nav_close.setBackgroundColor(resources.getColor(R.color.transparent))
        nav_signout.setBackgroundColor(resources.getColor(R.color.transparent))

        tvNavCart.setTextColor(resources.getColor(android.R.color.black))
        tvChangePassword.setTextColor(resources.getColor(android.R.color.black))
        tvNavReturn.setTextColor(resources.getColor(android.R.color.black))
        tvNavSearch.setTextColor(resources.getColor(android.R.color.black))
        tvNavSiamGiftCard.setTextColor(resources.getColor(android.R.color.black))
        tvClose.setTextColor(resources.getColor(android.R.color.black))
        tvSignOut.setTextColor(resources.getColor(android.R.color.black))

        ivCart.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_cart))
        ivChangePassword.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_changepass))
        ivNavReturn.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_return))
        ivNavSearch.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_search))
        ivNavSiamGiftCard.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_giftcard))
        ivClose.setImageDrawable(resources.getDrawable(R.drawable.ico_closeshift))
        ivSignOut.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_logout))

        when (idMenu) {
            R.id.nav_cart -> {
                nav_cart.setBackgroundColor(resources.getColor(R.color.highlight_menu))
                ivCart.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_cart_active))
                tvNavCart.setTextColor(resources.getColor(R.color.colorPrimary))
            }
            R.id.nav_change_password -> {
                nav_change_password.setBackgroundColor(resources.getColor(R.color.highlight_menu))
                ivChangePassword.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_changepass_active))
                tvChangePassword.setTextColor(resources.getColor(R.color.colorPrimary))
            }
            R.id.nav_return_product -> {
                nav_return_product.setBackgroundColor(resources.getColor(R.color.highlight_menu))
                ivNavReturn.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_return_active))

                tvNavReturn.setTextColor(resources.getColor(R.color.colorPrimary))
            }
            R.id.nav_search_product -> {
                nav_search_product.setBackgroundColor(resources.getColor(R.color.highlight_menu))
                ivNavSearch.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_search_active))

                tvNavSearch.setTextColor(resources.getColor(R.color.colorPrimary))
            }
            R.id.nav_siam_gift_card -> {
                nav_siam_gift_card.setBackgroundColor(resources.getColor(R.color.highlight_menu))
                ivNavSiamGiftCard.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_giftcard_active))

                tvNavSiamGiftCard.setTextColor(resources.getColor(R.color.colorPrimary))

            }
            R.id.nav_close -> {
                nav_close.setBackgroundColor(resources.getColor(R.color.highlight_menu))
                ivClose.setImageDrawable(resources.getDrawable(R.drawable.ico_closeshift_active))

                tvClose.setTextColor(resources.getColor(R.color.colorPrimary))
            }
            R.id.nav_signout -> {
                nav_signout.setBackgroundColor(resources.getColor(R.color.highlight_menu))
                ivSignOut.setImageDrawable(resources.getDrawable(R.drawable.ico_nav_logout_active))
                tvSignOut.setTextColor(resources.getColor(R.color.colorPrimary))
            }
        }
    }

    private fun addFragmentToActivity(manager: FragmentManager, fragment: Fragment, fragmentId: Int, tag: String) {

        val transaction = manager.beginTransaction()
        transaction.replace(fragmentId, fragment, tag)
        transaction.addToBackStack(fragment::javaClass.name)
//        transaction.commit()
        transaction.commitAllowingStateLoss()
    }

    private fun dialogConfirm(title: String, message: String, listenner: OnClickDialog) {
        dlgAlert = Dialog(this, R.style.dialog_theme)
        dlgAlert.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dlgAlert.setContentView(R.layout.dlg_confirm)
        dlgAlert.setCancelable(true)

        val tvOk = dlgAlert.findViewById<TextView>(R.id.tvOk)
        val tvCancel = dlgAlert.findViewById<TextView>(R.id.tvCancel)
        val tvTitle = dlgAlert.findViewById<TextView>(R.id.tvTitle)
        val tvMessage = dlgAlert.findViewById<TextView>(R.id.tvMessage)

        tvTitle.text = title
        tvMessage.text = message

        tvOk.text = getText(R.string.common_confirm)
        tvOk.setOnClickListener {
            dlgAlert.dismiss()
            listenner.onClickConfirm()
        }

        tvCancel.text = getText(R.string.common_cancel)
        tvCancel.setOnClickListener {
            dlgAlert.dismiss()
            listenner.onClickCancel()
        }
        dlgAlert.show()
    }

    @SuppressLint("CheckResult")
    private fun requestLogOff() {
        var dialog = this.indeterminateProgress("")

        AccountApi.logOff()
                .doOnSubscribe {
                    dialog?.show()
                }
                .subscribe { Response ->
                    dialog?.dismiss()
                    val (Response, Error) = Response
                    if (Error === null && Response?.Error?.size == 0) {
                        applicationContext?.let {

                            val arLstSurvey = userInfo?.getDataPostSurvey("M8000")
                            if (TextUtils.isEmpty(arLstSurvey)) {
                                mainStore.dispatch(CancelSeller())
                                Toast.makeText(this, getString(R.string.dlg_cancel_seller), Toast.LENGTH_SHORT).show()
                            } else {
                                val i = Intent(this, SurveyActivity::class.java)
                                i.putExtra(SurveyConst.KEY_SURVEY_JSON, arLstSurvey)
                                i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M8000_PostSurvey)
                                startActivity(i)
                            }
                        }
                    } else {
                        var errorMessage = ""

                        Error?.let {
                            errorMessage = it.message.toString()
                        }
                        Response?.Error?.let {
                            errorMessage = it.first().Message!!
                        }
                        applicationContext?.toast(errorMessage)
                    }
                }
    }

    private fun requestCloseShift() {

        val dialog = indeterminateProgress("")
        dialog!!.show()

        CloseShiftApi.closeShift().subscribe(object : SingleObserver<Result<CloseShiftModel, FuelError>> {
            override fun onSubscribe(d: Disposable) {

            }

            override fun onSuccess(  closeShiftModelResponseModel: Result<CloseShiftModel, FuelError>){
                val closeShiftModelResponseModel = closeShiftModelResponseModel.component1()
                val Error = closeShiftModelResponseModel!!.component2()

                if (Error == null && closeShiftModelResponseModel != null && closeShiftModelResponseModel.Error.size == 0)
                {
                    val salePersonModel = closeShiftModelResponseModel.ReportText
                    val model = Gson().toJson(salePersonModel)

                    requestLogOff()
                    finish()
                    startLoginActivity()
                }
                else
                {
                    if (closeShiftModelResponseModel != null) {
                        if (closeShiftModelResponseModel.ResponseMessage != null) {
                            val message = closeShiftModelResponseModel.ResponseMessage.Message
                            alertItemNotFound(message, "")
                        }
                    }
                }
            }

            override fun onError(e: Throwable) {
                dialog!!.dismiss()
            }
            
        } )
    }

    private fun startLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    private fun showDialogSale() {
        dlgSale = Dialog(this, R.style.dialog_theme)
        dlgSale.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dlgSale.setContentView(R.layout.dlg_sale)
        dlgSale.setCancelable(true)

        val llAddSale = dlgSale.findViewById(R.id.llAddSale) as LinearLayout
        llAddSale.setOnClickListener {
            startPairSaleWithProduct(SearchActivity.MODE_SEARCH_SALE, SearchActivity.PAGE_SCANNER, inputSaleLines)

            dlgSale.hide()
        }

        val llRemoveSale = dlgSale.findViewById(R.id.llRemoveSale) as LinearLayout
        llRemoveSale.setOnClickListener {

            val intent = Intent(this, MatchingSalePersonWithProductActivity::class.java)
            intent.putExtra("mode_sale", "remove")
            startActivity(intent)

            dlgSale.hide()
        }

        val btnClose = dlgSale.findViewById(R.id.btnClose) as Button
        btnClose.setOnClickListener {
            dlgSale.hide()
        }
        dlgSale.show()
    }

    private fun isCheckMenu(currentMenu:String):Boolean
    {
        if( supportFragmentManager == null ) return false
        if( supportFragmentManager.fragments.size <= 0 ) return false

        for( i in supportFragmentManager.fragments.indices )
        {
            val tag = supportFragmentManager.fragments[i].tag
            if( tag == currentMenu ) return true
        }
        return false
    }

    private fun isHasRoyaltyCard(): Boolean {
        return (mainStore.state.cartInfoState.transactionData.LoyaltyCard != null)
    }
    private fun isHasSale(): Boolean {
        val arLstItemInfoModelModel = Gson().fromJson<ArrayList<InputSalesLineModel>>(inputSaleLines, object : TypeToken<List<InputSalesLineModel>>() {

        }.type)

        for (i in arLstItemInfoModelModel.indices) {
            if (arLstItemInfoModelModel[i].SalesPersonId != null) return true
        }

        return false
    }

    private fun isHasProduct(): Boolean {
        if (TextUtils.isEmpty(inputSaleLines)) {
            return false
        }
        val arLstItemInfoModelModel = Gson().fromJson<ArrayList<InputSalesLineModel>>(inputSaleLines, object : TypeToken<List<InputSalesLineModel>>() {

        }.type)

        return (arLstItemInfoModelModel.size > 0)
    }

    private fun showDialogCancelSeller() {
        var dlgCancelSeller = Dialog(this, R.style.dialog_theme)
        dlgCancelSeller.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dlgCancelSeller.setContentView(R.layout.dlg_confirm)
        dlgCancelSeller.setCancelable(true)

        val tvTitle = dlgCancelSeller.findViewById(R.id.tvTitle) as TextView
        tvTitle.text = resources.getString(R.string.common_alert_title_cancel_sale)

        val tvMessage = dlgCancelSeller.findViewById(R.id.tvMessage) as TextView
        tvMessage.visibility = View.GONE

        val tvCanceler = dlgCancelSeller.findViewById(R.id.tvCancel) as TextView
        tvCanceler.visibility = View.VISIBLE
        tvCanceler.text = resources.getString(R.string.common_close)
        tvCanceler.setOnClickListener {
            dlgCancelSeller.hide()
        }

        val tvOk = dlgCancelSeller.findViewById(R.id.tvOk) as TextView
        tvOk.setOnClickListener(View.OnClickListener {
            dlgCancelSeller.hide()

            val arLstSurvey = userInfo?.getPreSurvey("M2500")
            val surveyJson = Gson().toJson(arLstSurvey)
            if (arLstSurvey!!.size < 1) {
                mainStore.dispatch(CancelSeller())
                Toast.makeText(this, getString(R.string.dlg_cancel_seller), Toast.LENGTH_SHORT).show()
            } else {
                val i = Intent(this, SurveyActivity::class.java)
                i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
                i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2500_PreSurvey)
                startActivity(i)
            }
        })
        dlgCancelSeller.show()
    }

    private fun showDialogEmptyCart() {
        dlgEmptyCart = Dialog(this, R.style.dialog_theme)
        dlgEmptyCart.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dlgEmptyCart.setContentView(R.layout.dlg_confirm)
        dlgEmptyCart.setCancelable(true)

        val tvTitle = dlgEmptyCart.findViewById(R.id.tvTitle) as TextView
        tvTitle.text = resources.getString(R.string.common_alert_title_empty_cart)

        val tvMessage = dlgEmptyCart.findViewById(R.id.tvMessage) as TextView
        tvMessage.text = resources.getString(R.string.common_alert_message_empty_cart)

        val tvCanceler = dlgEmptyCart.findViewById(R.id.tvCancel) as TextView
        tvCanceler.visibility = View.GONE

        val tvOk = dlgEmptyCart.findViewById(R.id.tvOk) as TextView
        tvOk.setOnClickListener(View.OnClickListener {
            dlgEmptyCart.cancel()
        })
        dlgEmptyCart.show()
    }

    private fun performSearch()
    {
        if (TextUtils.isEmpty(edtSearchProduct!!.text.toString())) {
            BaseOnActivity.getInstance().hideKeyboard()
            return
        }

        if( isCheckMenu(getString(R.string.nav_search_product)) )
        {
            requestSearchProduct( edtSearchProduct.text.toString()  )
        }
        else if(isCheckMenu(getString(R.string.nav_return_product)))
        {
            edtSearchProduct.setText("2420961-1000001")
            requestSearchReturnProduct(edtSearchProduct.text.toString())
        }
    }

    private fun requestSearchProduct(searchValue: String) {

        val dialog = indeterminateProgress("")
        Objects.requireNonNull<ProgressDialog>(dialog).show()
        Objects.requireNonNull<Window>(dialog!!.window).setGravity(Gravity.CENTER)

        ItemApi.searchProduct(searchValue, optionSearchProduct, "", "").subscribe(object : SingleObserver<Result<SearchProductListModel, FuelError>> {
            override fun onSubscribe(d: Disposable) {

            }

            override fun onSuccess(productInfoModelFuelErrorResult: Result<SearchProductListModel, FuelError>) {
                val searchList = productInfoModelFuelErrorResult.component1()
                val Error = productInfoModelFuelErrorResult.component2()

                dialog.dismiss()
                if (Error == null && searchList != null && searchList.Error.isEmpty()) {
                    startResultSearchProduct(searchList,searchValue)
                } else {
                    startResultSearchProduct(searchList,searchValue)
                }

                clearScannerView()
            }

            override fun onError(e: Throwable) {
                dialog.dismiss()
            }
        })
    }

    private fun requestSearchReturnProduct(receiptId: String) {

        val dialog = indeterminateProgress("")
        Objects.requireNonNull<ProgressDialog>(dialog).show()
        Objects.requireNonNull<Window>(dialog!!.window).setGravity(Gravity.CENTER)

        TransactionApi.returnSales(receiptId).subscribe(object : SingleObserver<Result<TransactionDataResponseModel, FuelError>> {
            override fun onSubscribe(d: Disposable) {

            }

            override fun onSuccess(transactionDataResponse: Result<TransactionDataResponseModel, FuelError>) {

                val transaction = transactionDataResponse.component1()
                val Error = transactionDataResponse.component2()

                dialog.dismiss()
                if (Error == null && transaction != null && !transaction.ResponseMessage.isError()) {
                    mainStore.dispatch(AddTransactionDataCalculate(transaction.Transaction))
                    val i = Intent(this@MainMenuActivity, CheckListTransactionActivity::class.java)
                    startActivity(i)
                } else {
                    if (transaction != null) {
                        if (transaction.ResponseMessage != null) {
                            val message = transaction.ResponseMessage!!.Message
                            alertItemNotFound(message, "")
                        }
                    }
                }

                clearScannerView()
            }

            override fun onError(e: Throwable) {
                dialog.dismiss()
            }
        })
    }

    private fun startResultSearchProduct(dataSearchProductList: SearchProductListModel? , keyword:String) {
        var data = ""
        if (dataSearchProductList == null)
            data = ""
        else
            data = Gson().toJson(dataSearchProductList, SearchProductListModel::class.java)

        val i = Intent(this, ResultSearchProductActivity::class.java)
        i.putExtra("keyword",keyword)
        i.putExtra("data", data)
        startActivity(i)

    }

    fun clearScannerView()
    {
        if (scannerView != null) {
            scannerView!!.onPause()
            scannerView!!.onDestroy()
            scannerView = null
        }

        flRootScanner.removeAllViews()
        isResume = false
    }

    fun setPage(page: Int) {
        if (page == PAGE_SEARCH) {
            clearScannerView()
        } else {
            if (scannerView == null) {
                initialScanner()
                if (!isResume!!) {
                    val handle = Handler()
                    handle.postDelayed({
                        scannerView!!.onResume()
                        isResume = true
                    }, 300)
                }
            }
            flRootScanner.removeAllViews()
            flRootScanner.addView(scannerView)

            hideKeyboard()
        }

        this.page = page
    }

    private fun initialScanner() {
        scannerView = ScannerView(this)
        scannerView!!.setTitle(getString(R.string.common_scan_barcode))
        scannerView!!.setShowFloatingButton(true)
        scannerView!!.setListener(object : ScannerView.ScannerListener {
            override fun onScanComplete(result: String) {

                if( isCheckMenu(getString(R.string.nav_search_product)) )
                {
                    requestSearchProduct(result)
                }
                else if(isCheckMenu(getString(R.string.nav_return_product)))
                {
                    requestSearchReturnProduct(result)
                }
            }

            override fun onClickClose() {
                clearScannerView()
            }

            override fun onClickKeyboard() {
                setPage(PAGE_SEARCH)
            }
        })
    }

    private fun alertItemNotFound(message: String?, dataSearch: String) {
        dlgAlert = Dialog(this, R.style.dialog_theme)
        dlgAlert.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dlgAlert.setContentView(R.layout.dlg_search_again)
        dlgAlert.setCancelable(true)

        val tvTitle = dlgAlert.findViewById<TextView>(R.id.tvTitle)
        tvTitle.text = message

        val tvOk = dlgAlert.findViewById<TextView>(R.id.tvOk)
        tvOk.visibility = View.GONE

        val tvClose = dlgAlert.findViewById<TextView>(R.id.tvClose)
        tvClose.setOnClickListener {
            dlgAlert.cancel()

            if (page == PAGE_SCANNER) {
                clearScannerView()
                setPage(page)
            } else {
                //ถ้ากดปุ่ม ปิด ระบบจะไม่เรียก api ต่อให้ user ต้องพิมพ์เอง แล้วกดปุ่ม search จาก keyboard
                showKeyboard()

//                val handler = Handler()
//                handler.postDelayed({
//                    if (SearchFragment.edtSearch != null)
//                        SearchFragment.edtSearch.requestFocus()
//                    else
//                        BaseOnActivity.Log("edtSearch == null")
//                }, 300)
            }
        }
        dlgAlert.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data == null) return

        getActiveFragment()?.onActivityResult(requestCode, resultCode, data)

//        if( requestCode == SurveyConst.M2000_PreSurvey)
//        {
//            initialFragment(R.id.nav_cart)
//        }

        if (resultCode == Activity.RESULT_OK) {
            //update ui
        }
    }

    interface OnClickDialog {
        fun onClickConfirm()
        fun onClickCancel()
    }
}
