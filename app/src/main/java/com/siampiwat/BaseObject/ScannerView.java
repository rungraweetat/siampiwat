package com.siampiwat.BaseObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.zxing.Result;
import com.siampiwat.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerView extends FrameLayout implements ZXingScannerView.ResultHandler {

    private ScannerListener listener;
    private Context context;
    private ZXingScannerView scannerView;
    private FrameLayout flRoot;
    private ImageView fabKeypad;
    private NavigatorView navigatorView;

    private boolean isShow = false;

    public ScannerView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public ScannerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public ScannerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    @Override
    public void handleResult(Result result) {
        if (listener != null) {

            listener.onScanComplete(result.getText());

        } else {
            Log.e("ScannerView","Please setListener first!!!");
        }
    }

    public void onResume() {

        if( scannerView != null )
        {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if( scannerView != null )
                        scannerView.resumeCameraPreview(ScannerView.this);
                }
            }, 2000);
            scannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
            scannerView.startCamera();// Start camera on resume
        }
    }

    public FrameLayout getRootView()
    {
        return flRoot;
    }

    public void onPause() {

        if( scannerView != null )
        {
            scannerView.stopCamera();           // Stop camera on pause
        }

    }

    public void onDestroy()
    {
        scannerView = null;
    }

    public void setListener(ScannerListener listener) {
        this.listener = listener;
    }
    public void setShowFloatingButton( boolean isShow )
    {
        this.isShow = isShow;

        updateUi();
    }

    private void updateUi()
    {
        if( isShow ) {
            fabKeypad.setVisibility(View.VISIBLE);
        }
        else {
            fabKeypad.setVisibility(View.GONE);
        }

        fabKeypad.invalidate();
    }

    private void init() {

        inflate(getContext(), R.layout.base_scanner_view, this);
        bindView();

        scannerView = new ZXingScannerView(context);
        flRoot.addView(scannerView);


    }

    public void setTitle( String title )
    {
        navigatorView.setNavigatorTitle(title);
    }

    @SuppressLint("RestrictedApi")
    private void bindView() {
        flRoot = findViewById(R.id.flRootScanner);
        navigatorView = findViewById(R.id.navigatorView);
        navigatorView.setListener(new NavigatorView.onClickNavigator() {
            @Override
            public void onClickBack() {
                if (listener != null) {
                    listener.onClickClose();
                } else {
                    Log.e("ScannerView","Please setListener first!!!");
                }
            }
        });

        fabKeypad = findViewById(R.id.fabKeypad);
        fabKeypad.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (listener != null) {
                    listener.onClickKeyboard();
                } else {
                    Log.e("ScannerView","Please setListener first!!!");
                }
            }
        });

    }

    public interface ScannerListener {
        void onScanComplete(String result);
        void onClickClose();
        void onClickKeyboard();
    }
}
