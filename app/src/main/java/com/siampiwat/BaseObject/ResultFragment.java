package com.siampiwat.BaseObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.siampiwat.Adapter.CartNewAdapter;
import com.siampiwat.Adapter.ResultCustomerAdapter;
import com.siampiwat.Adapter.ResultDiscountBarcodeAdapter;
import com.siampiwat.Adapter.ResultDiscountCouponAdapter;
import com.siampiwat.Adapter.ResultProductAdapter;
import com.siampiwat.Adapter.ResultSaleAdapter;
import com.siampiwat.Adapter.SaleAdapter;
import com.siampiwat.DetailProduct.DetailProductActivity;
import com.siampiwat.Extention.DoubleKt;
import com.siampiwat.MainApp;
import com.siampiwat.MainAppKt;
import com.siampiwat.Model.CustomerModel;
import com.siampiwat.Model.DiscountBarcodeModel;
import com.siampiwat.Model.DiscountCouponModel;
import com.siampiwat.Model.InputSalesLineModel;
import com.siampiwat.Model.InventoryModel;
import com.siampiwat.Model.ItemInfoModel;
import com.siampiwat.Model.LoyaltyCardModel;
import com.siampiwat.Model.ProductBarcodeModel;
import com.siampiwat.Model.ProductCategoryModel;
import com.siampiwat.Model.ProductInfoModel;
import com.siampiwat.Model.SalePersonModel;
import com.siampiwat.Model.SearchProductItemModel;
import com.siampiwat.Model.SearchProductListModel;
import com.siampiwat.R;
import com.siampiwat.State.State.CartInfoState;

import org.rekotlin.Store;
import org.rekotlin.StoreSubscriber;
import org.rekotlin.Subscription;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.siampiwat.BaseObject.SearchActivity.MODE_ADD_BAR_CODE_DISCOUNT;
import static com.siampiwat.BaseObject.SearchActivity.MODE_ADD_COUPON_DISCOUNT;
import static com.siampiwat.BaseObject.SearchActivity.MODE_ADD_PRODUCT;
import static com.siampiwat.BaseObject.SearchActivity.MODE_ADD_SALE_BY_PRODUCT;
import static com.siampiwat.BaseObject.SearchActivity.MODE_SEARCH_CUSTOMER;
import static com.siampiwat.BaseObject.SearchActivity.MODE_SEARCH_PRODUCTION;
import static com.siampiwat.BaseObject.SearchActivity.MODE_SEARCH_SALE;
import static com.siampiwat.MainAppKt.getMainStore;


public class ResultFragment extends Fragment {
    private static final String PARAM_MODE = "param1";
    private static final String PARAM_DATA = "param2";
    private static final String PARAM_SEARCH_VALUE = "PARAM_SEARCH_VALUE";
    private static final String PARAM_DATA_PRODUCT_LIST = "param_product_list";

    private ResultCustomerAdapter customerAdapter;
    private ResultProductAdapter productAdapter;
    private ResultSaleAdapter saleAdapter;
    private ResultDiscountCouponAdapter discountCouponAdapter;
    private ResultDiscountBarcodeAdapter discountBarcodeAdapter;
    private RecyclerView recyclerView;
    private NavigatorView navigationView;
    private int mode;
    private String data;
    private String searchValue;
    private String dataSelected;
    private String productList;
    private Activity activity;
    private Button btnOk;
    private TextView tvSearchValue;
    private TextView tvCountItemFound;
    private onClick listener;

    public ResultFragment() {

    }

    public static ResultFragment newInstance(int mode, String data, String searchValue, String productList) {
        ResultFragment fragment = new ResultFragment();
        Bundle args = new Bundle();
        args.putInt(PARAM_MODE, mode);
        args.putString(PARAM_DATA, data);
        args.putString(PARAM_SEARCH_VALUE, searchValue);
        args.putString(PARAM_DATA_PRODUCT_LIST, productList);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mode = getArguments().getInt(PARAM_MODE);
            data = getArguments().getString(PARAM_DATA);
            searchValue = getArguments().getString(PARAM_SEARCH_VALUE);
            productList = getArguments().getString(PARAM_DATA_PRODUCT_LIST);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_result, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bindView(view);
        bindValue();
    }

    private void bindView(View view) {
        navigationView = view.findViewById(R.id.navigatorView);
        navigationView.setListener(new NavigatorView.onClickNavigator() {
            @Override
            public void onClickBack() {
                if (listener != null)
                    listener.onClickBack();
                else
                    Log.e("ResultFragment", "Please setListener first!!");
            }
        });

        recyclerView = view.findViewById(R.id.recycleView);

        if( mode == MODE_ADD_PRODUCT || mode == MODE_SEARCH_PRODUCTION)
        {
            productAdapter = new ResultProductAdapter(activity);
            productAdapter.setMode(mode);
        }
        else if( mode == MODE_SEARCH_SALE )
            saleAdapter = new ResultSaleAdapter(activity);
        else if( mode == MODE_ADD_BAR_CODE_DISCOUNT)
            discountBarcodeAdapter = new ResultDiscountBarcodeAdapter(activity);
        else if( mode == MODE_ADD_COUPON_DISCOUNT )
            discountCouponAdapter = new ResultDiscountCouponAdapter(activity);
        else if( mode == MODE_SEARCH_CUSTOMER )
            customerAdapter = new ResultCustomerAdapter(activity);

        btnOk = view.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSelectItem(dataSelected);
            }
        });

        tvSearchValue = view.findViewById(R.id.tvSearchValue);
        tvCountItemFound = view.findViewById(R.id.tvCountItemFound);
    }

    public void setListener(onClick listener) {
        this.listener = listener;
    }

    private void bindValue() {
        tvSearchValue.setText(String.format(" '%s'", searchValue));
        btnOk.setVisibility(View.GONE);

        String countFoundItem = "";

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.line_divider));

        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(layoutManager);

        if (mode == MODE_SEARCH_CUSTOMER) {

            if (TextUtils.isEmpty(data)){

                countFoundItem = (String.format(getString(R.string.label_result_count_item_found), 0));
                tvCountItemFound.setText(countFoundItem);
                return;
            }

            ArrayList<LoyaltyCardModel> arLstCustomerModel = new Gson().fromJson(data, new TypeToken<List<LoyaltyCardModel>>() {
            }.getType());

            countFoundItem = (String.format(getString(R.string.label_result_count_item_found), arLstCustomerModel.size()));

            customerAdapter.setItem(arLstCustomerModel);
            customerAdapter.setListenerItemClick(new ResultCustomerAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position, LoyaltyCardModel model) {

                }

                @Override
                public void onSelected(int position, LoyaltyCardModel model) {
                    btnOk.setVisibility(View.VISIBLE);
                    dataSelected = new Gson().toJson(model);
                }
            });
            recyclerView.setAdapter(customerAdapter);
        } else if (mode == SearchActivity.MODE_ADD_PRODUCT) {

            btnOk.setText(getResources().getString(R.string.common_ok));

            if (TextUtils.isEmpty(data)){

                countFoundItem = (String.format(getString(R.string.label_result_count_item_found), 0));
                tvCountItemFound.setText(countFoundItem);
                return;
            }
            ArrayList<SearchProductItemModel> arLstProductModel = new Gson().fromJson(data, new TypeToken<List<SearchProductItemModel>>() {
            }.getType());

//            SearchProductListModel arLstProductModel = new Gson().fromJson(data, SearchProductListModel.class);
            countFoundItem = (String.format(getString(R.string.label_result_count_item_found), arLstProductModel.size()));

            productAdapter.setItem(arLstProductModel);
            productAdapter.setListenerItemClick(new ResultProductAdapter.OnItemClickListener() {
                @Override
                public void onClickDetail(int position, SearchProductItemModel model) {

                }

                @Override
                public void onSelected(int position, SearchProductItemModel model) {
                    btnOk.setVisibility(View.VISIBLE);
                    dataSelected = new Gson().toJson(model.toItemInfoModel());
                }
            });
            recyclerView.setAdapter(productAdapter);

        } else if (mode == SearchActivity.MODE_SEARCH_PRODUCTION) {
            btnOk.setText(getResources().getString(R.string.common_add_product_in_cart));

            if (TextUtils.isEmpty(data)){

                countFoundItem = (String.format(getString(R.string.label_result_count_item_found), 0));
                tvCountItemFound.setText(countFoundItem);
                return;
            }

            SearchProductListModel arLstProductModel = new Gson().fromJson(data, SearchProductListModel.class);
            countFoundItem = (String.format(getString(R.string.label_result_count_item_found), arLstProductModel.getProducts().size()));

            productAdapter.setItem(arLstProductModel.getProducts());
            productAdapter.setListenerItemClick(new ResultProductAdapter.OnItemClickListener() {
                @Override
                public void onClickDetail(int position, SearchProductItemModel model) {
                    Intent i = new Intent(activity, DetailProductActivity.class);
                    String itemInfoModel = new Gson().toJson(model);
                    i.putExtra("data", itemInfoModel);
                    startActivity(i);
                }

                @Override
                public void onSelected(int position, SearchProductItemModel model) {
                    btnOk.setVisibility(View.VISIBLE);
                    dataSelected = new Gson().toJson(model.toItemInfoModel());
                }
            });
            recyclerView.setAdapter(productAdapter);
        } else if (mode == MODE_SEARCH_SALE) {

            if (TextUtils.isEmpty(data)){

                countFoundItem = (String.format(getString(R.string.label_result_count_item_found), 0));
                tvCountItemFound.setText(countFoundItem);
                return;
            }

            final SalePersonModel salePersonModel = new Gson().fromJson(data, SalePersonModel.class);
            ArrayList<SalePersonModel> arLstSale = new ArrayList<>();
            arLstSale.add(salePersonModel);

            countFoundItem = (String.format(getString(R.string.label_result_count_item_found), arLstSale.size()));

            saleAdapter.setItem(arLstSale);
            saleAdapter.setListenerItemClick(new ResultSaleAdapter.OnItemClickListener() {
                @Override
                public void onSelected(int position, SalePersonModel model) {
                    btnOk.setVisibility(View.VISIBLE);
                     dataSelected = new Gson().toJson(model);
                }
            });
            recyclerView.setAdapter(saleAdapter);
        }else if (mode == MODE_ADD_SALE_BY_PRODUCT) {

            if (TextUtils.isEmpty(data)){

                countFoundItem = (String.format(getString(R.string.label_result_count_item_found), 0));
                tvCountItemFound.setText(countFoundItem);
                return;
            }

            final SalePersonModel salePersonModel = new Gson().fromJson(data, SalePersonModel.class);
            ArrayList<SalePersonModel> arLstSale = new ArrayList<>();
            arLstSale.add(salePersonModel);

            countFoundItem = (String.format(getString(R.string.label_result_count_item_found), arLstSale.size()));

            saleAdapter.setItem(arLstSale);
            saleAdapter.setListenerItemClick(new ResultSaleAdapter.OnItemClickListener() {
                @Override
                public void onSelected(int position, SalePersonModel model) {
                    btnOk.setVisibility(View.VISIBLE);
                     dataSelected = new Gson().toJson(model);
                }
            });
            recyclerView.setAdapter(saleAdapter);
        }else if (mode == MODE_ADD_BAR_CODE_DISCOUNT) {

            if (TextUtils.isEmpty(data)){

                countFoundItem = (String.format(getString(R.string.label_result_count_item_found), 0));
                tvCountItemFound.setText(countFoundItem);
                return;
            }

            final DiscountBarcodeModel model = new Gson().fromJson(data, DiscountBarcodeModel.class);
            ArrayList<DiscountBarcodeModel> arLstModel = new ArrayList<>();
            arLstModel.add(model);

            countFoundItem = (String.format(getString(R.string.label_result_count_item_found), arLstModel.size()));

            discountBarcodeAdapter.setItem(arLstModel);
            discountBarcodeAdapter.setListenerItemClick(new ResultDiscountBarcodeAdapter.OnItemClickListener() {
                @Override
                public void onSelected(int position, DiscountBarcodeModel model) {
                    btnOk.setVisibility(View.VISIBLE);
                    dataSelected = new Gson().toJson(model);
                }
            });
            recyclerView.setAdapter(discountBarcodeAdapter);
        }else if (mode == MODE_ADD_COUPON_DISCOUNT) {

            if (TextUtils.isEmpty(data)){

                countFoundItem = (String.format(getString(R.string.label_result_count_item_found), 0));
                tvCountItemFound.setText(countFoundItem);
                return;
            }

            final DiscountCouponModel model = new Gson().fromJson(data, DiscountCouponModel.class);
            if( model == null ) return;

            ArrayList<DiscountCouponModel> arLstModel = new ArrayList<>();
            arLstModel.add(model);

            countFoundItem = (String.format(getString(R.string.label_result_count_item_found), arLstModel.size()));

            discountCouponAdapter.setItem(arLstModel);
            discountCouponAdapter.setListenerItemClick(new ResultDiscountCouponAdapter.OnItemClickListener() {
                @Override
                public void onSelected(int position, DiscountCouponModel model) {
                    btnOk.setVisibility(View.VISIBLE);
                    dataSelected = new Gson().toJson(model);
                }
            });
            recyclerView.setAdapter(discountCouponAdapter);
        }

        tvCountItemFound.setText(countFoundItem);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.activity = getActivity();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public interface onClick {
        void onClickBack();

        void onSelectItem(String data);
    }
}
