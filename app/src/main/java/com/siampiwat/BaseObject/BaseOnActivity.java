
package com.siampiwat.BaseObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.siampiwat.R;

import org.json.JSONObject;

public class BaseOnActivity extends AppCompatActivity {

    public interface ResponseApi
    {
        public void onSuccess(JSONObject jData);
        public void onSuccess(String response);
        public void onFail(String response);
    }

    public ProgressDialog pDialog = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setScreenOrientation();

        instance = this;
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
    }

    private void setScreenOrientation()
    {
        if( getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT )
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public static void Log(String message)
    {
        try {
            String methodName = "";
            String fileName = "";
            StackTraceElement[] arStackTrace = Thread.currentThread().getStackTrace();

            if( arStackTrace.length > 3)
            {
                methodName = arStackTrace[3].getMethodName();
                fileName = arStackTrace[3].getFileName();

                Log.e(fileName+"-->"+methodName,message);
            }
            else
            {
                Log.e("Log","message = "+message);
            }
        }
        catch (Exception ex)
        {
            Log.e("Log","-->ex = "+ex.toString());
        }
    }

    private static BaseOnActivity instance = new BaseOnActivity();
    public static BaseOnActivity getInstance() {

        if( instance == null )
            instance = new BaseOnActivity();
        return instance;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

               Log("onDestroy");
    }

    public void showKeyboard()
    {
        InputMethodManager imgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if( imgr == null ) return;

        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void hideKeyboard()
    {
        if( getCurrentFocus() == null ) return;
        if( getCurrentFocus().getWindowToken() == null ) return;

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if( inputMethodManager == null ) return;

        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
}
