package com.siampiwat.BaseObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.result.Result;
import com.google.gson.Gson;
import com.siampiwat.API.AccountApi;
import com.siampiwat.API.DiscountApi;
import com.siampiwat.API.ItemApi;
import com.siampiwat.API.SalesPersonApi;
import com.siampiwat.DetailProduct.DetailProductActivity;
import com.siampiwat.EventBus.CloseSearchProductModel;
import com.siampiwat.EventBus.M2200PostSurveyModel;
import com.siampiwat.MainAppKt;
import com.siampiwat.MenuCart.MatchingSalePersonWithProductActivity;
import com.siampiwat.Model.CouponDiscountModel;
import com.siampiwat.Model.CustomerModel;
import com.siampiwat.Model.DiscountBarcodeModel;
import com.siampiwat.Model.DiscountBarcodeResponseModel;
import com.siampiwat.Model.DiscountCouponModel;
import com.siampiwat.Model.DiscountCouponResponseModel;
import com.siampiwat.Model.InputSalesLineModel;
import com.siampiwat.Model.ItemInfoModel;
import com.siampiwat.Model.ItemInfoResponseModel;
import com.siampiwat.Model.LoyaltyCardModel;
import com.siampiwat.Model.SalePersonModel;
import com.siampiwat.Model.ProductInfoModel;
import com.siampiwat.Model.ProductInfoResponseModel;
import com.siampiwat.Model.SalePersonResponseModel;
import com.siampiwat.Model.SearchProductListModel;
import com.siampiwat.Model.SurveyModel;
import com.siampiwat.R;
import com.siampiwat.State.Action.AddCouponDiscount;
import com.siampiwat.State.Action.AddCustomerInCart;
import com.siampiwat.State.Action.AddDiscountBarcode;
import com.siampiwat.State.Action.AddProductInCart;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import com.siampiwat.Extention.DoubleKt;
import com.siampiwat.Extention.ContextKt;
import com.siampiwat.State.Action.MatchingSalePersonWithProduct;
import com.siampiwat.State.Action.SetSellerInProduct;
import com.siampiwat.Survey.SurveyActivity;
import com.siampiwat.Survey.SurveyConst;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SearchActivity extends BaseOnActivity {

    private static final int SEARCH_FROM_SCAN = 0;
    private static final int SEARCH_FROM_KEY_IN = 1;

    public static int MODE_SEARCH_CUSTOMER = 0;
    public static int MODE_SEARCH_PRODUCTION = 1;
    public static int MODE_SEARCH_SALE = 2;
    public static int MODE_ADD_PRODUCT = 3;
    public static int MODE_ADD_BAR_CODE_DISCOUNT = 4;
    public static int MODE_ADD_COUPON_DISCOUNT = 5;
    public static int MODE_ADD_SALE_BY_PRODUCT = 6;
    public static int MODE_ADD_QRCODE_READER = 7;
    public static int MODE_RETURN_PRODUCT = 8;

    public static int PAGE_SEARCH = 0;
    public static int PAGE_SCANNER = 1;

    public static String TAG_SEARCH_FRAGMENT = "SearchFragment";
    public static String TAG_RESULT_FRAGMENT = "ResultFragment";

    private FragmentManager fmManager;
    private FrameLayout flRoot;
    private FrameLayout flRootScanner;
    private ImageView ivIcon;
    private EditText edtSearch;
    private RadioGroup rdGroup;
    private int mode = 0;
    private int page = 0;
    private int position = -1;
    private String productList = "";
    private ScannerView scannerView;
    private FloatingActionButton fabScan;
    private Dialog dlgAlert;
    private Boolean isResume = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mode = bundle.getInt("mode");
            page = bundle.getInt("page");

            if (bundle.containsKey("data")) {
                productList = bundle.getString("data");
            }

            if (bundle.containsKey("position")) {
                position = bundle.getInt("position");
            }
        }

        fmManager = getSupportFragmentManager();

        bindView();
        bindEvent();
//        initialScanner();
        setMode(mode);
        setPage(page);

//        Intent i = getPackageManager().getLaunchIntentForPackage("com.qrscan");
//        i.putExtra("scan","barcode");
//        startActivityForResult(i , Activity.RESU

//        requestSearchProduct("indochine");
//        requestBarCodeDiscount("160000000030130" ,SEARCH_FROM_SCAN);
    }

    private void startMatchingSalePersonWithProduct(String salePersonalModel) {
        Intent i = new Intent(this, MatchingSalePersonWithProductActivity.class);
        i.putExtra("data", salePersonalModel);
        i.putExtra("mode_sale", "matching_sale");
        startActivity(i);
    }

    private void startDetailProduct(String searchProduct) {
        Intent i = new Intent(this, DetailProductActivity.class);
        i.putExtra("data", searchProduct);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                setResultData(result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult

    private void initialScanner() {
        scannerView = new ScannerView(this);
        scannerView.setTitle(getString(R.string.common_scan_barcode));

        if (mode == MODE_ADD_QRCODE_READER) {
            scannerView.setShowFloatingButton(false);
        } else {
            scannerView.setShowFloatingButton(true);
        }

        scannerView.setListener(new ScannerView.ScannerListener() {
            @Override
            public void onScanComplete(String result) {

                if (mode == MODE_SEARCH_CUSTOMER) {
                    setResultData(result);
                } else if (mode == MODE_ADD_PRODUCT) {
                    requestItemInfo(result, SEARCH_FROM_SCAN);
                } else if (mode == MODE_SEARCH_PRODUCTION) {
                    requestSearchProduct(result);
                } else if (mode == MODE_SEARCH_SALE || mode == MODE_ADD_SALE_BY_PRODUCT) {
                    requestSaleInfo(result, SEARCH_FROM_SCAN);
                } else if (mode == MODE_ADD_BAR_CODE_DISCOUNT) {
                    requestBarCodeDiscount(result, SEARCH_FROM_SCAN);
                } else if (mode == MODE_ADD_COUPON_DISCOUNT) {
                    requestCouponDiscount(result, SEARCH_FROM_SCAN);
                } else if (mode == MODE_ADD_QRCODE_READER) {
                    // TODO: ....
                    setResultData(result);
                }
            }

            @Override
            public void onClickClose() {
                if (scannerView != null) {
                    scannerView.onPause();
                    scannerView.onDestroy();
                }
                setResultData("");
            }

            @Override
            public void onClickKeyboard() {
                setPage(PAGE_SEARCH);
            }
        });
    }

    private void setResultData(String data) {
        if (scannerView != null) {
            scannerView.onPause();
            scannerView.onDestroy();
            scannerView = null;
        }
        hideKeyboard();

        if (mode == MODE_SEARCH_CUSTOMER) {
            ArrayList<SurveyModel> arLstSurvey = Objects.requireNonNull(MainAppKt.getUserInfo()).getPostSurvey("M2200");
            String surveyJson = new Gson().toJson(arLstSurvey);
            Intent i = new Intent(this , SurveyActivity.class);
            i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson);
            i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2200_PostSurvey);
            startActivity(i);
        }

        Intent resultIntent = new Intent();
        resultIntent.putExtra("data", data);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    private void bindEvent() {
        ivIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mode == MODE_SEARCH_PRODUCTION) {
                    finish();
                    EventBus.getDefault().post(new CloseSearchProductModel());
                } else {
                    setResultData("");
                }
            }
        });

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });

        fabScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPage(PAGE_SCANNER);
            }
        });
    }

    private void performSearch() {
        edtSearch.clearFocus();

        hideKeyboard();
        if (TextUtils.isEmpty(edtSearch.getText().toString())) return;

        //call api
        if (mode == MODE_ADD_PRODUCT) {
//            requestProductInfo(edtSearch.getText().toString(), SEARCH_FROM_KEY_IN);
            requestItemInfo(edtSearch.getText().toString(), SEARCH_FROM_KEY_IN);
        } else if (mode == MODE_SEARCH_PRODUCTION) {
            requestSearchProduct(edtSearch.getText().toString());
        } else if (mode == MODE_SEARCH_SALE || mode == MODE_ADD_SALE_BY_PRODUCT) {
            requestSaleInfo(edtSearch.getText().toString(), SEARCH_FROM_KEY_IN);
//            startResultFragment( new SalePersonModel("001" , "name sale"));
        } else if (mode == MODE_ADD_BAR_CODE_DISCOUNT) {
            requestBarCodeDiscount(edtSearch.getText().toString(), SEARCH_FROM_KEY_IN);
        } else if (mode == MODE_ADD_COUPON_DISCOUNT) {
            requestCouponDiscount(edtSearch.getText().toString(), SEARCH_FROM_KEY_IN);
        } else {
            //กรณีที่ ไม่ได้ค้นหาสินค้า เมื่อไม่พบข้อมูล จะต้องแสดง dialog แจ้งว่าไม่พบข้อมูล และทำการค้นหาให้ อัติโนมัติ
            dlgAlert = new Dialog(this, R.style.dialog_theme);
            dlgAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dlgAlert.setContentView(R.layout.dlg_alert);
            dlgAlert.setCancelable(true);
            TextView tvOk = dlgAlert.findViewById(R.id.tvOk);
            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dlgAlert.cancel();
                    createFragment(
                            SearchFragment.newInstance(mode, edtSearch.getText().toString(),
                                    getResources().getString(R.string.item_result_search_product_title),
                                    true), "SearchFragment");
                }
            });
            dlgAlert.show();
        }
    }

    private void createFragment(Fragment fragment, String tag) {
        replaceFragment(fragment, tag);

        if (fragment instanceof SearchFragment) {
            ((SearchFragment) fragment).setListener(new SearchFragment.onClick() {
                @Override
                public void onClickBack() {
                    onBackPressed();
                    showKeyboard();
                }

                @Override
                public void onNotFound(String keyword) {
                    createFragment(ResultFragment.newInstance(mode, "", keyword, productList), "ResultFragment");
                    hideKeyboard();
                }

                @Override
                public void onSelected(String data, String keyword) {
                    createFragment(ResultFragment.newInstance(mode, data, keyword, productList), "ResultFragment");
                    hideKeyboard();
                }
            });
        } else if (fragment instanceof ResultFragment) {
            hideKeyboard();

            ((ResultFragment) fragment).setListener(new ResultFragment.onClick() {
                @Override
                public void onClickBack() {
                    onBackPressed();

                    if (page == PAGE_SEARCH)
                        showKeyboard();
                    else
                        hideKeyboard();
                }

                @Override
                public void onSelectItem(String data) {
                    // TODO: DON"T USE IN PRODUCTION!!   Hardcode for force result
                    if (mode == MODE_SEARCH_CUSTOMER) {

                        LoyaltyCardModel customerModel = new Gson().fromJson(data, LoyaltyCardModel.class);
                        MainAppKt.getMainStore().dispatch(new AddCustomerInCart(customerModel));
                        ContextKt.toast(SearchActivity.this, getString(R.string.toast_message_add_customer_success));

                    } else if (mode == MODE_ADD_PRODUCT) {


                        ItemInfoModel productInfoModel = new Gson().fromJson(data, ItemInfoModel.class);
                        requestItemInfo(productInfoModel.getItemId(), SEARCH_FROM_KEY_IN);

//                        MainAppKt.getMainStore().dispatch(new AddProductInCart(productInfoModel));
//                        ContextKt.toast(SearchActivity.this, getString(R.string.toast_message_add_product_success));

                    } else if (mode == MODE_ADD_BAR_CODE_DISCOUNT) {

                        DiscountBarcodeModel discountBarcodeModel = new Gson().fromJson(data, DiscountBarcodeModel.class);
                        MainAppKt.getMainStore().dispatch(new AddDiscountBarcode(discountBarcodeModel));
                    } else if (mode == MODE_ADD_COUPON_DISCOUNT) {

                        CouponDiscountModel model = new Gson().fromJson(data, CouponDiscountModel.class);
                        MainAppKt.getMainStore().dispatch(new AddCouponDiscount(model));
                    } else if (mode == MODE_SEARCH_SALE) {
                        startMatchingSalePersonWithProduct(data);
                    } else if (mode == MODE_SEARCH_PRODUCTION) {
//                        startDetailProduct(data);

                        ItemInfoModel productInfoModel = new Gson().fromJson(data, ItemInfoModel.class);
                        MainAppKt.getMainStore().dispatch(new AddProductInCart(productInfoModel));
                        ContextKt.toast(SearchActivity.this, getString(R.string.toast_message_add_product_success));
                    }

                    if (mode != MODE_SEARCH_PRODUCTION) //กรณีที่มาจากเมนูค้นหาสินค้า ไม่ต้องปิดหน้านี้
                        setResultData(data);
                }
            });
        }
    }

    private void bindView() {
        flRoot = findViewById(R.id.flRoot);
        flRootScanner = findViewById(R.id.flRootScanner);
        ivIcon = findViewById(R.id.ivIcon);
        edtSearch = findViewById(R.id.edtSearch);
        fabScan = findViewById(R.id.fabScan);
        rdGroup = findViewById(R.id.rdGroup);
    }

    public void setMode(int mode) {
        rdGroup.setVisibility(View.GONE);
        ivIcon.setImageDrawable(getResources().getDrawable(R.drawable.ico_close));
        flRoot.setBackgroundColor(getResources().getColor(android.R.color.white));

        if (mode == MODE_SEARCH_CUSTOMER) {
            edtSearch.setHint(getString(R.string.search_customer_activity_tab_search_customer));
        } else if (mode == MODE_SEARCH_PRODUCTION) {
//            edtSearch.setHint(getString(R.string.nav_search_product));
//            rdGroup.setVisibility(View.VISIBLE);
//            ivIcon.setImageDrawable(getResources().getDrawable(R.drawable.ico_nav_drawer));
//            flRoot.setBackgroundColor(getResources().getColor(R.color.bg_search_product));
        } else if (mode == MODE_ADD_PRODUCT) {
            edtSearch.setHint(getString(R.string.item_result_search_product_title));
        } else if (mode == MODE_SEARCH_SALE || mode == MODE_ADD_SALE_BY_PRODUCT) {
            edtSearch.setHint(getString(R.string.search_sale_title));
        } else if (mode == MODE_ADD_BAR_CODE_DISCOUNT || mode == MODE_ADD_COUPON_DISCOUNT) {
            edtSearch.setHint(getString(R.string.search_barcode_title));
        } else if (mode == MODE_ADD_QRCODE_READER) {

        }

        this.mode = mode;
    }

    public void setPage(int page) {
        if (page == PAGE_SEARCH) {
            if (scannerView != null) {
                scannerView.onPause();
                scannerView.onDestroy();
                scannerView = null;
            }

            flRootScanner.removeAllViews();
            isResume = false;
            showKeyboard();
        } else {
            if (scannerView == null) {
                initialScanner();
                if (!isResume) {
                    Handler handle = new Handler();
                    handle.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scannerView.onResume();
                            isResume = true;
                        }
                    }, 300);
                }
            }
            flRootScanner.removeAllViews();
            flRootScanner.addView(scannerView);

            hideKeyboard();
        }

        this.page = page;
    }

    private void replaceFragment(Fragment fragment, String tag) {
        if (fragment == null) {
            moveTaskToBack(true);
            System.exit(1);

            return;
        }

        FragmentTransaction transaction = fmManager.beginTransaction();
        transaction.replace(R.id.flRoot, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (scannerView != null)
            scannerView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (scannerView != null) {
            scannerView.onResume();
            isResume = true;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (page == PAGE_SEARCH) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (fmManager.getBackStackEntryCount() > 0) {
                        Fragment fragment = fmManager.getFragments().get(0);
                        if (fragment == null || fragment.getTag() == null) {

                            hideKeyboard();
                            return;
                        }

                        if (fragment.getTag().toString().equalsIgnoreCase(TAG_SEARCH_FRAGMENT)) {
                            Log(TAG_SEARCH_FRAGMENT);

                            SearchFragment.edtSearch.requestFocus();
                            showKeyboard();
                        } else if (fragment.getTag().toString().equalsIgnoreCase(TAG_RESULT_FRAGMENT)) {
                            Log(TAG_RESULT_FRAGMENT);
                            hideKeyboard();
                        }
                    } else {
                        showKeyboard();
                    }
                }
            }, 500);
        } else {
            hideKeyboard();
        }
    }

    @Override
    public void onBackPressed() {
        if (fmManager.getBackStackEntryCount() > 0) {
            Fragment fragment = fmManager.getFragments().get(0);
            if (fragment.getTag().toString().equalsIgnoreCase(TAG_SEARCH_FRAGMENT)) {
                Log(TAG_SEARCH_FRAGMENT);

                SearchFragment.edtSearch.requestFocus();
                showKeyboard();
            } else if (fragment.getTag().toString().equalsIgnoreCase(TAG_RESULT_FRAGMENT)) {
                Log(TAG_RESULT_FRAGMENT);
                showKeyboard();
            }
        } else {
            if (mode == MODE_SEARCH_PRODUCTION) {
                finish();
                EventBus.getDefault().post(new CloseSearchProductModel());
            }

            if (page == PAGE_SEARCH) {
                showKeyboard();
            } else {
                hideKeyboard();
            }
        }


        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (scannerView != null) {
            scannerView.onPause();
            scannerView.onDestroy();
            scannerView = null;
        }

        hideKeyboard();
    }

    private void startResultFragment(DiscountCouponModel discountCouponModel) {
        String data = new Gson().toJson(discountCouponModel, DiscountCouponModel.class);
        createFragment(ResultFragment.newInstance(mode, data, edtSearch.getText().toString(), ""), "ResultFragment");
    }

    private void startResultFragment(DiscountBarcodeModel discountBarcodeModel) {
        String data = new Gson().toJson(discountBarcodeModel, DiscountBarcodeModel.class);
        createFragment(ResultFragment.newInstance(mode, data, edtSearch.getText().toString(), ""), "ResultFragment");
    }

    private void startResultFragment(SearchProductListModel dataSearchProductList) {
        String data = "";
        if (dataSearchProductList == null) data = "";
        else data = new Gson().toJson(dataSearchProductList, SearchProductListModel.class);

        createFragment(ResultFragment.newInstance(mode, data, edtSearch.getText().toString(), ""), "ResultFragment");
    }

    private void startResultFragment(SalePersonModel model) {

        if (TextUtils.isEmpty(model.getId()) && TextUtils.isEmpty(model.getName())) {
            createFragment(ResultFragment.newInstance(mode, "", edtSearch.getText().toString(), productList), "ResultFragment");
        } else {
            String data = new Gson().toJson(model, SalePersonModel.class);
            createFragment(ResultFragment.newInstance(mode, data, edtSearch.getText().toString(), productList), "ResultFragment");
        }
    }

    private void requestSaleInfo(final String barcode, final int sourceOfSearch) {

        final ProgressDialog dialog = ContextKt.indeterminateProgress(SearchActivity.this, "");
        dialog.show();

        SalesPersonApi.INSTANCE.getSalesPersonInfo(barcode).subscribe(new SingleObserver<Result<SalePersonResponseModel, FuelError>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Result<SalePersonResponseModel, FuelError> salePersonResponseModelFuelErrorResult) {
                SalePersonResponseModel productInfoModel = salePersonResponseModelFuelErrorResult.component1();
                FuelError Error = salePersonResponseModelFuelErrorResult.component2();

                dialog.dismiss();

//                //mock data
//                SalePersonModel mockSalePersonalModel = mockSale();
//                String data = new Gson().toJson(mockSalePersonalModel);
//                startMatchingSalePersonWithProduct(data);
//                setResultData(data);

                if (Error == null && productInfoModel != null && productInfoModel.getError().size() == 0) {

                    SalePersonModel salePersonModel = productInfoModel.getSalesPerson();
                    String model = new Gson().toJson(salePersonModel);

                    if (mode == MODE_ADD_SALE_BY_PRODUCT) {
                        //update sale by product ( มาจากหน้า cartFragment )
                        ArrayList<InputSalesLineModel> arLstProduct = MainAppKt.getMainStore().getState().getCartInfoState().getTransactionData().getInputSalesLine();
                        if (position <= arLstProduct.size()) {
                            MainAppKt.getMainStore().dispatch(new SetSellerInProduct(arLstProduct.get(position).getItemId(), salePersonModel));
                        }
                        setResultData(model);
                    } else {
                        //add sale
                        setResultData(model);

                        startMatchingSalePersonWithProduct(model);
                    }
                } else {
                    //not found or error
//                    SalePersonModel salePersonModel = productInfoModel.getSalesPerson();
//                    startResultFragment(salePersonModel);

                    if (productInfoModel != null) {
                        if (productInfoModel.getResponseMessage() != null) {
                            String message = productInfoModel.getResponseMessage().getMessage();
                            alertItemNotFound(message, "");
                        }
                    }

                }
            }

            @Override
            public void onError(Throwable e) {
                dialog.dismiss();
            }
        });
    }

    private void requestBarCodeDiscount(final String barcode, final int sourceOfSearch) {

        final ProgressDialog dialog = ContextKt.indeterminateProgress(SearchActivity.this, "");
        dialog.show();

        Objects.requireNonNull(DiscountApi.INSTANCE.getDiscountBarcode(barcode)).subscribe(new SingleObserver<Result<DiscountBarcodeResponseModel, FuelError>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Result<DiscountBarcodeResponseModel, FuelError> discountBarcodeResponseModelFuelErrorResult) {
                DiscountBarcodeResponseModel productInfoModel = discountBarcodeResponseModelFuelErrorResult.component1();
                FuelError Error = discountBarcodeResponseModelFuelErrorResult.component2();

                dialog.dismiss();
                if (Error == null && productInfoModel != null && productInfoModel.getError().size() == 0) {

                    DiscountBarcodeModel discountBarcodeModel = productInfoModel.getDiscountBarcode();
                    MainAppKt.getMainStore().dispatch(new AddDiscountBarcode(discountBarcodeModel));
                    setResultData("");
                } else {

                    alertItemNotFound(productInfoModel.getResponseMessage().getMessage(), "");
                }
            }

            @Override
            public void onError(Throwable e) {
                dialog.dismiss();
            }
        });
    }

    private void requestCouponDiscount( String serialNumber, final int sourceOfSearch) {

        final ProgressDialog dialog = ContextKt.indeterminateProgress(SearchActivity.this, "");
        dialog.show();

//        serialNumber = "001";
//        String offerId = "CP204000034";
        String offerId = "";
        Objects.requireNonNull(DiscountApi.INSTANCE.checkDiscountCoupon(offerId, serialNumber)).subscribe(new SingleObserver<Result<DiscountCouponModel, FuelError>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Result<DiscountCouponModel, FuelError> discountCouponModelFuelErrorResult) {

                DiscountCouponModel discountCoupon = discountCouponModelFuelErrorResult.component1();
                FuelError Error = discountCouponModelFuelErrorResult.component2();

                dialog.dismiss();
                if (Error == null && discountCoupon != null && discountCoupon.getError().size() == 0) {

                    CouponDiscountModel couponDiscountModel = new CouponDiscountModel(1, 0, "", "", discountCoupon.getCouponName(), "", new ArrayList<SurveyModel>(), new ArrayList<SurveyModel>(), "");
                    MainAppKt.getMainStore().dispatch(new AddCouponDiscount(couponDiscountModel));
                    setResultData("");
                } else {

                    alertItemNotFound(discountCoupon.getResponseMessage().getMessage(), "");
                }
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }

    private void requestItemInfo(final String barcode, final int sourceOfSearch) {

        final ProgressDialog dialog = ContextKt.indeterminateProgress(SearchActivity.this, "");
        dialog.show();

        ItemApi.INSTANCE.getItemsInfo(barcode).subscribe(new SingleObserver<Result<ItemInfoResponseModel, FuelError>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Result<ItemInfoResponseModel, FuelError> productInfoModelFuelErrorResult) {
                ItemInfoResponseModel productInfoModel = productInfoModelFuelErrorResult.component1();
                FuelError Error = productInfoModelFuelErrorResult.component2();

                dialog.dismiss();
                if (Error == null && productInfoModel != null && !productInfoModel.getResponseMessage().isError()) {

                    ItemInfoModel itemInfoModel = null;
                    if (productInfoModel.getItemInfo().size() > 0)
                        itemInfoModel = productInfoModel.getItemInfo().get(0);

                    MainAppKt.getMainStore().dispatch(new AddProductInCart(itemInfoModel));
                    ContextKt.toast(SearchActivity.this, getString(R.string.toast_message_add_product_success));
                    setResultData("");
                } else {
                    String errorMessage = "";
                    if (Error != null) {
                        errorMessage = Error.getMessage();
                    }
                    if (productInfoModel != null) {
                        errorMessage = productInfoModel.getResponseMessage().getMessage();
                    }
                    alertItemNotFound(errorMessage, barcode);
                }
            }

            @Override
            public void onError(Throwable e) {
                dialog.dismiss();
            }
        });
    }

    private void requestProductInfo(final String barcode, final int sourceOfSearch) {

        final ProgressDialog dialog = ContextKt.indeterminateProgress(SearchActivity.this, "");
        dialog.show();

        ItemApi.INSTANCE.getProductInfo(barcode).subscribe(new SingleObserver<Result<ProductInfoResponseModel, FuelError>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Result<ProductInfoResponseModel, FuelError> productInfoModelFuelErrorResult) {
                ProductInfoResponseModel productInfoModel = productInfoModelFuelErrorResult.component1();
                FuelError Error = productInfoModelFuelErrorResult.component2();

                dialog.dismiss();
                if (Error == null && productInfoModel != null && productInfoModel.getError().size() == 0) {

                    ProductInfoModel productInfo = productInfoModel.getProductInfo();

                    ItemInfoModel itemInfoModel = new ItemInfoModel(productInfo.getProductId(),
                            barcode,
                            productInfo.getProductName(),
                            "",
                            false,
                            DoubleKt.toString2Digit(productInfo.getPrice()),
                            null,
                            true,
                            null,
                            false);

                    MainAppKt.getMainStore().dispatch(new AddProductInCart(itemInfoModel));
                    ContextKt.toast(SearchActivity.this, getString(R.string.toast_message_add_product_success));
                    setResultData("");
                } else {
                    alertItemNotFound(productInfoModel.getResponseMessage().getMessage(), "");
                }
            }

            @Override
            public void onError(Throwable e) {
                dialog.dismiss();
            }
        });
    }

    private void requestSearchProduct(final String searchValue) {

        final ProgressDialog dialog = ContextKt.indeterminateProgress(SearchActivity.this, "");
        Objects.requireNonNull(dialog).show();
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.CENTER);

        ItemApi.INSTANCE.searchProduct(searchValue, "", "yyy", "xxxx").subscribe(new SingleObserver<Result<SearchProductListModel, FuelError>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Result<SearchProductListModel, FuelError> productInfoModelFuelErrorResult) {
                SearchProductListModel searchList = productInfoModelFuelErrorResult.component1();
                FuelError Error = productInfoModelFuelErrorResult.component2();

                dialog.dismiss();
                if (Error == null && searchList != null && searchList.getError().isEmpty()) {
                    startResultFragment(searchList);
                } else {
                    if (mode == MODE_SEARCH_PRODUCTION)
                        startResultFragment(searchList);
                    else
                        alertItemNotFound(searchList.getResponseMessage().getMessage(), "");
                }
            }

            @Override
            public void onError(Throwable e) {
                dialog.dismiss();
            }
        });
    }

    private void alertItemNotFound(final String message, final String dataSearch) {
        dlgAlert = new Dialog(this, R.style.dialog_theme);
        dlgAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAlert.setContentView(R.layout.dlg_search_again);
        dlgAlert.setCancelable(true);

        TextView tvTitle = dlgAlert.findViewById(R.id.tvTitle);
        tvTitle.setText(message);

        TextView tvOk = dlgAlert.findViewById(R.id.tvOk);
        if (mode == MODE_ADD_PRODUCT)
            tvOk.setVisibility(View.VISIBLE);
        else
            tvOk.setVisibility(View.GONE);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgAlert.cancel();

                if (mode == MODE_ADD_PRODUCT) {

                    if (page == PAGE_SCANNER) {
                        clearScanner();
                    }

                    createFragment(SearchFragment.newInstance(mode, dataSearch,
                            getResources().getString(R.string.item_result_search_product_title),
                            true), "SearchFragment");
                }
                //ถ้ากดปุ่ม ค้นหาข้อมูล ระบบจะเรียก api ต่อให้ แล้วเปิดหน้า result เลย
            }
        });

        TextView tvClose = dlgAlert.findViewById(R.id.tvClose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgAlert.cancel();

                if (page == PAGE_SCANNER) {
                    clearScanner();
                    setPage(page);
                } else {
                    //ถ้ากดปุ่ม ปิด ระบบจะไม่เรียก api ต่อให้ user ต้องพิมพ์เอง แล้วกดปุ่ม search จาก keyboard
                    showKeyboard();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (SearchFragment.edtSearch != null)
                                SearchFragment.edtSearch.requestFocus();
                            else
                                Log("edtSearch == null");
                        }
                    }, 300);
                }

            }
        });
        dlgAlert.show();
    }

    private void clearScanner() {
        if (scannerView != null) {
            scannerView.onPause();
            scannerView.onDestroy();
            scannerView = null;
        }

        flRootScanner.removeAllViews();
        isResume = false;
    }
}
