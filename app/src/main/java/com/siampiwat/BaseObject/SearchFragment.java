package com.siampiwat.BaseObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.result.Result;
import com.google.gson.Gson;
import com.siampiwat.API.ItemApi;
import com.siampiwat.Extention.ContextKt;
import com.siampiwat.Model.CustomerModel;
import com.siampiwat.Model.ProductBarcodeModel;
import com.siampiwat.Model.ProductInfoModel;
import com.siampiwat.Model.SearchProductListModel;
import com.siampiwat.R;

import java.util.ArrayList;
import java.util.Objects;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

import static com.siampiwat.BaseObject.SearchActivity.MODE_ADD_PRODUCT;
import static com.siampiwat.BaseObject.SearchActivity.MODE_SEARCH_CUSTOMER;
import static com.siampiwat.BaseObject.SearchActivity.MODE_SEARCH_PRODUCTION;


public class SearchFragment extends Fragment {
    private static final String PARAM_MODE = "param1";
    private static final String PARAM_DATA = "param2";
    private static final String PARAM_HINT = "param3";
    private static final String PARAM_DO_SEARCH = "param4";


    private int mode;
    private String data;
    private String hint;
    private boolean doSearch = false;

    private Activity activity;
    public static EditText edtSearch;
    private ImageView ivIcon;

    private Dialog dlgAlert = null;
    private onClick listener = null;

    public SearchFragment() {

    }

    public interface onClick
    {
        void onClickBack();
        void onNotFound(String keyword);
        void onSelected( String data , String keyword );
    }

    public static SearchFragment newInstance(int mode, String data , String hint , boolean doSearch) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putInt(PARAM_MODE, mode);
        args.putString(PARAM_DATA, data);
        args.putString(PARAM_HINT, hint);
        args.putBoolean(PARAM_DO_SEARCH, doSearch);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mode = getArguments().getInt(PARAM_MODE);
            data = getArguments().getString(PARAM_DATA);
            hint= getArguments().getString(PARAM_HINT);
            doSearch= getArguments().getBoolean(PARAM_DO_SEARCH);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bindView(view);
        bindEvent();
        bindValue();

        if( doSearch )
            performSearch();
    }

    private void bindView( View view )
    {
        edtSearch = view.findViewById(R.id.edtSearch);
        ivIcon = view.findViewById(R.id.ivIcon);
    }

    private void bindEvent()
    {
        ivIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( listener != null )
                    listener.onClickBack();
            }
        });

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });
    }

    public void setListener( onClick listener )
    {
        this.listener = listener;
    }

    private void bindValue()
    {
        edtSearch.setText(data);
        edtSearch.setHint(hint);
    }

//    private void performSearch()
//    {
//        edtSearch.clearFocus();
//        InputMethodManager in = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//        if( in != null )
//            in.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
//
//        //call api
////        showAlertDialog();
//    }

    private void performSearch()
    {
//        edtSearch.clearFocus();

        if( TextUtils.isEmpty(edtSearch.getText().toString()) ) {
            BaseOnActivity.getInstance().hideKeyboard();
            return;
        }

        String data = "";
        if( mode == MODE_ADD_PRODUCT )
        {
            //call api
//            ArrayList<ProductInfoModel> arLstModel = new ArrayList<>();
//
//            ArrayList<ProductBarcodeModel> barcodeModel1 = new ArrayList<>();
//            barcodeModel1.add( new ProductBarcodeModel("0090000016578") );
//            ProductInfoModel model = new ProductInfoModel("001", "กระเป๋า",1000,  barcodeModel1, null,null,false);
//            arLstModel.add(model);
//
//            ArrayList<ProductBarcodeModel> barcodeModel2 = new ArrayList<>();
//            barcodeModel2.add( new ProductBarcodeModel("0090000016578") );
//            ProductInfoModel model2 = new ProductInfoModel("002", "นาฬิกา",1000, barcodeModel2, null,null,false);
//            arLstModel.add(model2);
//
//            data = new Gson().toJson(arLstModel);

            requestSearchProduct(edtSearch.getText().toString());

        }
        else if( mode == MODE_SEARCH_PRODUCTION )
        {
            //call api
            ArrayList<ProductInfoModel> arLstModel = new ArrayList<>();

            ArrayList<ProductBarcodeModel> barcodeModel1 = new ArrayList<>();
            barcodeModel1.add( new ProductBarcodeModel("0090000016578") );
            ProductInfoModel model = new ProductInfoModel("001", "กระเป๋า",100,  barcodeModel1, null,null,false);
            arLstModel.add(model);

            ArrayList<ProductBarcodeModel> barcodeModel2 = new ArrayList<>();
            barcodeModel2.add( new ProductBarcodeModel("0090000016578") );
            ProductInfoModel model2 = new ProductInfoModel("002", "นาฬิกา",100, barcodeModel2, null,null,false);
            arLstModel.add(model2);


//            data = new Gson().toJson(model4);
            data = new Gson().toJson(arLstModel);
        }
        else if( mode == MODE_SEARCH_CUSTOMER )
        {
            //call api
            ArrayList<CustomerModel> arLstModel = new ArrayList<>();
            CustomerModel model = new CustomerModel();
            model.type = "นักเรียน";
            model.name = "ณัฐพงค์ เมืองแมน";
            model.id = "001";
            arLstModel.add(model);

            CustomerModel model1 = new CustomerModel();
            model1.type = "ประชาชน";
            model1.name = "รสสุรินทร์ สุวรรณเขต";
            model1.id = "002";
            arLstModel.add(model1);

            CustomerModel model2 = new CustomerModel();
            model2.type = "ประชาชน";
            model2.name = "วลัย พรหมพรรณ";
            model2.id = "003";
            arLstModel.add(model2);

            data = new Gson().toJson(arLstModel);
        }

//        if( listener != null )
//        {
//            listener.onSelected(data);
//        }

        //reset value
        doSearch = false;
    }

    private void showAlertDialog()
    {
        dlgAlert = new Dialog(activity,R.style.dialog_theme);
        dlgAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAlert.setContentView(R.layout.dlg_confirm);
        dlgAlert.setCancelable(true);

        TextView tvOk = dlgAlert.findViewById(R.id.tvOk);
        tvOk.setText(getResources().getString(R.string.common_ok));
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgAlert.cancel();
                if( listener != null )
                    listener.onNotFound(edtSearch.getText().toString());
            }
        });
        dlgAlert.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.activity = getActivity();
    }

    private void requestSearchProduct(final String searchValue) {

        final ProgressDialog dialog = ContextKt.indeterminateProgress(activity, "");
        Objects.requireNonNull(dialog).show();
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.CENTER);

        ItemApi.INSTANCE.searchProduct(searchValue, "" , "" , "").subscribe(new SingleObserver<Result<SearchProductListModel, FuelError>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Result<SearchProductListModel, FuelError> productInfoModelFuelErrorResult) {
                SearchProductListModel searchList = productInfoModelFuelErrorResult.component1();
                FuelError Error = productInfoModelFuelErrorResult.component2();

                dialog.dismiss();
                if (Error == null && searchList != null && searchList.getError().isEmpty()) {
                    if( listener != null )
                    {
                        String data = new Gson().toJson(searchList.getProducts());
                        listener.onSelected(data , searchValue);
                    }
                } else {
                    listener.onNotFound(searchValue);
                }
            }

            @Override
            public void onError(Throwable e) {
                dialog.dismiss();
            }
        });
    }


}
