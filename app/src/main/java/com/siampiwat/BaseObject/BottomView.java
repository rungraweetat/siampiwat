package com.siampiwat.BaseObject;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.siampiwat.R;

/**
 * TODO: document your custom view class.
 */
public class BottomView extends FrameLayout {
    private String navigatorTitle;
    private Button btnAccept;
    private LinearLayout llContainer;
    private onClickNavigator listener;

    public interface onClickNavigator
    {
        public void onAccept();
    }

    public BottomView(Context context) {
        super(context);
        init(null);
    }

    public BottomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public BottomView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.base_bottom_view, this);
        bindView();

        if( attrs != null ) {
            setupStyleable(attrs);
            setBottomTitle(navigatorTitle);
        }
    }

    public void setListener( onClickNavigator listener )
    {
        this.listener = listener;
    }
    private void bindView()
    {
        btnAccept = findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if( listener != null )
                {
                    listener.onAccept();
                }
                else
                    Log.e("NavigatorView","Please setListener first!!");
            }
        });

        llContainer = findViewById(R.id.llContainer);
    }

    public LinearLayout getView()
    {
        return llContainer;
    }

    public String getBottomTitle() {
        return navigatorTitle;
    }

    public void setBottomTitle(String title) {
        navigatorTitle = title;

        updateUI();
    }

    public void updateUI()
    {
        btnAccept.setText(navigatorTitle);

        invalidate();
    }

    private void setupStyleable(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.BottomView);
        navigatorTitle = typedArray.getString(R.styleable.BottomView_title_bottom);
        typedArray.recycle();
    }
}
