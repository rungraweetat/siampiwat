package com.siampiwat.BaseObject;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.siampiwat.R;

/**
 * TODO: document your custom view class.
 */
public class NavigatorView extends FrameLayout {
    private String navigatorTitle; // TODO: use a default from R.string...
    private int mExampleColor = Color.RED; // TODO: use a default from R.color...
    private float mExampleDimension = 0; // TODO: use a default from R.dimen...
    private Drawable iconDrawable;

    private TextView tvTitle;
    private ImageView ivIcon;
    private LinearLayout llContainer;
    private onClickNavigator listener;

    public interface onClickNavigator
    {
       public void onClickBack();
    }

    public NavigatorView(Context context) {
        super(context);
        init(null);
    }

    public NavigatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public NavigatorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.base_header_view, this);

        bindView();
        if( attrs != null )
        {
            setupStyleable(attrs);
            setIconDrawable(iconDrawable);
            setNavigatorTitle(navigatorTitle);
        }
    }

    public void setListener( onClickNavigator listener )
    {
        this.listener = listener;
    }
    private void bindView()
    {
        llContainer = findViewById(R.id.llContainer);
        llContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if( listener != null )
                    listener.onClickBack();
                else
                    Log.e("NavigatorView","Please setListener first!!");
            }
        });

        tvTitle = findViewById(R.id.tvTitle);
        ivIcon = findViewById(R.id.ivIcon);
    }

    public LinearLayout getView()
    {
        return llContainer;
    }

    public String getNavigatorTitle() {
        return navigatorTitle;
    }

    public void setNavigatorTitle(String title) {
        navigatorTitle = title;

        updateUI();
    }

    public void updateUI()
    {
        tvTitle.setText(navigatorTitle);
        ivIcon.setImageDrawable(iconDrawable);

        invalidate();
    }

    public Drawable getIconDrawable() {
        return iconDrawable;
    }

    public void setIconDrawable(Drawable exampleDrawable) {
        iconDrawable = exampleDrawable;

        updateUI();
    }

    private void setupStyleable(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.NavigatorView);
        iconDrawable = typedArray.getDrawable(R.styleable.NavigatorView_icon_navigator);
        navigatorTitle = typedArray.getString(R.styleable.NavigatorView_title_navigator);
        typedArray.recycle();
    }
}
