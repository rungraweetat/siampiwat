package com.siampiwat.Payment

import android.graphics.Bitmap
import android.os.Bundle
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.R
import kotlinx.android.synthetic.main.activity_payment_qr_code.*

class PaymentQRCodeActivity : BaseOnActivity() {
    var qrCodeData = ""
    var title = ""
    companion object {

        val qrCodeWidth = 500
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_qr_code)

        val bundle = intent.extras
        if( bundle != null )
        {
            if( bundle.containsKey("data") )
            {
                qrCodeData = bundle.getString("data")!!
                title = bundle.getString("title")!!
                try {
                    textToImageEncode(qrCodeData)
                } catch (e: WriterException) {
                    e.printStackTrace()
                }
            }
        }

        bindEvent()

    }

    private fun bindEvent() {
        navigatorView?.setListener{
            finish()
        }

        navigatorView?.navigatorTitle = title
    }


    @Throws(WriterException::class)
    private fun textToImageEncode(Value: String) {
        val bitMatrix: BitMatrix
        try {
            bitMatrix = MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.QR_CODE,
                    qrCodeWidth, qrCodeWidth, null
            )

        } catch (Illegalargumentexception: IllegalArgumentException) {
            return
        }

        val bitMatrixWidth = bitMatrix.getWidth()

        val bitMatrixHeight = bitMatrix.getHeight()

        val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)

        for (y in 0 until bitMatrixHeight) {
            val offset = y * bitMatrixWidth

            for (x in 0 until bitMatrixWidth) {

                pixels[offset + x] = if (bitMatrix.get(x, y))
                    resources.getColor(R.color.black)
                else
                    resources.getColor(R.color.white)
            }
        }
        val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight)

        try {
            qrCodeImageView!!.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }

}