package com.siampiwat.Payment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.siampiwat.*
import com.siampiwat.API.LinePAYApi
import com.siampiwat.API.PaymentApi
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.BaseObject.SearchActivity
import com.siampiwat.EventBus.M2000PostSurveyModel
import com.siampiwat.Extention.indeterminateProgress
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.MenuCart.ResultTransactionActivity
import com.siampiwat.Model.PaymentModel
import com.siampiwat.Model.RequestPaymentModel
import com.siampiwat.Model.TransactionDataModel
import com.siampiwat.Model.TransactionDataResponseModel
import com.siampiwat.State.Action.AddTransactionDataCalculate
import com.siampiwat.State.State.CartInfoState
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_payment_method.*
import kotlinx.android.synthetic.main.item_remain_payment.*
import kotlinx.android.synthetic.main.layout_payment_period.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.rekotlin.StoreSubscriber


class PaymentMethodActivity : BaseOnActivity(), StoreSubscriber<CartInfoState> {
    var parentMenuId: String = ""
    private var posRefNo: String = getPOSRefNo()
    private var transactionDataCalculate: TransactionDataModel? = null
    private var oneTimeKeyLinePAY = ""
    private var paymentId = ""
    private var requestPaymentModel:RequestPaymentModel? = null
    private var paymentModel:PaymentModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_method)
        btnNext.visibility = View.GONE

        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this)
        val bundle = intent.extras
        if (bundle != null) {
            if (bundle.containsKey("parentMenuId")) {
                parentMenuId = bundle.getString("parentMenuId")!!

                requestPaymentModel = RequestPaymentModel()

            }
        }
        bindEvent()
        hideKeyboard()

        if( TextUtils.isEmpty(parentMenuId) )
        {
            addAlreadyPayment()
        }
        else{
            llContentAlreadyPayment.visibility = View.GONE
            llContentRemainPayment.visibility = View.GONE
        }

//        requestPaymentModel = RequestPaymentModel()
//        requestPaymentModel!!.PaymentId = "8201"
//        requestPaymentModel!!.PaymentAmount = 100.00
//        requestPaymentModel!!.TransactionRef3 = transactionDataCalculate?.TransactionId!!
//        requestPaymentModel!!.IsBlankOperation = false
//
//        requestPayment(transactionDataCalculate?.TransactionId!! ,
//                requestPaymentModel!!.PaymentId.toString() ,
//                requestPaymentModel!!.PaymentAmount.toString() ,
//                requestPaymentModel = requestPaymentModel!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)

        if (EventBus.getDefault().isRegistered(this)) EventBus.getDefault().unregister(this)
    }
    override fun newState(state: CartInfoState) {
        transactionDataCalculate = state.transactionDataCalculate
        txtNetPrice.text = transactionDataCalculate?.SalesNetAmount?.toString2Digit()
    }

    private fun getPOSRefNo(): String {
        val tsLong = System.currentTimeMillis() / 1000
        return tsLong.toString()
    }


    private fun bindEvent() {
        navigatorView?.setListener {
            finish()
        }

        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }.skipRepeats()
        }

        val paymentMenu = userInfo?.InitScreenData?.getPayment(parentId = parentMenuId)
        if( parentMenuId == "200" || parentMenuId == "410" || parentMenuId == "420" )
        {
            bindViewQRCodePayment(paymentMenu)
        }
        else{
            llRooPaymentMethod.removeAllViews()
            paymentMenu?.forEach {
                var llTemplateDiscount: View? = null
                llTemplateDiscount = LayoutInflater.from(this@PaymentMethodActivity).inflate(R.layout.item_simple_with_image, null)

                val imageView = llTemplateDiscount.findViewById<ImageView>(R.id.image)
                val textName = llTemplateDiscount.findViewById<TextView>(R.id.txtName)

                if (it.Image.isNotEmpty()) {
                    convertBase64ToImage(it.Image, imageView)
//                Picasso.with(applicationContext).load(it.Image).into(imageView)
                } else {
                    imageView.setImageDrawable(null)
                }
                textName.text = it.LabelTH

                var paymentModel = it
                val viewClickListener = View.OnClickListener {
                    paymentId = paymentModel!!.Id
                    val paymentChildMenu = userInfo?.InitScreenData?.getPayment(parentId = paymentId)
                    if (paymentChildMenu?.size!! > 0) {

                        val intent = Intent(this, PaymentMethodActivity::class.java)
                        intent.putExtra("parentMenuId", paymentId)
                        startActivity(intent)
                    } else {
                        handlePaymentMenuId(paymentId)
                    }
                }

                llTemplateDiscount.setOnClickListener(viewClickListener)
                llRooPaymentMethod.addView(llTemplateDiscount)
            }
        }

        btnNext.setListener{

            if( TextUtils.isEmpty(edtAmountPayment.text) ) {

                longToast("กรุณาระบุจำนวนเงิน")
            }
            else
            {
                if( isValidate() )
                {
                    handlePaymentMenuId(paymentId)
                }

//                var intent = Intent(this, ResultTransactionActivity::class.java)
////                intent.putExtra("isPayted", true)
//                intent.putExtra("parentMenuId" , parentMenuId)
//                startActivity(intent)
//                finish()

            }
        }
    }

    private fun isValidate():Boolean
    {
//        var totalAlreadyPayment = getTotalAlreadyPayment()
//        var remain = getAmountPayment()
//
//        if( transactionDataCalculate!!.SalesNetAmount - (totalAlreadyPayment + remain) < 0 )
//        {
//            var netAmount = transactionDataCalculate?.SalesNetAmount!!
//            var totalAlreadyPayment = getTotalAlreadyPayment()
//            remain = netAmount-totalAlreadyPayment
//
//            edtAmountPayment.setText(remain.toString2Digit())
//        }

        if( paymentModel != null )
        {
            if( paymentModel!!.OperationName.equals("Pay cash") )
            {
                if( ( getAmountPayment() <= transactionDataCalculate!!.PaymentDueAmount+9999 ) )
                    return true
                else
                {
                    longToast("ไม่สามารถกรอกจำนวนเงินเกิน "+(transactionDataCalculate!!.PaymentDueAmount+9999).toString())
                    return false
                }

            }
            else if( paymentModel!!.PaymentId == "8114" )
            {
                if( getAmountPayment() <= transactionDataCalculate!!.PaymentDueAmount )
                    return true
                else{
                    longToast("ไม่สามารถกรอกจำนวนเงินเกิน "+(transactionDataCalculate!!.PaymentDueAmount).toString())
                    return false
                }
            }
        }

        return false
    }

    private fun getAmountPayment():Double
    {
        if( TextUtils.isEmpty(edtAmountPayment.text) ) return 0.0

        return edtAmountPayment.text.toString().replace(",","").toDouble()
    }

    private fun bindViewQRCodePayment(paymentMenu:List<PaymentModel>?)
    {
        llRooPaymentMethod.removeAllViews()

        paymentMenu?.forEach {
            var llTemplateDiscount: View? = null
            llTemplateDiscount = LayoutInflater.from(this@PaymentMethodActivity).inflate(R.layout.item_qrcode_payment, null)

            val imageView = llTemplateDiscount.findViewById<ImageView>(R.id.image)
            val textName = llTemplateDiscount.findViewById<TextView>(R.id.txtName)
            val ivSelected = llTemplateDiscount.findViewById<ImageView>(R.id.ivSelected)

            if (it.Image.isNotEmpty()) {
                convertBase64ToImage(it.Image, imageView)
            } else {
                imageView.setImageDrawable(null)
            }
            textName.text = it.LabelTH

            var paymentInfo = it
            val viewClickListener = View.OnClickListener {

                if( !TextUtils.isEmpty(parentMenuId) )
                    paymentModel = paymentInfo

                paymentId = paymentInfo.Id
                clearSelected(paymentMenu)
                ivSelected.setImageDrawable(resources.getDrawable(R.drawable.ico_radio_button_active))
                btnNext.visibility = View.VISIBLE
                btnNext.tag = it.tag
            }

            llTemplateDiscount.setOnClickListener(viewClickListener)
            llRooPaymentMethod.addView(llTemplateDiscount)
        }

        var llRootPaymentPeriod: View? = null
        llRootPaymentPeriod = LayoutInflater.from(this@PaymentMethodActivity).inflate(R.layout.layout_payment_period, null)
        llRooPaymentMethod.addView(llRootPaymentPeriod)

        var netAmount = transactionDataCalculate?.SalesNetAmount
        var totalAlreadyPayment = getTotalAlreadyPayment()
        var remainPayment = 0.0

        remainPayment = netAmount!! - totalAlreadyPayment

        edtAmountPayment.setText(remainPayment.toString2Digit())
    }

    private fun clearSelected( paymentMenu:List<PaymentModel>? )
    {
        for( i in paymentMenu!!.indices )
        {
            val ivSelected = llRooPaymentMethod.getChildAt(i).findViewById<ImageView>(R.id.ivSelected)
            ivSelected.setImageDrawable(resources.getDrawable(R.drawable.ico_radio_button))
        }
    }

    private fun getCurrentPayment():String{
        for (i in 0..llRooPaymentMethod.childCount)
        {
            val txtName = llRooPaymentMethod.getChildAt(i).findViewById<TextView>(R.id.txtName)
            val ivSelected = llRooPaymentMethod.getChildAt(i).findViewById<ImageView>(R.id.ivSelected)

            val bmpSelected = (ivSelected.getDrawable() as BitmapDrawable).bitmap
            val bmpActive = (resources.getDrawable(R.drawable.ico_radio_button_active) as BitmapDrawable).bitmap
            if( bmpSelected == bmpActive )
                return txtName.text.toString()
        }

        return ""
    }


    private fun convertBase64ToImage(encodedImage: String, img: ImageView) {
        val decodedString = Base64.decode(encodedImage, Base64.DEFAULT)
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

        img.setImageBitmap(decodedByte)
    }


    private fun handlePaymentMenuId(paymentId: String) {
        when (paymentId) {
            PaymentMethod.CreditCard -> {
                val i = Intent(this@PaymentMethodActivity, PaymentCreditCardActivity::class.java)
                startActivity(i)
            }
            PaymentMethod.ViaSCBapp -> {
                startAppPaymentSCB(PaymentSCBTradeType.Card, RequestSCBPayment.Card)
            }
            PaymentMethod.ViaExtEDC -> {
                val i = Intent(this@PaymentMethodActivity, PaymentEDCActivity::class.java)
                startActivity(i)
            }
            PaymentMethod.EWallet -> {

            }
            PaymentMethod.WeChatPay -> {
                startAppPaymentSCB(PaymentSCBTradeType.WeChat, RequestSCBPayment.WeChat)
            }
            PaymentMethod.AliPay -> {
                startAppPaymentSCB(PaymentSCBTradeType.AliPay, RequestSCBPayment.AliPay)
            }
            PaymentMethod.RabbitLINEPay -> {

                val intent = Intent(this, SearchActivity::class.java)
                intent.putExtra("mode", SearchActivity.MODE_ADD_QRCODE_READER)
                intent.putExtra("page", SearchActivity.PAGE_SCANNER)
                startActivityForResult(intent, RequestCodeScanQR.RabbitLINEPay)

            }
            PaymentMethod.TRUEMoney -> {

            }
            PaymentMethod.AirPay -> {

            }
            PaymentMethod.BluePay -> {
                val intent = Intent(this, SearchActivity::class.java)
                intent.putExtra("mode", SearchActivity.MODE_ADD_QRCODE_READER)
                intent.putExtra("page", SearchActivity.PAGE_SCANNER)
                startActivityForResult(intent, RequestCodeScanQR.BluePay)
            }
            PaymentMethod.PromptPay -> {
                startAppPaymentSCB(PaymentSCBTradeType.ThaiQRCode, RequestSCBPayment.ThaiQRCode)
            }
            PaymentMethod.CashCoupons -> {

            }
            PaymentMethod.SiamPiwatCoupons -> {

            }
            PaymentMethod.SPWCoupon -> {
                var amount = getAmountPayment()

                requestPaymentModel!!.PaymentId = paymentModel!!.PaymentId
                requestPaymentModel!!.PaymentAmount = amount
                requestPaymentModel!!.TransactionRef3 = transactionDataCalculate?.TransactionId!!
                requestPaymentModel!!.IsBlankOperation = false
                requestPaymentModel!!.OperationName = paymentModel?.OperationName

                requestPayment(transactionDataCalculate?.TransactionId!! , requestPaymentModel!!.PaymentId.toString() , amount.toString() , requestPaymentModel = requestPaymentModel!!)

            }
            PaymentMethod.DCRCoupon -> {
                var amount = getAmountPayment()

                requestPaymentModel!!.PaymentId = paymentModel!!.PaymentId
                requestPaymentModel!!.PaymentAmount = amount
                requestPaymentModel!!.TransactionRef3 = transactionDataCalculate?.TransactionId!!
                requestPaymentModel!!.IsBlankOperation = false
                requestPaymentModel!!.OperationName = paymentModel?.OperationName

                requestPayment(transactionDataCalculate?.TransactionId!! , requestPaymentModel!!.PaymentId.toString() , amount.toString() , requestPaymentModel = requestPaymentModel!!)
            }
            PaymentMethod.ALANDCoupon -> {
                var amount = getAmountPayment()

                requestPaymentModel!!.PaymentId = paymentModel!!.PaymentId
                requestPaymentModel!!.PaymentAmount = amount
                requestPaymentModel!!.TransactionRef3 = transactionDataCalculate?.TransactionId!!
                requestPaymentModel!!.IsBlankOperation = false
                requestPaymentModel!!.OperationName = paymentModel?.OperationName

                requestPayment(transactionDataCalculate?.TransactionId!! , requestPaymentModel!!.PaymentId.toString() , amount.toString() , requestPaymentModel = requestPaymentModel!!)
            }
            PaymentMethod.PartnerCoupons -> {

            }
            PaymentMethod.HUBBACoupon -> {

                var amount = getAmountPayment()

                requestPaymentModel!!.PaymentId = paymentModel!!.PaymentId
                requestPaymentModel!!.PaymentAmount = amount
                requestPaymentModel!!.TransactionRef3 = transactionDataCalculate?.TransactionId!!
                requestPaymentModel!!.IsBlankOperation = false
                requestPaymentModel!!.OperationName = paymentModel?.OperationName

                requestPayment(transactionDataCalculate?.TransactionId!! , requestPaymentModel!!.PaymentId.toString() , amount.toString() , requestPaymentModel = requestPaymentModel!!)
            }
            PaymentMethod.UPlanCoupon -> {
                var amount = getAmountPayment()

                requestPaymentModel!!.PaymentId = paymentModel!!.PaymentId
                requestPaymentModel!!.PaymentAmount = amount
                requestPaymentModel!!.TransactionRef3 = transactionDataCalculate?.TransactionId!!
                requestPaymentModel!!.IsBlankOperation = false
                requestPaymentModel!!.OperationName = paymentModel?.OperationName

                requestPayment(transactionDataCalculate?.TransactionId!! , requestPaymentModel!!.PaymentId.toString() , amount.toString() , requestPaymentModel = requestPaymentModel!!)
            }
            PaymentMethod.MEV -> {

            }
        }
    }

    private fun startResultTransaction( refTransaction: String) {

//        val arLstSurvey = userInfo?.getPostSurvey("M2000")
//        val surveyJson = Gson().toJson(arLstSurvey)
//        if (arLstSurvey!!.size < 1) {
//            var intent = Intent(this, ResultTransactionActivity::class.java)
//            startActivity(intent)
//        } else {
//            val i = Intent(this, SurveyActivity::class.java)
//            i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
//            i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2000_PostSurvey)
//            startActivity(i)
//        }

        var remain = getAmountPayment()

        var intent = Intent(this, ResultTransactionActivity::class.java)
        var totalAlreadyPayment = getTotalAlreadyPayment()

        if( transactionDataCalculate?.SalesNetAmount!! - (totalAlreadyPayment+remain) <= 0 )
        {
            //จ่ายครบแล้ว
            intent.putExtra("isPayted",true)
        }
        intent.putExtra("parentMenuId" , parentMenuId)
        startActivity(intent)
        finish()
    }

    private fun getTotalAlreadyPayment():Double
    {
        return mainStore.state.cartInfoState.transactionDataCalculate.totalAlreadyPayment()
    }

    private fun startAppPaymentSCB(type: String, requestCode: Int) {
        posRefNo = getPOSRefNo()
        val pm = packageManager
        val intent = pm.getLaunchIntentForPackage(getString(R.string.package_payment_scb))
        if (intent != null) {
            intent.flags = 0
            intent.putExtra("trade_type", type)
            intent.putExtra("pos_ref_no", posRefNo)
            intent.putExtra("amount", transactionDataCalculate?.SalesNetAmount)
            startActivityForResult(intent, requestCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RequestCodeScanQR.BluePay -> {

            }
            RequestCodeScanQR.RabbitLINEPay -> {

                data?.let {
                    oneTimeKeyLinePAY = it.getStringExtra("data")
                    linePAYRequestPayment()
                }

            }
            RequestCodeScanQR.PromptPay -> {

            }
            RequestSCBPayment.Card -> {
                if( resultCode == Activity.RESULT_OK )
                    startResultTransaction("")
            }
            RequestSCBPayment.AliPay -> {
                if( resultCode == Activity.RESULT_OK )
                    startResultTransaction("")
            }
            RequestSCBPayment.WeChat -> {
                if( resultCode == Activity.RESULT_OK )
                    startResultTransaction("")
            }
            RequestSCBPayment.ThaiQRCode -> {
                if( resultCode == Activity.RESULT_OK )
                    startResultTransaction("")
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun linePAYRequestPayment() {
        if (oneTimeKeyLinePAY.length == 0) {
            return
        }
        var dialog = this.indeterminateProgress("")

        posRefNo = getPOSRefNo()
        var amount = 0.0
        if( !TextUtils.isEmpty(edtAmountPayment.text.toString()) )
        {
            amount = getAmountPayment()
        }
        else
        {
            longToast("กรุณาระบุจำนวนเงิน")
            return
        }

//        var amount = transactionDataCalculate?.SalesNetAmount!!
//        amount = 1.0
        LinePAYApi.paymentRequest("ชำระสินค้า SIAM PIWAT", amount, posRefNo, "-", "-", oneTimeKeyLinePAY)
                .subscribe { Response ->
                    val (linePAYPaymentRequestModel, Error) = Response
                    dialog?.dismiss()
                    if (linePAYPaymentRequestModel?.returnCode == "0000") {
                        linePAYConfirmPayment(linePAYPaymentRequestModel?.info!!.transactionId, amount)
                    } else {
                        if (Error != null) {
                            this.toast(Error.message.toString())
                        } else {
                            this.toast(linePAYPaymentRequestModel?.returnMessage!!)
                        }
                    }
                }
    }

    @SuppressLint("CheckResult")
    private fun linePAYConfirmPayment(transactionId: String, amount: Double) {
        var dialog = this.indeterminateProgress("")

        LinePAYApi.confirmPayment(transactionId, amount)
                .subscribe { Response ->
                    val (linePAYConfirmPaymentResponse, Error) = Response
                    dialog?.dismiss()
                    if (linePAYConfirmPaymentResponse?.returnCode == "0000") {
                        requestPaymentModel!!.PaymentId = paymentModel!!.Id
                        requestPaymentModel!!.PaymentAmount = amount
                        requestPaymentModel!!.TransactionRef3 = transactionId
                        requestPaymentModel!!.IsBlankOperation = false
                        requestPaymentModel!!.OperationName = paymentModel?.OperationName

                        requestPayment(transactionDataCalculate?.TransactionId!! , requestPaymentModel!!.PaymentId.toString() , amount.toString() , requestPaymentModel = requestPaymentModel!!)
                    } else {
                        if (Error != null) {
                            this.toast(Error.message.toString())
                        } else {
                            this.toast(linePAYConfirmPaymentResponse?.returnMessage!!)
                        }
                    }
                }
    }

    @Subscribe
    public fun onEvent( event:M2000PostSurveyModel )
    {
        var intent = Intent(this, ResultTransactionActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun addAlreadyPayment()
    {
        llContentAlreadyPayment.removeAllViews()
        for (i in mainStore.state.cartInfoState.transactionData.Payment.indices)
        {
            val vItemPaymented = LayoutInflater.from(this@PaymentMethodActivity).inflate(R.layout.item_paymented_, null)
            val flContentAlreadyPayment = vItemPaymented.findViewById(R.id.flContentAlreadyPayment) as FrameLayout
            val tvName = vItemPaymented.findViewById(R.id.txtName) as TextView
            val tvPaymented = vItemPaymented.findViewById(R.id.tvPaymented) as TextView

            tvName.text = mainStore.state.cartInfoState.transactionData.Payment[i].OperationName
            tvPaymented.text = mainStore.state.cartInfoState.transactionData.Payment[i].PaymentAmount.toString2Digit()

            if( i == 0 )
                flContentAlreadyPayment.visibility = View.VISIBLE
            else
                flContentAlreadyPayment.visibility = View.GONE

            llContentAlreadyPayment.addView(vItemPaymented)
            llContentAlreadyPayment.visibility = View.VISIBLE
        }

        llContentRemainPayment.visibility = View.VISIBLE
        val netAmount = transactionDataCalculate?.SalesNetAmount
        tvRemainPayment.text = (netAmount!! - transactionDataCalculate!!.totalAlreadyPayment()).toString2Digit()
    }


    private fun requestPayment(transactionId:String , paymentId:String , amountPayment:String , requestPaymentModel: RequestPaymentModel)
    {

        val dialog = indeterminateProgress("")
        dialog!!.show()

        PaymentApi.payment(transactionId , paymentId , amountPayment , requestPaymentModel).subscribe(object : SingleObserver<Result<TransactionDataResponseModel, FuelError>> {
            override fun onSubscribe(d: Disposable) {

            }

            override fun onSuccess(discountBarcodeResponseModelFuelErrorResult: Result<TransactionDataResponseModel, FuelError>) {
                val transactionDataResponse = discountBarcodeResponseModelFuelErrorResult.component1()
                val Error = discountBarcodeResponseModelFuelErrorResult.component2()

                dialog.dismiss()
                if (Error === null && !transactionDataResponse?.ResponseMessage!!.isError()) {

                    mainStore.dispatch(AddTransactionDataCalculate(transactionDataResponse.Transaction))
                    startResultTransaction(transactionId)

                } else {
                    var errorMessage = ""

                    Error?.let {
                        errorMessage = it.exception.message.toString()
                    }

                    if( transactionDataResponse?.ResponseMessage != null )
                    {
                        if (transactionDataResponse.ResponseMessage.isError()) {
                            errorMessage = transactionDataResponse.ResponseMessage.Message!!
                        }
                    }

                    longToast(errorMessage)
                }
            }

            override fun onError(e: Throwable) {
                dialog.dismiss()
            }
        })
    }
}