package com.siampiwat.Payment

import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.MenuCart.ResultTransactionActivity
import com.siampiwat.Model.TransactionDataModel
import com.siampiwat.R
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.Survey.SurveyActivity
import com.siampiwat.Survey.SurveyConst
import com.siampiwat.mainStore
import com.siampiwat.userInfo
import kotlinx.android.synthetic.main.activity_payment_credit_card.*
import org.jetbrains.anko.startActivity
import org.rekotlin.StoreSubscriber


class PaymentCreditCardActivity : BaseOnActivity(), StoreSubscriber<CartInfoState> {
    private val REQ_SCB_PAYMENT = 99
    private var transactionDataCalculate: TransactionDataModel? = null
    private var posRefNo: String = getPOSRefNo()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_credit_card)

        bindEvent()
        setupOnClickListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)
    }

    override fun newState(state: CartInfoState) {
        transactionDataCalculate = state.transactionDataCalculate
        txtNetPrice.text = transactionDataCalculate?.SalesNetAmount?.toString2Digit()
    }

    private fun bindEvent() {
        navigatorView?.setListener{
            finish()
        }

        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }.skipRepeats()
        }
    }

    private  fun setupOnClickListener() {
        flSCB?.setOnClickListener {
            this?.let {
//                startAppPaymentSCB()
                startResultTransaction()
            }
        }

        flEDC?.setOnClickListener {
            this?.let {
                val i = Intent(it, PaymentEDCActivity::class.java)
                startActivity(i)
            }
        }
    }

    private fun startResultTransaction() {
        val arLstSurvey = userInfo?.getPostSurvey("M2000")
        val surveyJson = Gson().toJson(arLstSurvey)
        if (arLstSurvey!!.size < 1) {
            var intent = Intent(this, ResultTransactionActivity::class.java)
            startActivity(intent)
        } else {
            val i = Intent(this, SurveyActivity::class.java)
            i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
            i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M2000_PostSurvey)
            startActivity(i)
        }
    }

    private fun startAppPaymentSCB() {
        posRefNo = getPOSRefNo()
        val pm = packageManager
        val inten  =pm.getLaunchIntentForPackage(getString(R.string.package_payment_scb))
        if( inten != null )
        {
            inten.flags = 0
            inten.putExtra("trade_type", "card")
            inten.putExtra("pos_ref_no", posRefNo)
            inten.putExtra("amount", transactionDataCalculate?.SalesNetAmount)
            startActivityForResult(intent, REQ_SCB_PAYMENT)
        }

    }

    private fun getPOSRefNo(): String{
        val tsLong = System.currentTimeMillis() / 1000
        return tsLong.toString()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode === REQ_SCB_PAYMENT){

            if (resultCode ===  RESULT_OK) {
                //TODO: get data from extra
            }
        }
    }
}