package com.siampiwat.Payment

import android.os.Bundle
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.Extention.toString2Digit
import com.siampiwat.R
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.mainStore
import kotlinx.android.synthetic.main.activity_payment_edc.*
import org.rekotlin.StoreSubscriber

class PaymentEDCActivity : BaseOnActivity(), StoreSubscriber<CartInfoState> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_edc)

        bindEvent()
        setupOnClickListener()
    }


    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)
    }

    override fun newState(state: CartInfoState) {
        txtNetPrice.text = state.transactionDataCalculate?.SalesNetAmount.toString2Digit()
    }

    private fun bindEvent() {
        navigatorView?.setListener{
            finish()
        }

        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }.skipRepeats()
        }
    }


    private  fun setupOnClickListener() {
    }
}