package com.siampiwat.MenuChangePassword

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.google.gson.Gson
import com.siampiwat.API.AccountApi
import com.siampiwat.BaseObject.BottomView
import com.siampiwat.BaseObject.ScannerView
import com.siampiwat.Extention.toast

import com.siampiwat.R
import com.siampiwat.State.Action.LogOffSuccessAction
import com.siampiwat.Survey.SurveyActivity
import com.siampiwat.Survey.SurveyConst
import com.siampiwat.mainStore
import com.siampiwat.userInfo
import org.jetbrains.anko.indeterminateProgressDialog

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class ChangePasswordFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var bottomView:BottomView
    private lateinit var activity: Activity
    private lateinit var edtNewPassword: EditText
    private lateinit var edtNewPasswordConfirm: EditText
    private lateinit var edtOldPassword: EditText

    private var mActivity:Activity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

            mActivity = getActivity()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_change_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindView(view)
        bindEvent()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        this.activity = this!!.getActivity()!!
    }

    override fun onDetach() {
        super.onDetach()

    }

    fun bindView(view:View )
    {
        bottomView = view.findViewById(R.id.bottomView)

        edtNewPassword = view.findViewById(R.id.edtNewPassword)
        edtNewPasswordConfirm = view.findViewById(R.id.edtNewPasswordConfirm)
        edtOldPassword = view.findViewById(R.id.edtOldPassword)
    }

    private fun bindEvent()
    {
        bottomView.setListener {
            requestChangePassword()
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ChangePasswordFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    @SuppressLint("CheckResult")
    private fun requestChangePassword(){
        if(edtOldPassword.text.isEmpty() || edtNewPassword.text.isEmpty() ||  edtNewPasswordConfirm.text.isEmpty()) {
            return
        }

        if(edtNewPassword.text == edtNewPasswordConfirm.text) {
            activity?.toast("Confirm Password not match")
            return
        }

        var dialog = activity.indeterminateProgressDialog("")
        dialog.setCancelable(false)
        AccountApi.changePassword(userInfo?.OperatorId.toString(), edtOldPassword.text.toString(), edtNewPasswordConfirm.text.toString() )
                .doOnSubscribe {
                    dialog.show()
                }
                .subscribe{
                    Response ->
                    dialog.dismiss()
                    val (Response, Error) = Response
                    if (Error === null && Response?.Error?.size == 0) {
                        edtOldPassword.text.clear()
                        edtNewPassword.text.clear()
                        edtNewPasswordConfirm.text.clear()
                        activity?.toast("Change Password Success")

                        callPostSurvey()

                    }
                    else{
                        var errorMessage = ""

                        Error?.let {
                            errorMessage = it.message.toString()
                        }
                        Response?.Error?.let {
                            errorMessage = it.first().Message!!
                        }
                        activity?.toast(errorMessage)
                    }
                }


    }

    private fun callPostSurvey()
    {
        val arLstSurvey = userInfo?.getPostSurvey("M7000")
        val surveyJson = Gson().toJson(arLstSurvey)
        if (arLstSurvey!!.size < 1 ) {
            activity.finish()
        }
        else{
            val i = Intent(mActivity, SurveyActivity::class.java)
            i.putExtra(SurveyConst.KEY_SURVEY_JSON, surveyJson)
            i.putExtra(SurveyConst.KEY_SURVEY_ACTION_MENU, SurveyConst.M7000_PreSurvey)
            startActivity(i)
        }
    }
}
