package com.siampiwat.SearchCustomer

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.annotation.Px
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.view.View
import android.view.Window
import android.widget.TextView
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.siampiwat.API.RoyaltyApi
import com.siampiwat.Adapter.TabAdapter
import com.siampiwat.BaseObject.*
import com.siampiwat.Model.LoyaltyCardModel
import com.siampiwat.Model.LoyaltyModelResponseModel
import com.siampiwat.R
import com.siampiwat.State.Action.AddCustomerInCart
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.mainStore
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_search_customer.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.rekotlin.StoreSubscriber


class SearchCustomerActivity : BaseOnActivity() , StoreSubscriber<CartInfoState> {
    override fun newState(state: CartInfoState) {

    }

    private var fmManager: FragmentManager? = null
    private var navigationView: NavigatorView? = null
    private var dlgAlert:Dialog? = null
    private var scannerView: ScannerView? =null
    private var isResume = false

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_customer)

        fmManager = supportFragmentManager

        val adapter = TabAdapter(supportFragmentManager)
        adapter.setActivity(this)
        adapter.setMode( TabAdapter.MODE_SEARCH_CUSTOMER)
        pager.adapter = adapter
        adapter.setListener( object:TabAdapter.OnClickButton{
            override fun onSearchCustomer(keword:String) {
                 requestRoyaltyInfo("0000006918")
//                 requestRoyaltyInfo(keword)
            }

            override fun onScanBarCode() {
                if (scannerView == null) {
                    initialScanner()
                    if (!isResume) {
                        val handle = Handler()
                        handle.postDelayed({
                            scannerView!!.onResume()
                            isResume = true
                        }, 300)
                    }
                }
                flRootScanner.removeAllViews()
                flRootScanner.addView(scannerView)
                hideKeyboard()
            }
        })

        tabLayout.setupWithViewPager(pager)
        pager.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(var1: Int, var2: Float, @Px var3: Int){

            }

            override fun onPageSelected(var1: Int){

                if( var1 == 0 )
                {

                    hideKeyboard()
                }
                else{
                    showKeyboard()
                }
            }

            override fun onPageScrollStateChanged(var1: Int){

            }
        })

        navigationView = findViewById(R.id.navigatorView)
        navigationView!!.setListener {
            finish()
        }

        setupCartState()
    }

    private fun initialScanner()
    {
        scannerView = ScannerView( this@SearchCustomerActivity )
        scannerView!!.setTitle(getString(R.string.common_scan_barcode))
        scannerView!!.setShowFloatingButton(true)
        scannerView!!.setListener(object : ScannerView.ScannerListener {
            override fun onScanComplete(result: String) {
                requestRoyaltyInfo("0000006918" )
//                requestRoyaltyInfo(result )
            }

            override fun onClickClose() {
                removeScannerView()
            }

            override fun onClickKeyboard() {
                removeScannerView()
            }
        })
    }

    private fun removeScannerView()
    {
        if (scannerView != null) {
            scannerView!!.onPause()
            scannerView!!.onDestroy()
            scannerView = null
        }
        isResume = false
        flRootScanner.removeAllViews()
    }

    private fun setupCartState() {
        mainStore.unsubscribe(this)
        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }
        }
    }

    private fun requestRoyaltyInfo(barcode: String) {

        val dialog = indeterminateProgressDialog("")
        dialog.setCancelable(false)

        RoyaltyApi.getRoyaltyInfo(barcode).subscribe(object: SingleObserver<Result<LoyaltyModelResponseModel, FuelError>> {
            override fun onSuccess(loyaltyModelResponseModelResult: Result<LoyaltyModelResponseModel, FuelError>) {
                val royaltyModelResponseModel = loyaltyModelResponseModelResult.component1()
                val error = loyaltyModelResponseModelResult.component2()

                dialog.dismiss()
                if (error == null && royaltyModelResponseModel != null && royaltyModelResponseModel.Error.size == 0) {

                    val loyaltyCardModel = royaltyModelResponseModel.Loyalty
                    if( loyaltyCardModel.size > 0 )
                    {
                        mainStore.dispatch( AddCustomerInCart(loyaltyCardModel[0]))
                        this@SearchCustomerActivity.finish()
                        hideKeyboard()
                    }
                } else {
                    alertItemNotFound(royaltyModelResponseModel!!.ResponseMessage!!.Message!!)
                }
            }

            override fun onSubscribe(d: Disposable) {

            }

            override fun onError(e: Throwable) {

            }

        })
    }

    private fun createFragment(fragment: Fragment , tag:String) {
        replaceFragment(fragment,tag)

        if (fragment is ResultFragment) {
            fragment.setListener(object : ResultFragment.onClick {
                override fun onClickBack() {
                    onBackPressed()

                    showKeyboard()
                }

                override fun onSelectItem(data: String) {
                    setResultData(data)

                    val customerModel = Gson().fromJson(data, LoyaltyCardModel::class.java)
                    mainStore.dispatch(AddCustomerInCart(customerModel))
                }
            })
        }
    }

    private fun alertItemNotFound(message: String?) {
        dlgAlert = Dialog(this, R.style.dialog_theme)
        dlgAlert!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dlgAlert!!.setContentView(R.layout.dlg_search_again)
        dlgAlert!!.setCancelable(true)

        val tvTitle = dlgAlert!!.findViewById<TextView>(R.id.tvTitle)
        tvTitle.text = message

        val tvOk = dlgAlert!!.findViewById<TextView>(R.id.tvOk)
        tvOk.visibility = View.GONE
        tvOk.setOnClickListener {
            dlgAlert!!.cancel()
        }

        val tvClose = dlgAlert!!.findViewById<TextView>(R.id.tvClose)
        tvClose.setOnClickListener {
            dlgAlert!!.cancel()

            showKeyboard()

            val handler = Handler()
            handler.postDelayed({
                if (SearchFragment.edtSearch != null)
                    SearchFragment.edtSearch.requestFocus()
                else
                    BaseOnActivity.Log("edtSearch == null")
            }, 300)
        }
        dlgAlert!!.show()
    }

//    private fun alertItemNotFound(keyword:String) {
//        dlgAlert = Dialog(this, R.style.dialog_theme)
//        dlgAlert!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dlgAlert!!.setContentView(R.layout.dlg_search_again)
//        dlgAlert!!.setCancelable(true)
//        val tvOk = dlgAlert!!.findViewById(R.id.tvOk) as TextView
//        tvOk.setOnClickListener {
//            dlgAlert!!.cancel()
//            createFragment(ResultFragment.newInstance(MODE_SEARCH_CUSTOMER, "", keyword, ""), "ResultFragment")
//        }
//
//        val tvClose = dlgAlert!!.findViewById(R.id.tvClose) as TextView
//        tvClose.setOnClickListener {
//            dlgAlert!!.cancel()
//
//            showKeyboard()
//
//            val handler = Handler()
//            handler.postDelayed({
//                if (SearchFragment.edtSearch != null)
//                    SearchFragment.edtSearch.requestFocus()
//                else
//                    BaseOnActivity.Log("edtSearch == null")
//            }, 300)
//        }
//        dlgAlert!!.show()
//    }

    private fun replaceFragment(fragment: Fragment? , tag:String) {
        if (fragment == null) {
            moveTaskToBack(true)
            System.exit(1)

            return
        }

        val transaction = fmManager!!.beginTransaction()
        transaction.replace(R.id.flRoot, fragment, tag)
        transaction.addToBackStack("")
        transaction.commit()
    }

    private fun setResultData(data: String) {

        hideKeyboard()

        val resultIntent = Intent()
        resultIntent.putExtra("data", data)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)

        if (scannerView != null) {
            scannerView!!.onPause()
            scannerView!!.onDestroy()
            scannerView = null
        }

        hideKeyboard()
    }

    override fun onPause() {
        super.onPause()

        if (scannerView != null)
            scannerView!!.onPause()
    }

    override fun onResume() {
        super.onResume()

        if (scannerView != null)
            scannerView!!.onResume()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
                setResultData(result)
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult
}
