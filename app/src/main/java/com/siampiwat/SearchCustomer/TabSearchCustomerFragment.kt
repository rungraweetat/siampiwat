package com.siampiwat.SearchCustomer

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import com.siampiwat.R
import android.text.TextUtils
import android.util.Log
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.siampiwat.API.RoyaltyApi
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.Model.LoyaltyModelResponseModel
import com.siampiwat.State.Action.AddCustomerInCart
import com.siampiwat.State.Action.SetSellerInProduct
import com.siampiwat.mainStore
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_search_customer.*
import org.jetbrains.anko.indeterminateProgressDialog


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class TabSearchCustomerFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var listener: OnClickButton? = null
    private var dlgAlert: Dialog? = null
    private var edtSearch: EditText? = null
    private var mActivity: Activity? = null

    interface OnClickButton {
        fun onClickScanBarcode()
        fun onSearchCustomer(keyword: String)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_customer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindView(view)
        bindEvent()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        this.mActivity = activity
    }

    fun setListenerOnClickButton(listener: OnClickButton) {
        this.listener = listener
    }

    private fun bindView(view: View) {
        edtSearch = view.findViewById(R.id.edtSearch)
    }

    private fun bindEvent() {

        edtSearch!!.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch()
                return@OnEditorActionListener true
            }
            false
        })

        fabScan!!.setOnClickListener {

            if (listener != null) {
                listener!!.onClickScanBarcode()
            } else {
                if (mActivity != null) {
                    BaseOnActivity.Log("please set listener first!!!")

                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        edtSearch!!.requestFocus();
//        edtSearch!!.showKeyboard()
    }

    private fun performSearch() {

        if (TextUtils.isEmpty(edtSearch!!.text.toString())) {
            BaseOnActivity.getInstance().hideKeyboard()
            return
        }

        if( listener != null )
            listener!!.onSearchCustomer(edtSearch!!.text.toString())

        //call api
//        dlgAlert = Dialog(mActivity, R.style.dialog_theme)
//        dlgAlert!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dlgAlert!!.setContentView(R.layout.dlg_alert)
//        dlgAlert!!.setCancelable(true)
//
//        val tvOk = dlgAlert!!.findViewById(R.id.tvOk) as TextView
//        tvOk.setOnClickListener(View.OnClickListener {
//
//            if( listener != null )
//            {
//                listener!!.onSearchNotFound(edtSearch!!.text.toString())
//            }
//            dlgAlert!!.cancel()
//        })
//        dlgAlert!!.show()
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                TabSearchCustomerFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
