package com.siampiwat.survey


import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.text.InputFilter
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.CheckBox
import com.google.gson.Gson
import com.siampiwat.Model.SurveyInputModel
import com.siampiwat.Model.SurveyModel
import com.siampiwat.R
import com.siampiwat.Survey.SurveyActivity
import com.siampiwat.Survey.SurveyConst
import com.siampiwat.Survey.SurveyConst.KEY_SURVEY_ARGS_JSON
import com.siampiwat.Survey.SurveyConst.ViewType.*
import kotlinx.android.synthetic.main.fragment_dynamic.*

class SurveyDynamicFragment : Fragment() {

    private var listViewData = ArrayList<View>()
    private var radioGroup: RadioGroup? = null
    private var selectedListView = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dynamic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            val json = arguments?.getString(SurveyConst.KEY_SURVEY_ARGS_JSON)
            val preSurvey = Gson().fromJson<SurveyModel>(json, SurveyModel::class.java)
            preSurvey.SurveyGroup?.forEach { group ->

                //TODO: Add check language
                getSurveyActivity().supportActionBar?.title = group.TitleTH
                group.SurveyInputs.forEach {
                    // Add view
                    val type = SurveyConst.ViewType.valueOf(it.ControlType.toInt())
                    inflateView(it, type)
                }
            }
        } catch(ex: Exception) {
             // Crash cannot parse json...
        }

        btnOKSurvey.setOnClickListener {
            val listData = ArrayList<String>()
            listViewData.forEach { v ->
                if(v is EditText) {
                    val text = v.text.toString()
                    listData.add(text)
                }
                if(v is CheckBox) {
                    val checked = if(v.isChecked) "1" else "0"
                    listData.add(checked)
                }
                if(v is RadioButton) {
                    val checked = if(v.isChecked) "1" else "0"
                    listData.add(checked)
                }
                if(v is Spinner) {
                    try {
                        val value = v.selectedItem.toString()
                        listData.add(value)
                    } catch (e: Exception){}
                }
                if(v is ListView) {
                    listData.add(selectedListView)
                }
            }
            getSurveyActivity().onDynamicFragmentNext(listData)
        }
        btnCancelSurvey.setOnClickListener {
            getSurveyActivity().finish()
        }

    }

    private fun inflateView(surveyInputModel: SurveyInputModel, viewType: SurveyConst.ViewType) {
        val root = view!!.findViewById<LinearLayout>(R.id.llRootSurvey)
        val inflater = LayoutInflater.from(activity)
        val view = getInflateView(root,inflater,viewType) ?: return

        setupViewInflater(view,surveyInputModel,viewType)

        if(viewType == RadioBox && radioGroup == null) {
            val radioview = inflater.inflate(R.layout.item_survey_radio_group, root, false)
            root.addView(radioview)
            radioGroup = root.findViewById(R.id.radioGroupItemSurvey)
        }

        if(viewType == RadioBox && radioGroup != null) {
//            view?.id = View.generateViewId()
            radioGroup!!.addView(view)
        } else {
            root.addView(view)
        }

    }

    private fun getInputType(type: SurveyConst.ViewType) : Int {
        return when(type) {
            InputText -> InputType.TYPE_CLASS_TEXT
            InputNumeric -> InputType.TYPE_CLASS_NUMBER
            InputDate -> InputType.TYPE_DATETIME_VARIATION_DATE
            InputDateTime -> InputType.TYPE_DATETIME_VARIATION_TIME
            InputPassword -> InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            else -> InputType.TYPE_NULL
        }
    }

    private fun getInflateView(root: LinearLayout, inflater: LayoutInflater, type: SurveyConst.ViewType) = when(type) {
        Hidden -> null
        InputText -> inflater.inflate(R.layout.item_survey_edit_text, root, false)
        InputNumeric -> inflater.inflate(R.layout.item_survey_edit_text, root, false)
        InputDate -> inflater.inflate(R.layout.item_survey_edit_text, root, false)
        InputDateTime -> inflater.inflate(R.layout.item_survey_edit_text, root, false)
        InputPassword -> inflater.inflate(R.layout.item_survey_edit_text, root, false)
        SurveyConst.ViewType.CheckBox -> inflater.inflate(R.layout.item_survey_check_box, root, false)
        RadioBox -> inflater.inflate(R.layout.item_survey_radio_button, root, false)
        ComboBox -> inflater.inflate(R.layout.item_survey_spinner, root, false)
        SubCode -> inflater.inflate(R.layout.item_survey_listview, root, false)
    }

    private fun setupViewInflater(view: View?, surveyInputModel: SurveyInputModel, type: SurveyConst.ViewType) {
        if(view == null) return
        when(type){
            Hidden -> return
            InputText,
            InputNumeric,
            InputDate,
            InputDateTime,
            InputPassword -> {

                val edt = view.findViewById<EditText>(R.id.edtItemSurvey)
                edt!!.inputType = getInputType(type)
                if(surveyInputModel.MaxLength != -1) {
                    edt!!.filters = arrayOf(InputFilter.LengthFilter(surveyInputModel.MaxLength))
                }
                val til = view.findViewById<TextInputLayout>(R.id.tilItemSurvey)
                //TODO: Add check language
                til!!.hint = surveyInputModel.LabelTH
                listViewData.add(edt)
            }
            SurveyConst.ViewType.CheckBox -> {
                val checkBox = view.findViewById<CheckBox>(R.id.cbItemSurvey)
                //TODO: Add check language
                checkBox!!.text = surveyInputModel.LabelTH
                listViewData.add(checkBox)
            }
            RadioBox -> {
                val radioButton = view.findViewById<RadioButton>(R.id.rbItemSurvey)
                //TODO: Add check language
                radioButton!!.text = surveyInputModel.LabelTH
                listViewData.add(radioButton)
            }
            ComboBox -> {
                val spinner = view.findViewById<Spinner>(R.id.spinnerItemSurvey)
                spinner.tag = R.id.spinnerItemSurvey
                ArrayAdapter(
                        activity,
                        android.R.layout.simple_spinner_item,
                        surveyInputModel.DataListItem.map { it.Value }
                ).also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinner.adapter = adapter
                }
                listViewData.add(spinner)
            }
            SubCode -> {
                val listview = view.findViewById<ListView>(R.id.listviewItemSurvey)
                val items = surveyInputModel.DataListItem.map { it.Value }
                ArrayAdapter(
                        activity,
                        android.R.layout.simple_list_item_1,
                        items
                ).also { adapter ->
                    listview.adapter = adapter
                    listview.setOnItemClickListener { parent, view, position, id ->
                        selectedListView = items[position]
                    }
                }
            }
        }
    }

    private fun getSurveyActivity() = (activity as SurveyActivity)
    companion object {
        fun newInstance(json: String) =
            SurveyDynamicFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_SURVEY_ARGS_JSON, json)
                }
            }

    }

}
