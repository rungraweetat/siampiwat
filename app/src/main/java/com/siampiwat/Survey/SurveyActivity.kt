package com.siampiwat.Survey

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.TextUtils
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.siampiwat.API.AccountApi
import com.siampiwat.API.RoyaltyApi
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.EventBus.*
import com.siampiwat.Extention.indeterminateProgress
import com.siampiwat.Model.SurveyInputModel
import com.siampiwat.Model.SurveyModel
import com.siampiwat.Model.SurveyResultModel
import com.siampiwat.R
import com.siampiwat.survey.SurveyDynamicFragment
import com.siampiwat.userInfo
import kotlinx.android.synthetic.main.activity_survey.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.longToast

/**
 * วิธีเรียกหน้า Survey
 *
 * val intent = Intent(this@MainActivity, SurveyActivity::class.java)
 * intent.putExtra(KEY_SURVEY_TYPE,1) // flag แสดงหน้ากลาง, 0 = discount, 1 = change price
 * intent.putExtra(KEY_SURVEY_JSON,json) // json ที่เป็น object ของ RequestManualTotalDiscount
 * startActivityForResult(intent, REQUEST_CODE)
 *
 *
 * override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
 *     if(requestCode == SurveyConst.REQUEST_CODE &&
 *             resultCode == Activity.RESULT_OK) {
 *         val list = data?.getStringArrayListExtra(SurveyConst.KEY_SURVEY_JSON)
 *         Log.w("survey","Request code => $requestCode")
 *         Log.w("survey","Result code => $resultCode")
 *         Log.w("survey","List => $list")
 * }
 * */
class SurveyActivity : BaseOnActivity() {

    private var fragmentLayout: Fragment? = null
    private var surveyList = ArrayList<SurveyModel>()
    private var resultList = ArrayList<String>()
    private var currentStep = 0
    private var surveyType = 0
    private var jsonData = ""
    private var actionMenu = 0
    private var itemId = ""
    override fun onBackPressed() {
        super.onBackPressed()

        hideKeyboard()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey)
        setSupportActionBar(surveyToolbar)
        surveyToolbar.setTitleTextColor(resources.getColor(android.R.color.white))
        try {
            //surveyType = intent.getIntExtra(SurveyConst.KEY_SURVEY_TYPE,0)
            jsonData = intent.getStringExtra(SurveyConst.KEY_SURVEY_JSON)
            actionMenu = intent.getIntExtra(SurveyConst.KEY_SURVEY_ACTION_MENU , 0)

            if( actionMenu ==  SurveyConst.M2102_PreSurvey || actionMenu == SurveyConst.M2103_PreSurvey || actionMenu == SurveyConst.M2101_PreSurvey)
            {
                if(intent.hasExtra("ItemId"))
                {
                    itemId = intent.getStringExtra("ItemId" )
                }
                else{
                    alertDialog()
                }
            }
            else if( actionMenu == SurveyConst.M2102ManageDiscountPresurvey )
            {
                if(intent.hasExtra("ItemId"))
                {
                    itemId = intent.getStringExtra("ItemId")
                }
            }

        } catch(ex: Exception) {
            alertDialog()
            return
        }

        getData()
    }

    private fun alertDialog()
    {
        AlertDialog.Builder(this)
                .setMessage("Don't have data")
                .setPositiveButton("OK") { dialog, which ->
                    hideKeyboard()
                    finish()
                }
                .setCancelable(false)
                .show()
    }

    private fun attachFragment(fragment: Fragment) {
        fragmentLayout = fragment
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.FrameFragmentLayout, fragmentLayout!!)
            //addToBackStack("")
            commit()
        }
        System.gc()
    }

    private fun getData() {
        surveyList = ArrayList()
        try {
            val typeToken = object: TypeToken<List<SurveyModel>>() {}.type
            val list = Gson().fromJson<List<SurveyModel>>(jsonData, typeToken)
            surveyList.addAll(list)

            if (surveyList.isNotEmpty()) {
                val json = Gson().toJson(surveyList[currentStep])
                attachFragment(SurveyDynamicFragment.newInstance(json))
            }
        } catch(ex: Exception) {
            AlertDialog.Builder(this)
                    .setMessage("Don't have data")
                    .setPositiveButton("OK") { dialog, which ->
                        hideKeyboard()
                        finish()
                    }
                    .setCancelable(false)
                    .show()
        }
    }

    fun onDynamicFragmentNext(list: List<String>) {
        resultList.addAll(list)
        checkResult()
    }

    private fun checkResult() {
        if( actionMenu == SurveyConst.M2102_PreSurvey )
            callChackManagerPrivilege()
        else if( actionMenu == SurveyConst.M2103_PreSurvey )
            callChackManagerPrivilege()
        else if( actionMenu == SurveyConst.M2800_PreSurvey ) {
//            callCheckCustomerIDCard()
            EventBus.getDefault().post(M2800PresurveyModel(null))
            hideKeyboard()
            finish()
        } else {
            onCallbackFromApi()
        }
    }

    private fun onCallbackFromApi() {
        currentStep += 1
        if(currentStep < surveyList.size) {
            val json = Gson().toJson(surveyList[currentStep])
            attachFragment(SurveyDynamicFragment.newInstance(json))
        } else {
            returnResult()
        }
    }

    private fun onCallbackFailed() {
        // TODO: Show error
    }

    private fun returnResult() {

        if( actionMenu == SurveyConst.M2102_PreSurvey )
            EventBus.getDefault().post(M2102PresurveyModel(itemId))
        else if( actionMenu == SurveyConst.M2103_PreSurvey )
            EventBus.getDefault().post(M2103PresurveyModel(itemId))
        else if( actionMenu == SurveyConst.M2102ManageDiscountPresurvey )
            EventBus.getDefault().post(M2102ManageDiscountPresurveyModel( itemId ))
        else if( actionMenu == SurveyConst.ReemtionDiscountPresurvey )
            EventBus.getDefault().post(RedemtionDiscountPresurvey())
        else if( actionMenu == SurveyConst.M2000_PostSurvey )
            EventBus.getDefault().post(M2000PostSurveyModel())
        else if( actionMenu == SurveyConst.M2200_PreSurvey )
            EventBus.getDefault().post(M2200PreSurveyModel())
        else if( actionMenu == SurveyConst.M2400_PreSurvey )
            EventBus.getDefault().post(M2400PreSurveyModel())
        else if( actionMenu == SurveyConst.M2500_PreSurvey )
            EventBus.getDefault().post(M2500PreSurveyModel())
        else if( actionMenu == SurveyConst.M2500_PostSurvey )
            EventBus.getDefault().post(M2500PostSurveyModel())
        else if( actionMenu == SurveyConst.M4000_PreSurvey )
            EventBus.getDefault().post(M4000PreSurveyModel())
        else if( actionMenu == SurveyConst.M6000_PreSurvey )
            EventBus.getDefault().post(M6000PreSurveyModel())
        else if( actionMenu == SurveyConst.M6000_PostSurvey )
            EventBus.getDefault().post(M6000PostSurveyModel())
        else if( actionMenu == SurveyConst.M7000_PreSurvey )
            EventBus.getDefault().post(M7000PreSurveyModel())
        else if( actionMenu == SurveyConst.M7000_PostSurvey )
            EventBus.getDefault().post(M7000PostSurveyModel())
        else if( actionMenu == SurveyConst.CouponDiscount_PreSurvey )
            EventBus.getDefault().post(PreSurveyCouponDiscountsModel())
        else if( actionMenu == SurveyConst.M2201_PostSurvey ){
            //finish
        }
        else if( actionMenu == SurveyConst.M2101_PreSurvey )
        {
            if(!TextUtils.isEmpty(itemId))
               EventBus.getDefault().post(M2101PreSurveyModel(itemId.toInt()))
        }
        else if( actionMenu == SurveyConst.M2800_PreSurvey ) {
            //callGetRedeemablePointBalance(resultList[0])
            callCheckCustomerIDCard()
            return
        }
//        else if( actionMenu == SurveyConst.BarcodeDiscount_PreSurvey )
//        {
//            EventBus.getDefault().post(BarcodeDiscountsPreSurveyModel())
//        }

        finish()
    }

    private fun callActionUri() {
        if(surveyList.isEmpty() || resultList.size < 2) {
            //TODO: Show error
            return
        }
        val surveyModel = surveyList[currentStep]
        val dialog = this.indeterminateProgress("")
        val inputKeys = surveyModel.SurveyGroup[currentStep].SurveyInputs.map { it.InputKey }
        val surveyResultModel = SurveyResultModel.build2(surveyModel.Key, inputKeys, resultList)
        val json = Gson().toJson(surveyResultModel)

        dialog?.show()
        Fuel.post(surveyModel.ActionUri)
                .header("Content-Type" to "application/json")
                .body(json)
                .responseObject<SurveyInputModel> {request, response, result ->
                    dialog?.dismiss()
                    //
                    val (obj, error) = result
                    if (error == null) {
                        onCallbackFromApi()
                    } else {
                        onCallbackFailed()
                    }
                }
    }

    @SuppressLint("CheckResult")
    private fun callChackManagerPrivilege() {
        if(surveyList.isEmpty() || resultList.size < 2) {
            //TODO: Show error
            return
        }
        val surveyModel = surveyList[currentStep]
        val operatorId = resultList[0]
//        val pass = "60053634"
        val pass = resultList[1]
        val dialog = this.indeterminateProgress("")
        val surveyResult = SurveyResultModel.build(surveyModel.Key, operatorId, pass)

        AccountApi
                .checkManagerPrivilege(surveyResult)
                .doOnSubscribe {
                    dialog?.show()
                }
                .doFinally {
                    dialog?.dismiss()
                }
                .subscribe{
                    Response ->
                    val (UserResponse, Error) = Response
                    if( Error === null && !UserResponse?.ResponseMessage?.isError()!!){
                        UserResponse.OperatorId = operatorId
//                        applicationContext?.let {
//                            mainStore.dispatch(LogonSuccessAction(it, UserResponse))
//                        }
                        onCallbackFromApi()
                    } else{
                        var errorMessage = ""
                        Error?.let {
                            errorMessage = it.message.toString()
                        }
                        UserResponse?.let {
                            if(it?.ResponseMessage?.isError()!!) {
                                errorMessage = UserResponse?.ResponseMessage?.Message!!
                            }
                        }
                        longToast(errorMessage)
                        onCallbackFailed()
                    }
                }


        //onCallbackFromApi()
    }


    @SuppressLint("CheckResult")
    private fun callCheckCustomerIDCard() {
        if(surveyList.isEmpty()) {
            EventBus.getDefault().post(M2800PresurveyModel(null))

            hideKeyboard()
            finish()
            return
        }
        val dialog = this.indeterminateProgress("")
        // call api
        RoyaltyApi.checkCustomerIDCard(surveyList[0].Key)
                .doOnSubscribe {
                    dialog?.show()
                }
                .doFinally {
                    dialog?.dismiss()
                    finish()
                }
                .subscribe{
                    Response ->
                    val (loyaltyResponse, Error) = Response
                    if( Error === null && !loyaltyResponse?.ResponseMessage?.isError()!!){
                        loyaltyResponse.Loyalty.filter{it.CustomerName == userInfo?.OperatorName}?.first()?.also {
//                            callGetRedeemablePointBalance(it.CardNumber)

                            EventBus.getDefault().post(M2800PresurveyModel(null))
                            hideKeyboard()
                            finish()
                        }
                    } else {
                        var errorMessage = ""

                        Error?.let {
                            errorMessage = it.message.toString()
                        }
                        if(loyaltyResponse?.ResponseMessage?.isError()!!) {
                            errorMessage = loyaltyResponse?.ResponseMessage?.Message!!
                        }

                        longToast(errorMessage)
                        onCallbackFailed()
                    }

                }
        //dispos.dispose()
    }

    @SuppressLint("CheckResult")
    private fun callGetRedeemablePointBalance(cardNumber: String?) {
        if(cardNumber.isNullOrEmpty()) {
            EventBus.getDefault().post(M2800PresurveyModel(null))
            hideKeyboard()
            finish()
            return
        }
        val dialog = this.indeterminateProgress("")
        RoyaltyApi.getRedeemablePointBalance(cardNumber!!)
                .doOnSubscribe {
                    dialog?.show()
                }
                .doFinally {
                    dialog?.dismiss()
                    hideKeyboard()
                    finish()
                }
                .subscribe{
                    Response ->
                    val (loyaltyResponse, Error) = Response

                    if( Error === null && !loyaltyResponse?.ResponseMessage?.isError()!!){
                        val cardModelList = loyaltyResponse.Loyalty
                        EventBus.getDefault().post(M2800PresurveyModel(cardModelList))
                        onCallbackFromApi()
                    } else {
                        var errorMessage = ""

                        Error?.let {
                            errorMessage = it.message.toString()
                        }
                        if(loyaltyResponse?.ResponseMessage?.isError()!!) {
                            errorMessage = loyaltyResponse?.ResponseMessage?.Message!!
                        }

                        longToast(errorMessage)
                        onCallbackFailed()
                    }

                }
    }
}
