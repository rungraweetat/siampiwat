package com.siampiwat.Survey

object SurveyConst {
    const val KEY_SURVEY_TYPE = "survey_extra_type"
    const val KEY_SURVEY_JSON = "survey_extra_json"
    const val KEY_SURVEY_ARGS_JSON = "survey_args_json"
    const val KEY_SURVEY_TOTAL_DISCOUNT = "key_manual_total_discounts"

    const val KEY_SURVEY_ACTION_MENU = "survey_action_menu"
    const val REQUEST_CODE = 777

    const val M2000_PreSurvey = 99
    const val M2000_PostSurvey = 98

    const val M2102_PreSurvey = 97
    const val M2102_PostSurvey = 96

    const val M2103_PreSurvey = 95
    const val M2103_PostSurvey = 94

    const val M2800_PreSurvey = 93
    const val M2800_PostSurvey = 92

    const val M2102ManageDiscountPresurvey = 91
    const val ReemtionDiscountPresurvey = 90

    const val M2100_AddProductPreSurvey = 89
    const val M2100_AddProductPostSurvey = 88

    const val M2200_PreSurvey = 87
    const val M2200_PostSurvey = 86

    const val M2101_PreSurvey = 85
    const val M2101_PostSurvey = 84

    const val M2201_PreSurvey = 83
    const val M2201_PostSurvey = 82

    const val M2400_PreSurvey = 81
    const val M2400_PostSurvey = 80

    const val M2500_PreSurvey = 79
    const val M2500_PostSurvey = 78

    const val CouponDiscount_PreSurvey = 77
    const val CouponDiscount_PostSurvey = 76

    const val BarcodeDiscount_PreSurvey = 75
    const val BarcodeDiscount_PostSurvey = 74

    const val M4000_PreSurvey = 73
    const val M4000_PostSurvey = 72

    const val M7000_PreSurvey = 71
    const val M7000_PostSurvey = 70

    const val M6000_PreSurvey = 69
    const val M6000_PostSurvey = 68

    const val M8000_PreSurvey = 67
    const val M8000_PostSurvey = 66

    enum class ViewType {
        Hidden,
        InputText,
        InputNumeric,
        InputDate,
        InputDateTime,
        InputPassword,
        CheckBox,
        RadioBox,
        ComboBox,
        SubCode;
        companion object {
            fun valueOf(input: Int): ViewType = values().find { it.ordinal == input } ?: Hidden
        }
    }
}