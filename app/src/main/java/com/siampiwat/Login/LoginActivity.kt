package com.siampiwat.Login

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.siampiwat.API.AccountApi
import com.siampiwat.BuildConfig
import com.siampiwat.Extention.indeterminateProgress
import com.siampiwat.MainMenu.MainMenuActivity
import com.siampiwat.R
import com.siampiwat.State.Action.LogonSuccessAction
import com.siampiwat.State.Service.PreferenceApiService
import com.siampiwat.State.State.AuthenticationState
import com.siampiwat.State.State.LoggedInState
import com.siampiwat.mainStore
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.longToast
import org.rekotlin.StoreSubscriber

class LoginActivity : AppCompatActivity() ,  StoreSubscriber<AuthenticationState> {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (BuildConfig.DEBUG) {

            edtUserName.setText("60050000")
            edtPassword.setText("1234")
        }


        btnLogin.setOnClickListener{
            requestLogOn()
        }

        // Subscript and select State
        mainStore.subscribe(this){
            it.select {
                it.authenticationState
            }.skipRepeats()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun newState(state: AuthenticationState) {
        if( state.loggedInState === LoggedInState.loggedIn){
            println("=========== LOGIN SUCCESS ===========")
            mainStore.unsubscribe(this)
            loginSuccess()
        }
    }

    @SuppressLint("CheckResult")
    private fun requestLogOn() {


        var dialog = this.indeterminateProgress("")

        AccountApi
                .login(edtUserName.text.toString(), edtPassword.text.toString())
                .doOnSubscribe {
                    dialog?.show()
                }
                .doFinally {
                    dialog?.dismiss()
                }
                .subscribe{
                    Response ->
                    val (UserResponse, Error) = Response

                    if( Error === null && !UserResponse?.ResponseMessage?.isError()!!){
                        UserResponse.OperatorId = edtUserName.text.toString()
                        applicationContext?.let {
                            mainStore.dispatch(LogonSuccessAction(it, UserResponse))
                        }
                    }
                    else{
                        var errorMessage = ""

                        Error?.let {
                            errorMessage = it.message.toString()
                        }
                        UserResponse?.let {
                            if(it?.ResponseMessage?.isError()!!) {
                                errorMessage = UserResponse?.ResponseMessage?.Message!!
                            }
                        }

                        longToast(errorMessage)
                    }
                }
    }

    private  fun  setupFuelManager(){
        PreferenceApiService.setupFuelBaseParameter(applicationContext)
        PreferenceApiService.setupFuelManager()
    }

    private fun loginSuccess() {
        setupFuelManager()

//        val intent = Intent(this, PaymentMethodActivity::class.java)
//        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//        startActivity(intent)

        val intent = Intent(this, MainMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }
}
