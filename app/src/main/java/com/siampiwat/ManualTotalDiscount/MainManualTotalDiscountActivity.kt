package com.siampiwat.ManualTotalDiscount

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.google.gson.Gson
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.MenuSearchProduct.SearchProductFragment
import com.siampiwat.Model.ManualTotalDiscount
import com.siampiwat.Model.RequestManualTotalDiscount
import com.siampiwat.R
import com.siampiwat.State.Action.AddManualTotalDiscount
import com.siampiwat.State.State.CartInfoState
import com.siampiwat.mainStore
import com.siampiwat.userInfo
import kotlinx.android.synthetic.main.activity_main_menu.*
import org.rekotlin.StoreSubscriber


class MainManualTotalDiscountActivity : BaseOnActivity() , StoreSubscriber<CartInfoState> {
    override fun newState(state: CartInfoState) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_manual_total_discount)

//        var fragment = MenuManualTotalDiscountFragment.newInstance("", "")
//        addFragmentToActivity( supportFragmentManager ,fragment, R.id.flRoot, "" )
//
//        fragment.setListenerClickMenu(object : MenuManualTotalDiscountFragment.onClickMenuDiscount {
//
//            override fun onClickMenu(count: Int) {
//
//            }
//        } )

        var manualTotalDiscount: ManualTotalDiscount? = null
        var bundle = intent.extras
        if( bundle != null )
        {
           var data = bundle.getString("data")
            manualTotalDiscount = Gson().fromJson( data , ManualTotalDiscount::class.java )
        }

        showKeyboard()

        if( manualTotalDiscount!!.DiscountVal > 0 )
        {
            hideKeyboard()
//            mainStore.dispatch(AddManualTotalDiscount(manualTotalDiscount))
            finish()
        }

        val data = Gson().toJson(manualTotalDiscount)
        val fragment = ManualTotalDiscountFragment.newInstance(data)
        addFragmentToActivity( supportFragmentManager ,fragment, R.id.flRoot, "" )
        fragment.setListenerClickMenu( object: ManualTotalDiscountFragment.onClickButton {
            override fun onClose(){
                hideKeyboard()
                finish()
            }

            override fun onFinish( manualTotalDiscount: RequestManualTotalDiscount){
                hideKeyboard()
                finish()
                mainStore.dispatch(AddManualTotalDiscount(manualTotalDiscount))
            }
        } )

        setupCartState()
    }

    private fun addFragmentToActivity(manager: FragmentManager, fragment: Fragment, fragmentId: Int, tag: String) {

        val transaction = manager.beginTransaction()
        transaction.replace(fragmentId, fragment, tag)
        transaction.addToBackStack(fragment::javaClass.name)
        transaction.commitAllowingStateLoss()
    }

    override fun onDestroy() {
        super.onDestroy()
        mainStore.unsubscribe(this)
    }

    private fun setupCartState() {
        mainStore.unsubscribe(this)
        mainStore.subscribe(this) {
            it.select {
                it.cartInfoState
            }
        }

    }

    private fun getActiveFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.flRoot)
    }


    override fun onBackPressed() {

        val fragment = getActiveFragment()
        if (fragment != null) {

            if( fragment is ManualTotalDiscountFragment)
            {
                finish()
            }
            return
        }

        super.onBackPressed()
    }
}
