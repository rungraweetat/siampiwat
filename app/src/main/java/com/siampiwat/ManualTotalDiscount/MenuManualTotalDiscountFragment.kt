package com.siampiwat.ManualTotalDiscount

import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import com.siampiwat.R
import com.siampiwat.userInfo
import kotlinx.android.synthetic.main.layout_button_ok.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MenuManualTotalDiscountFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MenuManualTotalDiscountFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MenuManualTotalDiscountFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var llContainer:LinearLayout? = null
    private var listenerOnClickMenu: onClickMenuDiscount? = null

    public interface onClickMenuDiscount
    {
        public fun onClickMenu( count:Int )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu_manual_total_discount, container, false)
    }

    companion object {

        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                MenuManualTotalDiscountFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindView(view)
    }

    public fun setListenerClickMenu( listener: onClickMenuDiscount)
    {
        listenerOnClickMenu = listener
    }

    private fun bindView( view:View )
    {
        llContainer = view.findViewById(R.id.llContainer)


        val arLstSurvey = userInfo?.InitScreenData!!.ManualTotalDiscounts
        for( i in arLstSurvey.indices )
        {
            var btnDiscount = Button(activity)
            btnDiscount.getBackground().setColorFilter(ContextCompat.getColor(this!!.activity!!, R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
            btnDiscount.setTextColor(resources.getColor(R.color.white))

            var layoutParam = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
            btnDiscount.layoutParams = ViewGroup.LayoutParams(layoutParam)
            btnDiscount.setPadding(20,0,20,0)
            btnDiscount.text = arLstSurvey[i].LabelTH
            btnDiscount.setOnClickListener {
                listenerOnClickMenu!!.onClickMenu(i)

            }

            llContainer!!.addView(btnDiscount)
        }
    }
}
