package com.siampiwat.ManualTotalDiscount

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.Gson
import com.siampiwat.BaseObject.BottomView
import com.siampiwat.BaseObject.NavigatorView
import com.siampiwat.Model.ManualTotalDiscount
import com.siampiwat.Model.RequestManualTotalDiscount
import com.siampiwat.R


private const val ARG_DATA = "data"

class ManualTotalDiscountFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var argData: String? = null
    private var param2: String? = null
    private var llContainerScrollView: LinearLayout? = null
    private var listenerOnClickMenu: onClickButton? = null
    private var navigatorView: NavigatorView? = null
    private var btnNext: BottomView? = null

    public interface onClickButton {
        public fun onFinish( manualTotalDiscount:RequestManualTotalDiscount  )
        public fun onClose()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            argData = it.getString(ARG_DATA)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manual_total_discount, container, false)
    }

    companion object {

        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String) =
                ManualTotalDiscountFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_DATA, param1)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        llContainerScrollView = view.findViewById(R.id.llContainerScrollView) as LinearLayout
        navigatorView = view.findViewById(R.id.navigatorView) as NavigatorView
        btnNext = view.findViewById(R.id.btnNext) as BottomView

        bindView(view)
    }

    public fun setListenerClickMenu(listener: onClickButton) {
        listenerOnClickMenu = listener
    }

    private fun bindView(view: View) {

        var manualTotalDiscountModel = Gson().fromJson(argData, ManualTotalDiscount::class.java)
        navigatorView!!.navigatorTitle = manualTotalDiscountModel.LabelTH
        navigatorView!!.setListener {
            if (listenerOnClickMenu != null) {
                listenerOnClickMenu!!.onClose()
            }
        }

        var vLayout = layoutInflater.inflate(R.layout.layout_manual_total_discount, null)

        var edtValue = vLayout.findViewById(R.id.edtValue) as EditText
        var tvPrefix = vLayout.findViewById(R.id.tvPrefix) as TextView

        if (manualTotalDiscountModel.DiscountAttribute == 1) {
            tvPrefix.text = getString(R.string.manual_total_discount_activity_bath)

        } else {
            tvPrefix.text = getString(R.string.manual_total_discount_activity_percent)

            val maxLength = 2
            val filterArray = arrayOfNulls<InputFilter>(1)
            filterArray[0] = InputFilter.LengthFilter(maxLength)
            edtValue.filters = filterArray
        }

        if (manualTotalDiscountModel.DiscountVal <= 0) {
            edtValue.isEnabled = true
        } else {
            edtValue.isEnabled = false
            edtValue.setText(manualTotalDiscountModel.DiscountAttribute.toString())
        }

        edtValue.setText("")
        llContainerScrollView!!.addView(vLayout)

        btnNext!!.setListener {
            if (listenerOnClickMenu != null) {
                val manualTotalDiscountModel = Gson().fromJson(argData, ManualTotalDiscount::class.java)
                val request = RequestManualTotalDiscount()
                request.DiscountAmount = manualTotalDiscountModel.DiscountAmount
                request.DiscountAttribute = manualTotalDiscountModel.DiscountAttribute
                request.DiscountOfferID = manualTotalDiscountModel.DiscountOfferId
                request.DiscountValue = edtValue.text.toString().toDouble()

                listenerOnClickMenu!!.onFinish( request )
            }
        }
    }
}
