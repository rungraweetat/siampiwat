package com.siampiwat.API


import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.AffiliationServiceRoute
import com.siampiwat.Model.AffiliationsResponseModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object AffiliationsApi {

    fun getAffiliations(): Single<Result<AffiliationsResponseModel, FuelError>> {

        return FuelManager.instance.request(AffiliationServiceRoute.GetAffiliationList())
                .rx_object(AffiliationsResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
