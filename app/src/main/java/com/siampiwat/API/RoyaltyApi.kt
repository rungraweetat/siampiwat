package com.siampiwat.API


import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.LoyaltyCardServiceRoute
import com.siampiwat.Model.LoyaltyModelResponseModel
import com.siampiwat.Model.PointModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object RoyaltyApi {

    fun getRoyaltyInfo( searchValue: String): Single<Result<LoyaltyModelResponseModel, FuelError>> {

        return FuelManager.instance.request(LoyaltyCardServiceRoute.GetLoyaltyInfo(searchValue))
                .rx_object(LoyaltyModelResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getPoint( idCard: String): Single<Result<PointModel, FuelError>> {

        return FuelManager.instance.request(LoyaltyCardServiceRoute.GetRedeemablePointBalance(idCard))
                .rx_object(PointModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun checkCustomerIDCard(surveyKey: String): Single<Result<LoyaltyModelResponseModel, FuelError>> {
        return FuelManager.instance.request(LoyaltyCardServiceRoute.CheckCustomerIDCard(surveyKey))
                .rx_object(LoyaltyModelResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getRedeemablePointBalance(cardNumber: String): Single<Result<LoyaltyModelResponseModel, FuelError>> {
        return FuelManager.instance.request(LoyaltyCardServiceRoute.CheckCustomerIDCard(cardNumber))
                .rx_object(LoyaltyModelResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
