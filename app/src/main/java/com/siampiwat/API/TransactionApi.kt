package com.siampiwat.API


import android.util.Log
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.TransactionServiceRoute
import com.siampiwat.Model.TransactionDataModel
import com.siampiwat.Model.TransactionDataResponseModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object TransactionApi {

    fun calculate( transactionId:String , transactionData: TransactionDataModel): Single<Result<TransactionDataResponseModel, FuelError>> {

        var request = TransactionServiceRoute.CalculateTotal(transactionId , transactionData)
        Log.e("","body = "+request.body.toString());
        return FuelManager.instance.request(request)
                .rx_object(TransactionDataResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun returnSales( receiptId:String ): Single<Result<TransactionDataResponseModel, FuelError>>{
        var request = TransactionServiceRoute.ReturnSales(receiptId)
        return FuelManager.instance.request(request)
                .rx_object(TransactionDataResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
