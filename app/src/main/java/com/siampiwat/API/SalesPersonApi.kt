package com.siampiwat.API


import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.SalePersonServieRoute
import com.siampiwat.Model.SalePersonResponseModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object SalesPersonApi {

    fun getSalesPersonInfo( salesPersonId: String): Single<Result<SalePersonResponseModel, FuelError>> {

        return FuelManager.instance.request(SalePersonServieRoute.GetSalesPersonInfo(salesPersonId , "" , ""))
                .rx_object(SalePersonResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
