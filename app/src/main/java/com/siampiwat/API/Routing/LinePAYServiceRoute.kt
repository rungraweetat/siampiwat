package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.siampiwat.API.API_LINE_CONSTANT
import com.siampiwat.Extention.setLinePAYParams
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString


sealed class LinePAYServiceRoute: FuelRouting {

    override val basePath = API_LINE_CONSTANT.LinePAY_URL

    class PaymentRequest(val productName: String, val amount: Double, val orderId: String , val confirmUrl: String, val cancelUrl: String, val oneTimeKey: String): LinePAYServiceRoute()
    class ConfirmPayment(val transactionId: String,val amount: Double): LinePAYServiceRoute()

    override val method: Method
        get() {
            return when(this) {
                is PaymentRequest -> Method.POST
                is ConfirmPayment -> Method.POST
            }
        }

    override val path: String
        get() {
            return when(this) {
                is PaymentRequest -> API_LINE_CONSTANT.API_RESERVE
                is ConfirmPayment -> API_LINE_CONSTANT.API_CONFIRM_PAYMENT.replace("{transactionId}", transactionId)
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
            return setParams()
        }

    override val body: String?
        get() {
            return when(this) {
                is PaymentRequest -> {
                    setLinePAYParams("productName" to  productName, "amount" to amount,
                            "currency" to "THB", "orderId" to orderId,
                            "confirmUrl" to confirmUrl, "cancelUrl" to cancelUrl ,
                            "capture" to true, "confirmUrlType" to "CLIENT",
                            "oneTimeKey" to oneTimeKey).toJsonString()
                }
                is ConfirmPayment ->  setLinePAYParams( "amount" to amount, "currency" to "THB" ).toJsonString()
            }
        }
    override val headers: Map<String, String>?
        get() {
            return mapOf("X-LINE-ChannelId" to API_LINE_CONSTANT.LineChanelId, "X-LINE-ChannelSecret" to API_LINE_CONSTANT.LineChannelSecretKey, "Content-Type" to "application/json")
        }

}
