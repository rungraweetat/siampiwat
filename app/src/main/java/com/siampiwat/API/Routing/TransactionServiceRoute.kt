package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString
import com.siampiwat.Model.TransactionDataModel

sealed class TransactionServiceRoute: FuelRouting  {

    override val basePath = API_CONSTANT.BASE_URL

    class VoidSales(val transactionId: String): TransactionServiceRoute()
    class CalculateTotal(val transactionId: String , val transactionData: TransactionDataModel): TransactionServiceRoute()
    class ReturnSales(val receiptId: String): TransactionServiceRoute()
    override val method: Method
        get() {
            return when(this) {
                is VoidSales -> Method.POST
                is CalculateTotal -> Method.POST
                is ReturnSales -> Method.POST
            }
        }

    override val path: String
        get() {
            return when(this) {
                is VoidSales -> API_CONSTANT.API_VOID_SALES
                is CalculateTotal -> API_CONSTANT.API_CALCULATE_TOTAL
                is ReturnSales -> API_CONSTANT.API_RETURN_SALES
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
            return setParams()
        }

    override val body: String?
        get() {
            return when(this) {
                is VoidSales -> setParams("TransactionId" to transactionId).toJsonString()
                is CalculateTotal -> setParams("TransactionId" to transactionId , "Transaction" to transactionData.toJSONObject()).toJsonString()
                is ReturnSales ->  setParams("ReceiptId" to receiptId).toJsonString()
            }
        }
    override val headers: Map<String, String>?
        get() {
            return mapOf("Content-Type" to "application/json")
        }

}
