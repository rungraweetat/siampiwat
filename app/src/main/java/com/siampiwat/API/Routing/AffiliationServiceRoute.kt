package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString

sealed class AffiliationServiceRoute: FuelRouting  {

    override val basePath = API_CONSTANT.BASE_URL

    class GetAffiliationList: AffiliationServiceRoute()

    override val method: Method
        get() {
            return when(this) {
//                is GetAffiliationList -> Method.GET
                is GetAffiliationList -> Method.POST
            }
        }

    override val path: String
        get() {
            return when(this) {
                is GetAffiliationList -> API_CONSTANT.API_AFFILIATION_LIST
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
            return setParams()
        }

    override val body: String?
        get() {
            return when(this) {
                is GetAffiliationList -> setParams().toJsonString()

            }
        }
    override val headers: Map<String, String>?
        get() {
            return mapOf("Content-Type" to "application/json")
        }

}
