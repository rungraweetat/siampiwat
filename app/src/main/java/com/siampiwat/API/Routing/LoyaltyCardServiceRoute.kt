package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString


sealed class LoyaltyCardServiceRoute: FuelRouting {

    override val basePath = API_CONSTANT.BASE_URL

    class GetLoyaltyInfo(val searchValue: String): LoyaltyCardServiceRoute()
    class GetRedeemablePointBalance(val cardNumber: String): LoyaltyCardServiceRoute()
    class CheckCustomerIDCard(val surveyKey: String): LoyaltyCardServiceRoute()


    override val method: Method
        get() {
            return when(this) {
//                is GetLoyaltyInfo -> Method.GET
                is GetRedeemablePointBalance -> Method.POST
                is CheckCustomerIDCard -> Method.POST

                is GetLoyaltyInfo -> Method.POST
//                is GetRedeemablePointBalance -> Method.GET
//                is CheckCustomerIDCard -> Method.GET
            }
        }

    override val path: String
        get() {
            return when(this) {
                is GetLoyaltyInfo -> API_CONSTANT.API_GET_LOYALTY_INFO
                is GetRedeemablePointBalance -> API_CONSTANT.API_GET_REDEEMABLE_POINT_BALANCE
                is CheckCustomerIDCard -> API_CONSTANT.API_CHECK_CUSTOMER_ID_CARD
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
            return setParams()
        }

    override val body: String?
        get() {
            return when(this) {
                is GetLoyaltyInfo -> setParams( "SearchValue" to searchValue).toJsonString()
                is GetRedeemablePointBalance -> setParams( "CardNumber" to cardNumber).toJsonString()
                is CheckCustomerIDCard -> setParams( "SurveyKey" to surveyKey).toJsonString()
            }
        }
    override val headers: Map<String, String>?
        get() {
            return mapOf("Content-Type" to "application/json")
        }

}
