package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString


sealed class SiamGiftCardServiceRoute: FuelRouting {

    override val basePath = API_CONSTANT.BASE_URL

    class CheckSiamGiftCardBalance(val cardNumber: String, val pinNumber: String): SiamGiftCardServiceRoute()

    override val method: Method
        get() {
            return when(this) {
                is CheckSiamGiftCardBalance -> Method.POST
//                is CheckSiamGiftCardBalance -> Method.GET
            }
        }

    override val path: String
        get() {
            return when(this) {
                is CheckSiamGiftCardBalance -> API_CONSTANT.API_CHECK_SIAM_GIFT_BALANCE
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
            return when(this) {
                is CheckSiamGiftCardBalance -> setParams("CardNumber" to cardNumber, "PinNumber" to pinNumber)
            }
        }

    override val body: String?
    get() {
        return when(this) {
            is SiamGiftCardServiceRoute.CheckSiamGiftCardBalance ->  setParams("CardNumber" to cardNumber, "PinNumber" to pinNumber).toJsonString()
        }
    }
    override val headers: Map<String, String>?
        get() {
            return mapOf("Content-Type" to "application/json")
        }

}
