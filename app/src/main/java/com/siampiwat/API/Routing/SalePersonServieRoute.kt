package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString


sealed class SalePersonServieRoute: FuelRouting {

    override val basePath = API_CONSTANT.BASE_URL

    class GetSalesPersonInfo(val salesPersonId: String , val terminalId:String , val token:String): SalePersonServieRoute()

    override val method: Method
        get() {
            return when(this) {
//                is GetSalesPersonInfo -> Method.GET
                is GetSalesPersonInfo -> Method.POST
            }
        }

    override val path: String
        get() {
            return when(this) {
                is GetSalesPersonInfo -> API_CONSTANT.API_GET_SALES_PERSON_INFO
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
//            return setParams()
            return when(this) {
                is GetSalesPersonInfo -> setParams( "SalesPersonId" to salesPersonId)
            }
        }
//        }

    override val body: String?
        get() {
            return when(this) {
                is GetSalesPersonInfo -> setParams("SalesPersonId" to salesPersonId , "TerminalId" to terminalId , "Token" to token).toJsonString()
            }
        }
    override val headers: Map<String, String>?
        get() {
            return mapOf("Content-Type" to "application/json")
        }
//    (http://10.10.1.83:8500/HHService.svc/Test/Salespersons/?SalesPersonId=t&Token=c356a4f0-0321-488a-876d-01e601946ee4&TerminalId=)
//    http://10.10.1.83:8500/HHService.svc/Test/Salespersons/?SalesPersonId=hh&Token=c356a4f0-0321-488a-876d-01e601946ee4&TerminalId=
//    "Body : (empty)"
//    "Headers : (1)"
//    Accept-Encoding : compress;q=0.5, gzip;q=1.0
//    <-- 200 (http://10.10.1.83:8500/HHService.svc/Test/Salespersons/?SalesPersonId=hh&Token=c356a4f0-0321-488a-876d-01e601946ee4&TerminalId=)
//    Response : OK
//    Length : 203
//    Body : ({"Error":[{"Level":0,"Message":"SalesPersonId is required."}],"SalesPerson":{"Id":"","Name":""}})
}
