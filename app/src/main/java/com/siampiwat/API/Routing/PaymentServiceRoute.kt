package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString
import com.siampiwat.Model.RequestPaymentModel


sealed class PaymentServiceRoute: FuelRouting {

    override val basePath = API_CONSTANT.BASE_URL

    class Payment(val TransactionId:String , val PaymentId:String , val Amount:String , val Payment: RequestPaymentModel): PaymentServiceRoute()

    override val method: Method
        get() {
            return when(this) {
//                is Payment -> Method.GET
                is Payment -> Method.POST
            }
        }

    override val path: String
        get() {
            return when(this) {
                is Payment -> API_CONSTANT.API_PAYMENT
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
            return setParams()
        }

    override val body: String?
        get() {
            return when(this) {
                is Payment -> setParams( "TransactionId" to TransactionId , "PaymentId" to PaymentId , "Amount" to Amount , "Payment" to Payment.toJSONObject()).toJsonString()
            }
        }
    override val headers: Map<String, String>?
        get() {
            return mapOf("Content-Type" to "application/json")
        }

}
