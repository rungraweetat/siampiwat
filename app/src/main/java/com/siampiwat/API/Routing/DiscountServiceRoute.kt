package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString


sealed class DiscountServiceRoute: FuelRouting {

    override val basePath = API_CONSTANT.BASE_URL

    class GetDiscountBarcode(val barcode: String): DiscountServiceRoute()
    class CheckDiscountCoupon(val offerId: String, val serialNumber: String): DiscountServiceRoute()

    override val method: Method
        get() {
            return when(this) {
                is GetDiscountBarcode -> Method.POST
                is CheckDiscountCoupon -> Method.POST

//                is GetDiscountBarcode -> Method.GET
//                is CheckDiscountCoupon -> Method.GET
            }
        }

    override val path: String
        get() {
            return when(this) {
                is GetDiscountBarcode -> API_CONSTANT.API_GET_DISCOUNT_BARCODE
                is CheckDiscountCoupon -> API_CONSTANT.API_CHECK_DISCOUNT_COUPON
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
            return when(this) {
                is GetDiscountBarcode ->  setParams("Barcode" to barcode)
                is CheckDiscountCoupon -> setParams("OfferId" to offerId, "SerialNumber" to serialNumber)
            }
        }

    override val body: String?
    get() {
        return when(this) {
            is GetDiscountBarcode ->  setParams("Barcode" to barcode).toJsonString()
            is CheckDiscountCoupon -> setParams("OfferId" to offerId, "SerialNumber" to serialNumber).toJsonString()
        }
    }

    override val headers: Map<String, String>?
        get() {
            return mapOf("Content-Type" to "application/json")
        }

}
