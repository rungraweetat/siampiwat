package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.google.gson.Gson
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString
import com.siampiwat.Model.SurveyResultModel

sealed class AccountServiceRoute: FuelRouting  {

    override val basePath = API_CONSTANT.BASE_URL

    class Login(val operatorId: String, val password: String, val handheldDeviceId: String): AccountServiceRoute()
    class Logoff: AccountServiceRoute()
    class ChangePassword(val operatorId: String, val operatorPassword: String, val newPassword: String): AccountServiceRoute()
    //class checkManagerPrivilege(val transaction: TransactionDataModel, val surveyResult: SurveyDataModel, val operationId: String): AccountServiceRoute()
    class CheckManagerPrivilege(val surveyResult: SurveyResultModel): AccountServiceRoute()

    override val method: Method
        get() {
            return when(this) {
//                is Login -> Method.GET
//                is Logoff -> Method.GET
//                is ChangePassword -> Method.GET
//                is checkManagerPrivilege -> Method.GET

                is Login -> Method.POST
                is Logoff -> Method.POST
                is ChangePassword -> Method.POST
                is CheckManagerPrivilege -> Method.POST
            }
        }

    override val path: String
        get() {
            return when(this) {
                is Login -> API_CONSTANT.API_LOGON
                is Logoff -> API_CONSTANT.API_LOGOFF
                is ChangePassword -> API_CONSTANT.API_CHANGE_PASSWORD
                is CheckManagerPrivilege -> API_CONSTANT.API_CHECK_MANAGER_PRIVILEGE
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
            return when(this) {
                is CheckManagerPrivilege -> setParams(//"TransactionData" to Gson().toJson(transaction).toString(),
                        "SurveyResult" to Gson().toJson(surveyResult).toString()
                        //"OperationID" to operationId
                        )
                else -> setParams()
            }
        }

    override val body: String?
        get() {
            return when(this) {
                is Login ->  setParams("OperatorId" to operatorId, "Password" to password, "HandheldDeviceId" to handheldDeviceId).toJsonString()
                is Logoff ->  setParams().toJsonString()
                is ChangePassword ->setParams("OperatorId" to operatorId, "OperatorPassword" to operatorPassword, "NewPassword" to newPassword).toJsonString()
                is CheckManagerPrivilege -> setParams("SurveyResult" to surveyResult.toJSONObject()).toJsonString()
                //else -> setParams().toJsonString()
            }
        }
    override val headers: Map<String, String>?
        get() {
            return mapOf("Content-Type" to "application/json")
        }

}
