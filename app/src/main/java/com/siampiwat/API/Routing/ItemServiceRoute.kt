package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString


sealed class ItemServiceRoute: FuelRouting {

    override val basePath = API_CONSTANT.BASE_URL

    class GetItemInfo(val inputItem: String): ItemServiceRoute()
    class SearchProduct(val searchValue: String, val searchOption: String , val token: String, val terminalId: String): ItemServiceRoute()
    class GetProductInfo(val barcode: String): ItemServiceRoute()



    override val method: Method
        get() {
            return when(this) {
                is GetItemInfo -> Method.POST
                is GetProductInfo -> Method.POST
                is SearchProduct -> Method.POST
            }
        }

    override val path: String
        get() {
            return when(this) {
                is GetItemInfo -> API_CONSTANT.API_GET_ITEM_INFO
                is SearchProduct -> API_CONSTANT.API_SEARCH_PRODUCT
                is GetProductInfo -> API_CONSTANT.API_GET_PRODUCT_INFO
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
            return setParams()
        }

    override val body: String?
        get() {
            return when(this) {
                is GetItemInfo -> {
                    setParams("InputItem" to inputItem).toJsonString()
                }
                is SearchProduct -> {
                    setParams("SearchValue" to searchValue, "SearchOption" to searchOption ).toJsonString()
                }
                is GetProductInfo -> {
                    setParams( "Barcode" to barcode).toJsonString()
                }
            }
        }
    override val headers: Map<String, String>?
        get() {
            return mapOf("Content-Type" to "application/json")
        }

}
