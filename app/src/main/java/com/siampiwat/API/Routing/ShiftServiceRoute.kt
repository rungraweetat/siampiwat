package com.siampiwat.API.Routing

import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.util.FuelRouting
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.Extention.setParams
import com.siampiwat.Extention.toJsonString

sealed class ShiftServiceRoute: FuelRouting  {

    override val basePath = API_CONSTANT.BASE_URL

    class CloseShift: ShiftServiceRoute()

    override val method: Method
        get() {
            return when(this) {
//                is CloseShift -> Method.GET
                is CloseShift -> Method.POST
            }
        }

    override val path: String
        get() {
            return when(this) {
                is CloseShift -> API_CONSTANT.API_CLOSE_SHIFT
            }
        }

    override val params: List<Pair<String, Any?>>?
        get() {
            return setParams()
        }

    override val body: String?
        get() {
            return when(this) {
                is CloseShift -> setParams().toJsonString()

            }
        }
    override val headers: Map<String, String>?
        get() {
            return mapOf("Content-Type" to "application/json")
        }

}
