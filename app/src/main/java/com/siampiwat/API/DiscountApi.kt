package com.siampiwat.API

import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.DiscountServiceRoute
import com.siampiwat.Model.DiscountBarcodeResponseModel
import com.siampiwat.Model.DiscountCouponModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


object DiscountApi {

    fun getDiscountBarcode( barcode: String): Single<Result<DiscountBarcodeResponseModel, FuelError>>? {

        return FuelManager.instance.request(DiscountServiceRoute.GetDiscountBarcode(barcode))
                .rx_object(DiscountBarcodeResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun checkDiscountCoupon(offerId: String, serialNumber: String): Single<Result<DiscountCouponModel, FuelError>>? {

        return FuelManager.instance.request(DiscountServiceRoute.CheckDiscountCoupon(offerId, serialNumber))
                .rx_object(DiscountCouponModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
