package com.siampiwat.API

import android.util.Log
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.AccountServiceRoute
import com.siampiwat.Model.EmptyResponse
import com.siampiwat.Model.SurveyResultModel
import com.siampiwat.Model.UserModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object AccountApi {

    fun login(operatorId: String, password: String): Single<Result<UserModel, FuelError>> {
        val handheldDeviceId = "HHPOS000"
        return FuelManager.instance.request(AccountServiceRoute.Login(operatorId, password, handheldDeviceId))
                .rx_object(UserModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun checkManagerPrivilege(surveyModel: SurveyResultModel): Single<Result<UserModel, FuelError>> {
        val routing = AccountServiceRoute.CheckManagerPrivilege(surveyModel)
        Log.e( "","body = "+routing.body.toString() )
        return FuelManager.instance.request(routing)
//        return FuelManager.instance.request(AccountServiceRoute.CheckManagerPrivilege(surveyModel))
                .rx_object(UserModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun logOff(): Single<Result<EmptyResponse, FuelError>> {

        return FuelManager.instance.request(AccountServiceRoute.Logoff())
                .rx_object(EmptyResponse.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun changePassword(operatorId: String, oldPassword: String, newPassword: String): Single<Result<EmptyResponse, FuelError>> {
        return FuelManager.instance.request(AccountServiceRoute.ChangePassword(operatorId, oldPassword, newPassword))
                .rx_object(EmptyResponse.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
