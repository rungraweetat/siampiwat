package com.siampiwat.API


import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.SiamGiftCardServiceRoute
import com.siampiwat.Model.SiamGiftCardResponseModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object SiamGiftCardApi {

    fun getSiamGiftCard( cardNumber: String , pinNumber:String): Single<Result<SiamGiftCardResponseModel, FuelError>> {

        return FuelManager.instance.request(SiamGiftCardServiceRoute.CheckSiamGiftCardBalance(cardNumber , pinNumber))
                .rx_object(SiamGiftCardResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
