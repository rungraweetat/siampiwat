package com.siampiwat.API


import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.LinePAYServiceRoute
import com.siampiwat.Model.LinePAYConfirmPaymentResponseModel
import com.siampiwat.Model.LinePAYPaymentRequestModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object LinePAYApi {

    fun paymentRequest(productName: String, amount: Double, orderId: String , confirmUrl: String, cancelUrl: String, onTimeKey: String ): Single<Result<LinePAYPaymentRequestModel, FuelError>> {

        return FuelManager.instance.request(LinePAYServiceRoute.PaymentRequest(productName, amount, orderId, confirmUrl, cancelUrl, onTimeKey))
                .rx_object(LinePAYPaymentRequestModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun confirmPayment(transactionId: String, amount: Double): Single<Result<LinePAYConfirmPaymentResponseModel, FuelError>> {
        return FuelManager.instance.request(LinePAYServiceRoute.ConfirmPayment(transactionId, amount))
                .rx_object(LinePAYConfirmPaymentResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }
}
