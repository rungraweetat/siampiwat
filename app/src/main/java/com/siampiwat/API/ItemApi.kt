package com.siampiwat.API


import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.ItemServiceRoute
import com.siampiwat.Model.ItemInfoResponseModel
import com.siampiwat.Model.ProductInfoResponseModel
import com.siampiwat.Model.SearchProductListModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object ItemApi {

    fun getItemsInfo(inputItem: String): Single<Result<ItemInfoResponseModel, FuelError>> {
        return FuelManager.instance.request(ItemServiceRoute.GetItemInfo(inputItem))
                .rx_object(ItemInfoResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun searchProduct( searchValue: String,  searchOption: String = "" , token: String,  terminalId: String = ""): Single<Result<SearchProductListModel, FuelError>> {

        return FuelManager.instance.request(ItemServiceRoute.SearchProduct(searchValue, searchOption , token , terminalId))
                .rx_object(SearchProductListModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }


    fun getProductInfo( inputItem: String): Single<Result<ProductInfoResponseModel, FuelError>> {

        return FuelManager.instance.request(ItemServiceRoute.GetProductInfo(inputItem))
                .rx_object(ProductInfoResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
