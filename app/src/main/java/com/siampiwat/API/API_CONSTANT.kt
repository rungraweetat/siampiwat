package com.siampiwat.API

object API_CONSTANT {
    private val URL_TEST = byteArrayOf(104, 116, 116, 112, 58, 47, 47, 117, 97, 116, 46, 49, 49, 53, 53, 45, 116, 111, 117, 99, 104, 46, 116, 107, 47, 97, 112, 105, 115)
    private val URL_PRODUCTION = byteArrayOf(104, 116, 116, 112, 58, 47, 47, 49, 49, 53, 53, 116, 111, 117, 99, 104, 46, 99, 111, 109, 47, 97, 112, 105, 115)

//    const val BASE_URL =  "https://1c43b670-94fb-42d9-b807-78a4b5cc96f3.mock.pstmn.io/HHService.svc/Test"
//    const val BASE_URL =  "http://10.10.1.83:8500/HHService.svc"
//                                  10.10.1.83:8500/HHService.svc/Items/SearchProduct
    const val BASE_URL =  "http://10.10.1.83:8500/HHService.svc"
    //URL_TEST.toStringUTF8()


    const val API_LOGON = "Accounts/LogOn/"
    const val API_LOGOFF = "Accounts/LogOff/"
    const val API_GET_ITEM_INFO = "Items/"
    const val API_GET_SALES_PERSON_INFO = "Salespersons/"
    const val API_GET_LOYALTY_INFO = "/LoyaltyCards/"
    const val API_AFFILIATION_LIST = "Affiliations/"
    const val API_VOID_SALES = "Transactions/Void/"
    const val API_GET_DISCOUNT_BARCODE = "Discounts/DiscountBarcode/"
    const val API_CHECK_DISCOUNT_COUPON = "DiscountCoupons/Check/"
    const val API_GET_REDEEMABLE_POINT_BALANCE = "LoyaltyCards/RedeemableBalance/"
    const val API_CALCULATE_TOTAL = "Transactions/Calculate/"
    const val API_PAYMENT = "Payment/"
    const val API_RETURN_SALES = "Transactions/Return/"
    const val API_SEARCH_PRODUCT = "Items/SearchProduct/"
//    const val API_SEARCH_PRODUCT = "Items/SearchProdcut"
    const val API_GET_PRODUCT_INFO = "Items/ProductInfo/"
    const val API_CHECK_SIAM_GIFT_BALANCE = "Siamgiftcard/Check/"
    const val API_CLOSE_SHIFT = "Shifts/Close/"
    const val API_CHANGE_PASSWORD = "Accounts/ChangePassword/"
    const val API_CHECK_CUSTOMER_ID_CARD = "Loyalty/ChkIDCard/"
    const val API_CHECK_MANAGER_PRIVILEGE = "Privileges/CheckManager/"

}


object API_LINE_CONSTANT {

    val LineChanelId = "1617749995"
    val LineChannelSecretKey = "8b20ce4323e2f55ebc0c0637fe8cf7b5"
    val LinePAY_URL = "https://api-pay.line.me" //"https://sandbox-api-pay.line.me"

    const val API_RESERVE = "v2/payments/request"
    const val API_CONFIRM_PAYMENT= "/v2/payments/{transactionId}/confirm"
}

object API_TIMEOUT{
    val timeoutMinutes = 1000*60*5
}