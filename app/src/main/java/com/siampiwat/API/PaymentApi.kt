package com.siampiwat.API


import android.util.Log
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.PaymentServiceRoute
import com.siampiwat.Model.RequestPaymentModel
import com.siampiwat.Model.TransactionDataResponseModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object PaymentApi {

    fun payment( transactionId:String , paymentId:String , amountPayment:String , requestPaymentModel: RequestPaymentModel): Single<Result<TransactionDataResponseModel, FuelError>> {

        var request = PaymentServiceRoute.Payment(transactionId , paymentId,amountPayment , requestPaymentModel)
        Log.e("","body = "+request.body.toString());
        return FuelManager.instance.request(request)
                .rx_object(TransactionDataResponseModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
