package com.siampiwat.API


import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.siampiwat.API.Routing.ShiftServiceRoute
import com.siampiwat.Model.CloseShiftModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object CloseShiftApi {

    fun closeShift(): Single<Result<CloseShiftModel, FuelError>> {

        return FuelManager.instance.request(ShiftServiceRoute.CloseShift())
                .rx_object(CloseShiftModel.Deserializer())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

//    fun closeShift(): Single<Result<LoyaltyModelResponseModel, FuelError>> {
//
//        return FuelManager.instance.request(ShiftServiceRoute.CloseShift())
//                .rx_object(LoyaltyModelResponseModel.Deserializer())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//    }
}
