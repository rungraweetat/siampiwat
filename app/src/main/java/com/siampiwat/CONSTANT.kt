package com.siampiwat

object REQUEST_CODE {
    const val ADD_PRODUCT  = 999
    const val ADD_CUSTOMER = 998
    const val SEARCH_PRODUCT = 997
}


object ITEM_TYPE {
    const val PRODUCT  = 0
    const val CUSTOMER = 1
    const val VIEW_PRODUCT = 2
    const val VIEW_DISCOUNT = 3
    const val VIEW_NET_PRICE = 4

}
object ClassOfDiscount {
    const val DiscountBarcodeModel  = 0
    const val CouponDiscountModel = 1
    const val RedemptionDiscountModel = 2
    const val SubTotalDiscountModel = 3
    const val BottomLineDiscountModel = 4
    const val ManualDiscount = 5
    const val RequestManualTotalDiscount = 6

}
object PaymentMethod {
    const val CreditCard  = "100"
    const val ViaSCBapp = "110"
    const val ViaExtEDC = "120"
    const val EWallet  = "200"
    const val WeChatPay = "210"
    const val AliPay = "220"
    const val RabbitLINEPay = "230"
    const val TRUEMoney = "240"
    const val AirPay = "250"
    const val BluePay = "260"
    const val PromptPay  = "300"
    const val CashCoupons  = "400"
    const val SiamPiwatCoupons  = "410"
    const val SPWCoupon = "411"
    const val DCRCoupon = "412"
    const val ALANDCoupon = "413"
    const val PartnerCoupons = "420"
    const val HUBBACoupon = "421"
    const val UPlanCoupon = "422"
    const val MEV = "500"

}

object RequestCodeScanQR {
    const val BluePay  = 0
    const val RabbitLINEPay  = 1
    const val PromptPay  = 2
}

object RequestSCBPayment {
    const val Card  = 11
    const val AliPay  = 22
    const val WeChat = 33
    const val ThaiQRCode = 44
}


object PaymentSCBTradeType {
    const val Card  = "CARD"
    const val AliPay  = "ALIPAY"
    const val WeChat  = "WECHAT"
    const val ThaiQRCode = "THAIQRCODE"
}