package com.siampiwat.State.Reducer


import com.siampiwat.State.State.*
import org.rekotlin.Action
import org.rekotlinrouter.NavigationReducer

fun appReducer(action: Action, oldState: AppState?) : AppState {

    val state = oldState ?: AppState(
            navigationState = NavigationReducer.handleAction(action = action, state = oldState?.navigationState),
            authenticationState = AuthenticationState(loggedInState = LoggedInState.notLoggedIn, userInfo = null),
            cartInfoState = CartInfoState())

    return state.copy(
            navigationState = NavigationReducer.reduce(action = action, oldState = state.navigationState),
            authenticationState = (::authenticationReducer)(action, state.authenticationState),
            cartInfoState = (::cartInfoReducer)(action, state.cartInfoState))
}

