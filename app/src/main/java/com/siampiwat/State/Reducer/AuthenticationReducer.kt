package com.siampiwat.State.Reducer

import com.siampiwat.MainApp
import com.siampiwat.Model.UserModel
import com.siampiwat.State.Action.LogOffSuccessAction
import com.siampiwat.State.Action.LogonFailedAction
import com.siampiwat.State.Action.LogonSuccessAction
import com.siampiwat.State.Service.PreferenceApiService
import com.siampiwat.State.State.AuthenticationState
import com.siampiwat.State.State.LoggedInState
import com.siampiwat.terminalID
import com.siampiwat.userInfo
import org.rekotlin.Action


fun authenticationReducer(action: Action, state: AuthenticationState?): AuthenticationState {

    val  newState =  state ?: AuthenticationState(LoggedInState.notLoggedIn,userInfo = null)
    return when (action) {

        is LogonSuccessAction ->  {
            PreferenceApiService.setAuthenticationPreference(action.context, action.userInfo!!)
            userInfo = action.userInfo
            return newState.copy(LoggedInState.loggedIn, userInfo = action.userInfo)
        }
        is LogonFailedAction ->  newState.copy(LoggedInState.loginFail, userInfo = null)
        is LogOffSuccessAction -> {
            PreferenceApiService.clearAuthenticationPreference(action.context)
            userInfo =  null
            terminalID = null
            MainApp.mInstance?.setupStore()
            return newState.copy(LoggedInState.notLoggedIn, userInfo = null)
        }
        else -> newState
    }
}