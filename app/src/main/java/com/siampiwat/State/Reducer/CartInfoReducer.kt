package com.siampiwat.State.Reducer

import com.siampiwat.Model.InputSalesLineModel
import com.siampiwat.Model.SalePersonModel
import com.siampiwat.Model.TransactionDataModel
import com.siampiwat.State.Action.*
import com.siampiwat.State.State.CartInfoState
import org.rekotlin.Action

fun cartInfoReducer(action: Action, state: CartInfoState): CartInfoState {
    return when (action) {
        is GetCartInfo -> {

            return state
        }
        is AddProductInCart -> {

            var hasProductInCart = isAddedProductInCart(action.product.ItemId , state)
            if( hasProductInCart  > -1 )
            {
                //ถ้ามีสินค้าในตะกร้าแล้วให้
                state.transactionData.InputSalesLine[hasProductInCart].Qty += 1
            }
            else
            {
                action.product.Amount = 1

                val lineNum = state.transactionData.InputSalesLine.size + 1
                val productItem = action.product
                var systemPrice = 0.0

                systemPrice = try {
                    productItem.SystemPrice.toDouble()
                } catch (ex: java.lang.Exception) {
                    productItem.SystemPrice.replace(",", "").toDouble()
                }

                val inputSaleLine = InputSalesLineModel(
                        lineNum,
                        productItem.ItemId,
                        productItem.ItemBarcode,
                        productItem.ItemDescription,
                        systemPrice,
                        systemPrice,
                        productItem.Amount,
                        productItem.UnitOfMeasure,
                        productItem.IsKeyInPrice,
                        IsChangePrice = false,
                        SalesPersonId = null,
                        SalesPersonName = null,
                        ManualLineDiscount = null,
                        LineSurveyItems = null
                )
                state.transactionData.InputSalesLine.apply {
                    add(inputSaleLine)
                }
            }


            return state
        }
        is AddProductListInCart -> {

            for (productItem in action.products) {
                val lineNum = state.transactionData.InputSalesLine.size + 1
                val inputSaleLine = InputSalesLineModel(
                        lineNum,
                        productItem.ItemId,
                        productItem.ItemBarcode,
                        productItem.ItemDescription,
                        productItem.SystemPrice.toDouble(),
                        productItem.SystemPrice.toDouble(),
                        productItem.Amount,
                        productItem.UnitOfMeasure,
                        productItem.IsKeyInPrice,
                        IsChangePrice = false,
                        SalesPersonId = null,
                        SalesPersonName = null,
                        ManualLineDiscount = null,
                        LineSurveyItems = null
                )

                state.transactionData.InputSalesLine.apply {
                    add(inputSaleLine)
                }
            }

            return state
        }
        is IncreaseAmountProductInCart -> {
            state.transactionData.InputSalesLine[action.index].Qty += 1

            return state
        }
        is DecreaseAmountProductInCart -> {
            val inputSalesLine = state.transactionData.InputSalesLine[action.index]
            if (inputSalesLine.Qty > 1) {
                state.transactionData.InputSalesLine[action.index].Qty -= 1
            }

            return state
        }
        is RemoveProductInCart -> {
            if( state.transactionData.LoyaltyCard == null )
            {
                state.transactionData.InputSalesLine.removeAt(action.index)
            }
            else
            {
                //ถ้ามี ลูกค้าแล้วให้เอา (index - 1) เพราะตำแหน่งใน recyclerView มันต่อกัน( customer กับ product )
                state.transactionData.InputSalesLine.removeAt(action.index-1)
            }
            return state
        }
        is AddRedemption -> {
            state.transactionData.RedemtionDiscount = action.redemptionModel
            return state
        }
        is AddCustomerInCart -> {

            state.transactionData.LoyaltyCard = action.customer

            return state
        }
        is RemoveCustomerInCart -> {

            state.transactionData.LoyaltyCard = null

            return state
        }
        is CancelSeller -> {

            state.transactionData = TransactionDataModel()
//            for (inputSaleLine in   state.transactionData.InputSalesLine ){
//                inputSaleLine.clearSalesPersonData()
//            }

            return state
        }
        is MatchingSalePersonWithProduct -> {
            for (i in action.products.indices) {

                state.transactionData.InputSalesLine[i].SalesPersonId = action.products[i].SalesPersonId
                state.transactionData.InputSalesLine[i].SalesPersonName = action.products[i].SalesPersonName
            }

            state.transactionData.InputSalesLine = action.products

            return state
        }
        is RemoveSellerInProduct -> {
            if (state.transactionData.InputSalesLine.size > action.index) {

                state.transactionData.InputSalesLine[action.index].SalesPersonId = ""
                state.transactionData.InputSalesLine[action.index].SalesPersonName = ""
                state.transactionData.InputSalesLine[action.index].isSelectedSalePerson = false
                state.transactionData.InputSalesLine[action.index].setSalesPersonData(SalePersonModel("", ""))
            }

            return state
        }

        is SetSellerInProduct -> {
            var inputSaleLine = state.transactionData.InputSalesLine
            for (i in inputSaleLine.indices) {
                if (inputSaleLine[i].ItemId == action.productKey) {
                    state.transactionData.InputSalesLine[i].SalesPersonId = action.sellerInfo.Id
                    state.transactionData.InputSalesLine[i].SalesPersonName = action.sellerInfo.Name
                }
            }
            return state
        }

        is AddDiscountBarcode -> {

            state.transactionData.DiscountBarcodes.add(action.discountBarcode)
            return state
        }
        is RemoveDiscountBarcode -> {

            state.transactionData.DiscountBarcodes.removeAt(action.index)
            return state
        }

        is RemoveDiscountBarcodeById -> {

            if( state.transactionData.DiscountBarcodes.size > 0 )
            {
                for( i in state.transactionData.DiscountBarcodes.indices )
                    if( state.transactionData.DiscountBarcodes[i].Barcode == action.barcode )
                        state.transactionData.DiscountBarcodes.removeAt(i)
            }

            return state
        }

        is AddCouponDiscount -> {

            state.transactionData.CouponDiscount.add(action.couponDiscount)
            return state
        }
        is AddManualLineDiscount -> {
            var inputSaleLine = state.transactionData.InputSalesLine
            for (i in inputSaleLine.indices) {
                if (inputSaleLine[i].ItemId == action.productKey) {
                    if( action.manualLineDiscountModel.DiscountValue > 0 )
                        state.transactionData.InputSalesLine[i].ManualLineDiscount = action.manualLineDiscountModel
                }
            }

            return state
        }
        is ChangePrice -> {

            var inputSaleLine = state.transactionData.InputSalesLine
            for (i in inputSaleLine.indices) {
                if (inputSaleLine[i].ItemId == action.productKey) {
                    if( action.changePrice > 0 )
                    {
                        state.transactionData.InputSalesLine[i].Price = action.changePrice
                        state.transactionData.InputSalesLine[i].IsKeyInPrice = true
                        state.transactionData.InputSalesLine[i].IsChangePrice = true
                    }
                }
            }

            return state
        }
        is RemoveCouponDiscount -> {

            state.transactionData.CouponDiscount.removeAt(action.index)
            return state
        }
        is AddSubTotalDiscount -> {
            state.transactionData.SubTotalDiscounts.add(action.subtotalDiscount)
            return state
        }
        is RemoveSubTotalDiscount -> {
            state.transactionData.SubTotalDiscounts = ArrayList(state.transactionData.SubTotalDiscounts.filterNot { subTotalDiscount -> subTotalDiscount.Id == action.subtotalDiscount.Id })
            return state
        }
        is AddTransactionDataCalculate -> {
            state.transactionDataCalculate = action.transactionDataCalculate
            return state
        }
        is AddManualTotalDiscount -> {
            state.transactionData.ManualTotalDiscount = action.requestManualTotalDiscount
            return state
        }
        else -> state
    }
}

fun isAddedProductInCart(itemId:String , state: CartInfoState):Int{

    for( i in state.transactionData.InputSalesLine.indices )
    {
        var inputSaleLine = state.transactionData.InputSalesLine[i]
        if(inputSaleLine.ItemId == itemId) return i
    }

    return -1
}