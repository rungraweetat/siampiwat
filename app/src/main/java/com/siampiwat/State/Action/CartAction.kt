package com.siampiwat.State.Action

import com.siampiwat.Model.*
import org.rekotlin.Action


class GetCartInfo: Action

data class AddProductInCart(val product: ItemInfoModel): Action
data class AddProductListInCart(val products: ArrayList<ItemInfoModel>): Action
data class IncreaseAmountProductInCart(val index: Int): Action
data class DecreaseAmountProductInCart(val index: Int): Action
data class RemoveProductInCart(val index: Int): Action
class CancelSeller(): Action


data class AddCustomerInCart(val customer: LoyaltyCardModel): Action
data class RemoveCustomerInCart(val index: Int): Action
data class SetTypeCustomer( val sellerInfo: CustomerModel): Action


data class SetSellerInProduct(val productKey: String, val sellerInfo: SalePersonModel): Action
data class MatchingSalePersonWithProduct(val products: ArrayList<InputSalesLineModel>): Action

data class RemoveSellerInProduct(val index: Int): Action

data class AddManualTotalDiscount(var requestManualTotalDiscount: RequestManualTotalDiscount): Action
data class AddRedemption(var redemptionModel: RedemptionDiscountModel): Action


data class AddManualLineDiscount(val manualLineDiscountModel: ManualLineDiscountModel , val productKey: String ): Action
data class ChangePrice(val changePrice: Double , val productKey: String ): Action

data class AddDiscountBarcode(val discountBarcode: DiscountBarcodeModel): Action
data class RemoveDiscountBarcode(val index: Int): Action
data class RemoveDiscountBarcodeById(val barcode:String): Action

data class AddCouponDiscount(val couponDiscount: CouponDiscountModel): Action
data class RemoveCouponDiscount(val index: Int): Action

data class AddSubTotalDiscount(val subtotalDiscount: SubTotalDiscountModel): Action
data class RemoveSubTotalDiscount(val subtotalDiscount: SubTotalDiscountModel): Action

data class AddTransactionDataCalculate(val transactionDataCalculate: TransactionDataModel): Action
