package com.siampiwat.State.Action

import android.content.Context
import com.siampiwat.Model.UserModel
import org.rekotlin.Action

data class LogonSuccessAction(val context: Context, val userInfo: UserModel?): Action
data class LogonFailedAction(val errorMessage: String?): Action

data class LogOffSuccessAction(val context: Context): Action
data class LogOffFailedAction(val errorMessage: String?): Action


