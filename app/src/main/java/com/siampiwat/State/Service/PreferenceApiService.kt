package com.siampiwat.State.Service


import android.content.Context
import android.content.SharedPreferences
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.interceptors.loggingRequestInterceptor
import com.github.kittinunf.fuel.core.interceptors.loggingResponseInterceptor
import com.google.gson.Gson
import com.siampiwat.API.API_CONSTANT
import com.siampiwat.API.API_TIMEOUT
import com.siampiwat.Model.UserModel
import com.siampiwat.State.PREFS_CONSTANT
import com.siampiwat.terminalID

object PreferenceApiService {


    fun savePreference(context: Context, key: String, value: String) {
        val settings: SharedPreferences = context.getSharedPreferences(PREFS_CONSTANT.PREFS_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = settings.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun clearPreference(context: Context) {
        val settings: SharedPreferences = context.getSharedPreferences(PREFS_CONSTANT.PREFS_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = settings.edit()
        editor.clear()
        editor.commit()
    }

    fun getPreference(context: Context, key: String): String? {
        val settings = getSharedPreferenceByName(context, PREFS_CONSTANT.PREFS_NAME)
        return settings.getString(key, null)
    }

    fun getSharedPreferenceByName(context: Context, sharedPreferenceKey: String): SharedPreferences {
        return context.getSharedPreferences(sharedPreferenceKey, Context.MODE_PRIVATE)
    }

    fun getAuthenticationPreference(context: Context): UserModel? {

        var userInfo:UserModel? = null
        val userInfoJson = getPreference(context, PREFS_CONSTANT.PREFS_USER_INFO)

        if (userInfoJson != null){
            userInfo= Gson().fromJson(userInfoJson, UserModel::class.java)
        }
        return userInfo
    }

    fun setAuthenticationPreference(context: Context, userModel: UserModel) {
       var userString =  Gson().toJson(userModel)
        savePreference(context, PREFS_CONSTANT.PREFS_USER_INFO, userString)
        savePreference(context, PREFS_CONSTANT.PREFS_TERMINAL_ID, "")
    }

    fun clearAuthenticationPreference(context: Context) {
        clearPreference(context)
    }

    fun setupFuelManager() {
        val manager = FuelManager.instance
        manager.basePath = API_CONSTANT.BASE_URL
        manager.addRequestInterceptor(loggingRequestInterceptor())
        manager.addResponseInterceptor { loggingResponseInterceptor() }
        manager.timeoutInMillisecond = API_TIMEOUT.timeoutMinutes
        manager.timeoutReadInMillisecond = API_TIMEOUT.timeoutMinutes
    }

    fun setupFuelBaseParameter(context: Context) {

        terminalID = PreferenceApiService.getPreference(context = context, key = PREFS_CONSTANT.PREFS_TERMINAL_ID)
        val userInfo = PreferenceApiService.getAuthenticationPreference(context)

        val manager = FuelManager.instance
        manager.baseParams = listOf("Token" to userInfo?.Token, "TerminalId" to terminalID)
    }
}