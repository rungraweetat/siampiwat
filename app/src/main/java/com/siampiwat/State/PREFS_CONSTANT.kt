package com.siampiwat.State

object  PREFS_CONSTANT {
    val PREFS_NAME = "SIAM_PIWAT_PREFS"
    val PREFS_USER_INFO =  "PREFS_USER_INFO"
    val PREFS_TERMINAL_ID = "PREFS_TERMINAL_ID"
}