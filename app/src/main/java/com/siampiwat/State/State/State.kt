package com.siampiwat.State.State

import com.siampiwat.Model.*
import org.rekotlin.StateType
import org.rekotlinrouter.HasNavigationState
import org.rekotlinrouter.NavigationState

data class AppState(override var navigationState: NavigationState,
                    var authenticationState: AuthenticationState,
                    var cartInfoState: CartInfoState): StateType, HasNavigationState


enum class LoggedInState {
    notLoggedIn,
    loging,
    loggedIn,
    loginFail
}

data class AuthenticationState(var loggedInState: LoggedInState,
                               var userInfo: UserModel?): StateType




data class CartInfoState(var transactionData: TransactionDataModel = TransactionDataModel(),
                         var transactionDataCalculate: TransactionDataModel = TransactionDataModel()): StateType
