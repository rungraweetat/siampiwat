package com.siampiwat

import android.content.Intent
import android.support.multidex.MultiDexApplication
import com.siampiwat.MainMenu.MainMenuActivity
import com.siampiwat.Model.UserModel
import com.siampiwat.State.Reducer.appReducer
import com.siampiwat.State.Service.PreferenceApiService
import com.siampiwat.State.State.*
import org.rekotlin.Store
import org.rekotlinrouter.NavigationState

var mainStore = Store(state = null,
        reducer = ::appReducer,
        middleware = emptyList())

var userInfo: UserModel? = null
var terminalID: String? = ""
val handheldDeviceId: String = "HHPOS001"

class MainApp : MultiDexApplication() {
    val loggedInState = LoggedInState.notLoggedIn
    override fun onCreate() {
        super.onCreate()
        setupFuelManager()
        setupStore()

        if (loggedInState == LoggedInState.loggedIn){
            loggedOn()
        }
        mInstance = this
    }

    private  fun  setupFuelManager(){
        PreferenceApiService.setupFuelBaseParameter(applicationContext)
        PreferenceApiService.setupFuelManager()
    }


    fun  setupStore(){

        userInfo = PreferenceApiService.getAuthenticationPreference(applicationContext)
        val loggedInState = if (userInfo != null) LoggedInState.loggedIn else LoggedInState.notLoggedIn
        val authenticationState = AuthenticationState(loggedInState = loggedInState, userInfo = userInfo)

        val state = AppState(navigationState = NavigationState(),
                authenticationState = authenticationState,
                cartInfoState = CartInfoState())

        mainStore = Store(state = state,
                reducer = ::appReducer,
                middleware = emptyList(),
                automaticallySkipRepeats = true)

    }
    private fun loggedOn() {

        setupFuelManager()
        val intent = Intent(this, MainMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    companion object {
        @get:Synchronized var mInstance: MainApp? = null
            private set

    }

}