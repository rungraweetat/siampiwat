package com.siampiwat.Setting

import android.os.Bundle
import com.siampiwat.BaseObject.BaseOnActivity
import com.siampiwat.R
import kotlinx.android.synthetic.main.activity_setting.*
import android.content.pm.PackageManager
import android.widget.RadioButton
import com.siampiwat.Extention.toast

class SettingActivity : BaseOnActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        bindEvent()
        setTextVersion()
        setRadioGroupListener()
    }

    private fun  setTextVersion() {
        try {
            val pInfo = applicationContext.getPackageManager().getPackageInfo(packageName, 0)
            val version = getString(R.string.label_version)  +pInfo.versionName
            txtVersion.text =  version
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }
    private fun bindEvent() {

        navigatorView?.setListener{
            finish()
        }
    }

    private fun setRadioGroupListener() {
        radioChangeLanguageGroup.setOnCheckedChangeListener { group, checkedId ->
            this?.let {
                val radio: RadioButton = findViewById(checkedId)
                applicationContext?.toast(radio.text.toString())
            }
        }
    }
}