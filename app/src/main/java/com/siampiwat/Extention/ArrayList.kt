package com.siampiwat.Extention

import com.google.gson.Gson
import com.siampiwat.terminalID
import com.siampiwat.userInfo
import org.json.JSONObject

fun setParams(vararg element: Pair<String, Any?>?): List< Pair<String, Any?>>? {
    val arrayList: ArrayList<Pair<String, Any?>> = arrayListOf()

    if (element.isNotEmpty()){
        element.map {
            it?.let { it1 -> arrayList.add(it1) }
        }
    }

    arrayList.add("Token" to userInfo?.Token)
    arrayList.add("TerminalId" to userInfo?.TerminalId)
    return arrayList
}


fun setLinePAYParams(vararg element: Pair<String, Any?>?): List< Pair<String, Any?>>? {
    val arrayList: ArrayList<Pair<String, Any?>> = arrayListOf()

    if (element.isNotEmpty()){
        element.map {
            it?.let { it1 -> arrayList.add(it1) }
        }
    }

    return arrayList
}

fun List<Pair<String, Any?>>?.toJsonString(): String  {
    var jsonObject =  JSONObject()
    this?.map {
        jsonObject.put(it.first, it.second)
    }

    return jsonObject.toString()
}