package com.siampiwat.Extention

fun String.toByteArrayUTF8(): String =  this.toByteArray(Charsets.UTF_8).contentToString()
