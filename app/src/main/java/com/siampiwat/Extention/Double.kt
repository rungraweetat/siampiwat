package com.siampiwat.Extention

import java.text.DecimalFormat

fun Double.toString2Digit(): String {
    val df = DecimalFormat("#,###.00")
    if( this == 0.0 ){
        return "0.00"
    }
    return df.format(this)
}

fun Double.convertToString(): String {
    return this.toString()
}