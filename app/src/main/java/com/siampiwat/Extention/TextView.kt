package com.siampiwat.Extention

import android.widget.TextView

fun TextView.setDrawableLeft(drawable: Int){
    this.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0)
}