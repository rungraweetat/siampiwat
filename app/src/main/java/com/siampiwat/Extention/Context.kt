package com.siampiwat.Extention
 
import android.app.ProgressDialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.WindowManager
import android.widget.Toast
import com.siampiwat.R
import org.jetbrains.anko.AlertDialogBuilder
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog

fun Context?.toast(text: String) = this?.let { Toast.makeText(it, text, Toast.LENGTH_LONG).show() }
fun Context?.toastShot(text: String) = this?.let { Toast.makeText(it, text, Toast.LENGTH_SHORT).show() }
fun Context?.indeterminateProgress(text: String): ProgressDialog? {
    this?.let {
        val dialog = this.indeterminateProgressDialog(text)
        dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog.window.setBackgroundDrawable( ColorDrawable(android.graphics.Color.TRANSPARENT) )
        dialog.setContentView(R.layout.layout_progress)
        dialog.setCancelable(false)
        return dialog
    }
    return null
}

fun Context.alert(title: String, message: String): AlertDialogBuilder {
    return this.alert(message, title)
}



